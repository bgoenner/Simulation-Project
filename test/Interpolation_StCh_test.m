clc
clear
close all

addpath('.\..\backgroundFlow')

%backgroundFlowFileName = 'VfieldStCh_100w_50h_straight_relativeTole-6FinerMesh.csv';

%backgroundFlowFileName = 'Katrina_field_100w_50h_Straight_finerMesh.csv';%r = dat_dat(:,1)'*1e0;s = surf(qx, qz, -qv);
% backgroundFlowFileName = 'KatrinaVP_100w_50h_sphere_straight.csv';
backgroundFlowFileName = 'TestTest.csv';

 dat_dat = csvread(backgroundFlowFileName,10);
% slice of X

%dat_dat = unique(dat_dat(dat_dat(:,2) == 0, :), 'rows');
%dat_dat = dat_dat(dat_dat(:,2) == -5e-5, :);
r = dat_dat(:,1)'*1e0;%which is x axis (channel width)
%y = dat_dat(:,2)'*1e0;
z = dat_dat(:,3)'*1e0;%which is z axis (channel height)
u = dat_dat(:,4)'*1e0;%which is Ux axis
v = dat_dat(:,5)'*1e0;%which is Uy axis
w = dat_dat(:,6)'*1e0;%which is Uz axis
p  = dat_dat(:,7)'*1e0;

p = p + -min(p);%??

U = scatteredInterpolant(r', z', u');
V = scatteredInterpolant(r', z', v');
W = scatteredInterpolant(r', z', w');
P  = scatteredInterpolant(r', z', p');


x_chan = linspace(min(r), max(r), 200);
z_chan = linspace(min(z), max(z), 200);

rho = 1000; %kg / m^3

[qx, qz] = meshgrid(x_chan, z_chan);

qu = U(qx, qz);
qv = V(qx, qz);
qw = W(qx, qz);
qp = P(qx, qz);

%qE = qp + (qu.^2 + qv.^2 + qw.^2) *rho * 0.5;
qKE = (qu.^2 + qw.^2) *rho * 0.5;
%qE = qp + qKE;

figure
[C,h] = contourf(qx, qz, qu);
clabel(C,h)
title('x velocity contour')
figure
[C,h] = contourf(qx, qz, qv);
clabel(C,h)
title('y velocity contour')
figure
[C,h] = contourf(qx, qz, qw);
clabel(C,h)
title('z velocity contour')
% figure
% [C,h] = contourf(qx, qz, qp);
% clabel(C,h)
% title('Pressure contour')
% figure
% [C,h] = contourf(qx, qz, qE);
% clabel(C,h)
% title('Energy contour')

% figure
% s = surf(qx, qz, qp);
% %clabel(C,h)
% s.EdgeColor = 'none';
% title('Pressure surface')

% figure
% s = surf(qx, qz, qE);
% %clabel(C,h)
% s.EdgeColor = 'none';
% title('Total Energy surface')

figure
s = surf(qx, qz, -qv);
%clabel(C,h)
s.EdgeColor = 'none';
title('Through Channel Velocity')


% figure
% s = surf(qx, qz, qKE);
% %clabel(C,h)
% s.EdgeColor = 'none';
% title('Kinetic Energy surface')


%Put red dot here on Pcenter = 1965; to run the simulation, get the result
%for related plots to V, P
Pcenter = 20;
Plength = 3;
h = 22;
Pl = Pcenter - Plength/2;
Pr = Pcenter + Plength/2;
Pt = h + Plength/2;
Pb = h - Plength/2;
Parea = Plength^2 * 1e-12;

Fnetx = P(Pl,h)*Parea - P(Pr,h)*Parea;
Fnetz = P(Pt,h)*Parea - P(Pb,h)*Parea;


% velx = U(Pcenter, h);%To understand V at that point to calculate Fd
% velz = W(Pcenter, h);
mu = 10^-3;

velx = Fnetx/(6*pi*Plength/2*mu);
velz = Fnetz/(6*pi*Plength/2*mu);

U = scatteredInterpolant(r', z', u', 'natural');
V = scatteredInterpolant(r', z', v', 'natural');
W = scatteredInterpolant(r', z', w', 'natural');
P = scatteredInterpolant(r', z', p', 'natural');

x_chan = linspace(min(r),max(r),1000);
z_chan = linspace(min(z),max(z),1000);

[qx, qz] = meshgrid(x_chan, z_chan);

qu = U(qx, qz);
qv = V(qx, qz);
qw = W(qx, qz);
qp = P(qx, qz);

qE = qp + (qu^2 + qw^2) *rho * 0.5;

figure
contourf(qx, qz, qu)
title('x velocity contour natural')
figure
contourf(qx, qz, qv)
title('y velocity contour natural')
figure
contourf(qx, qz, qw)
title('z velocity contour natural')
figure
contourf(qx, qz, qp)
title('Pressure contour natural')
figure
contourf(qx, qz, qE)
title('Energy contour natural')
