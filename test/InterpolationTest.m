clc
clear
close all


addpath('.\..\backgroundFlow')

%backgroundFlowFileName = 'Vfield_100w_50h_r2000_spiral.csv';
backgroundFlowFileName = 'Vfield_100w_50h_r2000_spiral_relativeTolerancee-4Mesh.csv';
backgroundFlowPressure = 'Pfield_100w_50h_r2000_spiral_finerMesh.csv';


%backgroudFlowFileName = 'ComsolExport1.csv';
dat_dat = csvread(backgroundFlowFileName,10);
dat_datP = csvread(backgroundFlowPressure,10);
r = dat_dat(:,1)'*1e0;%which is x axis
z = dat_dat(:,2)'*1e0;%which is y axis
u = dat_dat(:,3)'*1e0;%which is Ux axis
v = dat_dat(:,4)'*1e0;%which is Uz axis
w = dat_dat(:,5)'*1e0;%which is Uy axis
rp = dat_datP(:,1)'*1e0;
zp = dat_datP(:,2)'*1e0;
p  = dat_datP(:,3)'*1e0;

p = p + -min(p);

U = scatteredInterpolant(r', z', u');
V = scatteredInterpolant(r', z', v');
W = scatteredInterpolant(r', z', w');
P = scatteredInterpolant(rp', zp', p');


x_chan = linspace(min(r), max(r), 500);
z_chan = linspace(min(z), max(z), 500);

rho = 1000; %kg / m^3

[qx, qz] = meshgrid(x_chan, z_chan);

qu = U(qx, qz);
qv = V(qx, qz);
qw = W(qx, qz);
qp = P(qx, qz);

qE = qp + (qu.^2 + qv.^2 + qw.^2) *rho * 0.5;
qKE = (qu.^2 + qw.^2) *rho * 0.5;
%qE = qp + qKE;


figure
[C,h] = contourf(qx, qz, qu);
clabel(C,h)
title('x velocity contour')
figure
[C,h] = contourf(qx, qz, qv);
clabel(C,h)
title('y velocity contour')
figure
[C,h] = contourf(qx, qz, qw);
clabel(C,h)
title('z velocity contour')
figure
[C,h] = contourf(qx, qz, qp);
clabel(C,h)
title('Pressure contour')
figure
[C,h] = contourf(qx, qz, qE);
clabel(C,h)
title('Energy contour')


figure
s = surf(qx, qz, qp);
%clabel(C,h)
s.EdgeColor = 'none';
title('Pressure surface')

figure
s = surf(qx, qz, qE);
%clabel(C,h)
s.EdgeColor = 'none';
title('Total Energy surface')

figure
s = surf(qx, qz, qv);
%clabel(C,h)
s.EdgeColor = 'none';
title('Through Channel Velocity')


figure
s = surf(qx, qz, qKE);
%clabel(C,h)
s.EdgeColor = 'none';
title('Kinetic Energy surface')



Pcenter = 1985; %um
Plength = 3;    %um
h = 0;          %um
Pl = Pcenter - Plength/2;
Pr = Pcenter + Plength/2;
Pt = h + Plength/2;
Pb = h - Plength/2;
Parea = Plength^2 * 1e-12; %m

Fnetx = P(Pl,h)*Parea - P(Pr,h)*Parea; %Pressures are Pa
Fnetz = P(Pcenter,Pt)*Parea - P(Pcenter,Pb)*Parea;


% velx = U(Pcenter, h);%To understand V at that point to calculate Fd
% velz = W(Pcenter, h);
mu = 8.89e-4;

Fvx = -6*pi*Plength/2*mu*U(Pcenter, h)*1e-6;
Fvz = -6*pi*Plength/2*mu*W(Pcenter, h)*1e-6;


velx = Fnetx/(6*pi*Plength/2*mu); 
velz = Fnetz/(6*pi*Plength/2*mu);

% contour plots

partX = @(Pcenter, h) abs((P(Pcenter - Plength/2,h).*Parea - P(Pcenter + Plength/2,h)*Parea)./ (-6*pi*Plength/2*mu*U(Pcenter, h)*1e-6));
partZ = @(Pcenter, h) abs((P(Pcenter,h + Plength/2).*Parea - P(Pcenter,h - Plength/2)*Parea)./ (-6*pi*Plength/2*mu*W(Pcenter, h)*1e-6));

% remove outside points were the edges of the particle are outside the
% walls
rR = r(r > min(r) + Plength/2 & r < max(r) - Plength/2 & z > min(z) + Plength/2 & z < max(z) - Plength/2);
zR = z(r > min(r) + Plength/2 & r < max(r) - Plength/2 & z > min(z) + Plength/2 & z < max(z) - Plength/2);

XR = partX(rR, zR);
ZR = partZ(rR, zR);

mx = 20;
mn = -3;

PvVx = scatteredInterpolant(rR(XR < mx & XR > mn)', zR(XR < mx & XR > mn)', XR(XR < mx & XR > mn)', 'natural');
PvVz = scatteredInterpolant(rR(ZR < mx & ZR > mn)', zR(ZR < mx & ZR > mn)', ZR(ZR < mx & ZR > mn)', 'natural');

figure
s = surf(qx, qz, PvVx(qx, qz));
%clabel(C,h)
s.EdgeColor = 'none';
title('X F_P/F_V')

figure
s = surf(qx, qz, PvVz(qx, qz));
%clabel(C,h)
s.EdgeColor = 'none';
title('Z F_P/F_V')

U = scatteredInterpolant(r', z', u', 'natural');
V = scatteredInterpolant(r', z', v', 'natural');
W = scatteredInterpolant(r', z', w', 'natural');
P = scatteredInterpolant(rp', zp', p', 'natural');

x_chan = linspace(min(r),max(r),1000);
z_chan = linspace(min(z),max(z),1000);

[qx, qz] = meshgrid(x_chan, z_chan);

qu = U(qx, qz);
qv = V(qx, qz);
qw = W(qx, qz);
qp = P(qx, qz);

qE = qp + (qu^2 + qw^2) *rho * 0.5;

figure
contourf(qx, qz, qu)
title('x velocity contour natural')
figure
contourf(qx, qz, qv)
title('y velocity contour natural')
figure
contourf(qx, qz, qw)
title('z velocity contour natural')
figure
contourf(qx, qz, qp)
title('Pressure contour natural')
figure
contourf(qx, qz, qE)
title('Energy contour natural')
