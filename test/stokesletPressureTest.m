% stokeslet and Pressure Test

clc
clear
close all

addpath('.\..\src')
import SimulationClass.LowReynoldsSimulation.*
import LowReynoldsGeometry.*

dP_exact = @(mu, U, R, r, th) 3/2 * mu*U*R/r^2 * cos(th);

sx = 0;
sy = 0;
sz = 0;
st = 0;
sp = 0;
sg = 0;

radius = 0.25e-3;
ds = 0.05e-3;

rs = evenSphere(sx, sy, sz, radius, ds);
P_b = [sx, sy, sz, st, sp, sg];

ep = ds^0.8;
%ep = ds/0.05;
a = ds/10;
mu = 8.9e-4;

% get G matrix
Ginv_mat = G_Matrix_sim_multi_S({rs}, {a}, {ep}, mu);

% get IRX
K_mat = IRX_Matrix_sim_multi({rs}, {P_b});

L_dot = K_mat * [1000 0 0 0 0 0]';

% normal vectors
rn = rs - repmat(P_b(1:3), length(rs), 1);
rn = cell2mat(mat2cell(rn, ones(length(rn*3),1), 3)')';

% get Pressure
P_mat = PressureForceStokeslets({rs}, {P_b(1:3)}, {ep});

M_mat = K_mat' *Ginv_mat * K_mat;

VG = -M_mat^-1 * K_mat' * Ginv_mat * L_dot;
VP = -K_mat'* ((P_mat));

P = zeros(1, length(rs)*3)';

%[V_Lab,V_G, V_LJ, M, mem, numS, V_P, V_Pex]  iterate_multi_PressureEx(r_, r_o, L_dot, r_n, P, a_, e_, mu, sig_LJ, e_LJ)
[V_Lab, V_G, V_LJ, M, mem, numS, V_P, P_mat] = iterate_multi_PressureEx({rs}, {P_b}, L_dot, rn, P, {a}, {ep}, mu, 0, 0);


P_max = dP_exact(mu, 1000, radius, radius, 0);
th = linspace(-pi, pi, 1000);
P_soln= dP_exact(mu, 1000, radius, radius, th);

figure
plot(th, P_soln)

%% plot pressure and velocity

[X, Y, Z] = meshgrid(-2e-3:0.05e-3:2e-3);

P = zeros(length(X(:, 1, 1)), length(X(1, :, 1)), length(X(1, 1, :)));
U = zeros(length(X(:, 1, 1)), length(X(1, :, 1)), length(X(1, 1, :)), 3);

Se_ij = @(r, e) eye(length(r)) * (r * r' + 5*e^2) / (r * r' + e^2)^(3/2) + (r' * r)/(r * r' + e^2)^(3/2);
P_j = @(r, e) r * (2 * r * r' + 5 * e^2) / (r * r' + e^2)^(5/2);

fj = Ginv_mat * L_dot;

sl = 41;

for i = 1:length(X(:, 1, 1))
    for j = 1:length(X(1, :, 1))
        %for k = 1:length(X(1, 1, :))
            k = sl;
            r = [X(i, j, k) Y(i, j, k) Z(i, j, k)];
            Ui = zeros(1,3);
            Pi = 0;
            for l = 1:length(rs)
                x_0 = rs(l, :);
                Ui = Ui + (Se_ij(r - x_0, ep) * fj(l*3-2:l*3))';
                Pi = Pi + P_j(r - x_0, ep) * fj(l*3-2:l*3);
            end
            U(i, j, k, :) = Ui;
            P(i, j, k) = Pi;
        %end
    end
end

P_inter = griddedInterpolant(X(:, :, sl)', Y(:, :, sl)', P(:, :, sl)');

x_in = sin(th);
y_in = cos(th);

P_in = P_inter(x_in, y_in);
hold on
%plot(th, P_in);
hold off

figure
contour(X(:, :, sl), Y(:, :, sl), P(:, :, sl), 'ShowText','on')
title('Pressure plot')
hold on
%plot(S_rx, S_ry);
plot(rs(:,1), rs(:,2), '*')
quiver(rs(:,1), rs(:,2), L_dot(1:3:end), L_dot(2:3:end))
hold off

figure
[c, h] = contour(X(:, :, sl), Y(:, :, sl), U(:, :, sl, 1), 'ShowText','on');
title('X vel plot')
hold on
%plot(S_rx, S_ry);
plot(rs(:,1), rs(:,2), '*')
quiver(rs(:,1), rs(:,2), L_dot(1:3:end), L_dot(2:3:end))
hold off

figure
contour(X(:, :, sl), Y(:, :, sl), U(:, :, sl, 2), 'ShowText','on')
title('Y vel plot')
hold on
%plot(S_rx, S_ry);
plot(rs(:,1), rs(:,2), '*')
quiver(rs(:,1), rs(:,2), L_dot(1:3:end), L_dot(2:3:end))
hold off

sk = 4;

figure
quiver(X(1:sk:end, 1:sk:end, sl), Y(1:sk:end, 1:sk:end, sl), U(1:sk:end, 1:sk:end, sl, 1), U(1:sk:end, 1:sk:end, sl, 2))
title('Velocity quiver plot')
hold on
%plot(S_rx, S_ry);
plot(rs(:,1), rs(:,2), '*')
quiver(rs(:,1), rs(:,2), L_dot(1:3:end), L_dot(2:3:end))
hold off

