
clc
clear
close all

addpath('.\..\src')
%addpath('.\..\src\SimulationClass')
import SimulationClass.LowReynoldsSimulation.*
import LowReynoldsGeometry.*

% sphere constants

sx = 0;
sy = 0;
sz = 0;
ds = 0.06;

radius = 1.01;

a = ds/10;
e = ds^0.9;

mu = 8.9e-4;
% init sphere

r_s = evenSphere(sx, sy, sz, radius, ds);
P_body = [sx, sy, sz];

% calc G matrix and IRX

G   = G_Matrix_sim_multi_Sarr({r_s}, {a}, {e}, mu);
IRX = IRX_Matrix_sim_multi({r_s}, {P_body});

M = IRX' * G * IRX;

dragX = M(1,1);
dragY = M(2,2);
dragZ = M(3,3);
dragExact = 6 * pi * mu * radius;

dragErr = abs(dragX - dragExact)/dragExact; 
