clc
clear
close all

xc = -10;%2000;%150, moves the particle radially (channel radius varies from 100-200 microns)
yc = 0;%4.95, It moves the particle into the channel
zc = 0;%-25, moves the particle up and down
axis = 0;
height= 50;
delta = 0.5;
L_y = 100;
ang = 1.5;%was 0.5 but sperm tail is bigger than te channel, so changed te angle to 1.5
[r_s1] = FlatPlate(xc, yc, zc, axis, height, delta, L_y);%delta=0.001, to get horizontal ellipsoid 2.5, 1.5 


for k = 1:length(r_s1)
    x1(k) = r_s1{k}(1);
    y1(k) = r_s1{k}(2);
    z1(k) = r_s1{k}(3);
end
  
figure
hold on
xlabel('X axis')
ylabel('Y axis')
zlabel('Z axis')
plot3(x, y, z, 'b*')  


