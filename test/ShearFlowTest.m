clc
clear
close all

addpath('.\..\src')
%addpath('.\..\src\SimulationClass')
import SimulationClass.LowReynoldsSimulation.*
import LowReynoldsGeometry.*

% sphere constants

rho = 1e3;%density, kg/m^3

sx = 0;
sy = 0;
sz = 0;
ds = 0.06e-6;

radius = 1.01e-6;

a = ds/10;
e = ds^0.9;
mu = 8.9e-4;

% init sphere

r_s = evenSphere(sx, sy, sz, radius, ds);
r = r_s(:,1);
z = r_s(:,3);
P_body = [sx, sy, sz];

L_dot = repelem(U(r, z), 3, 1) .* repmat([1;0;0], length(r_s), 1);

% calc G matrix and IRX

Ginv   = G_Matrix_sim_multi_Sarr({r_s}, {a}, {e}, mu);
IRX = IRX_Matrix_sim_multi({r_s}, {P_body});

M = IRX' * Ginv * IRX;

V_B = -M^-1 * IRX' * Ginv * L_dot;



