clc
clear
close all

% regularized stokeslet
Se_ij = @(r, e) eye(length(r)) * (r * r' + 5*e^2) / (r * r' + e^2)^(3/2) + (r' * r)/(r * r' + e^2)^(3/2);

% stokeslet
S_ij = @(r, f) f / (8*pi*(r*r')^(1/2)) + f * (r'*r) / (8*pi*(r*r')^(3/2));

% Doublelet
D_ij = @(r, g, b) ((g*b') * r + (g*r')*b - (b*r')*g)/(8*pi*(r*r')^(3/2)) - 3*(g*r')*(b*r')*r/(8*pi*(r*r')^(5/2));

% potential dipole
Pd_ij = @(r, q) q / (4 * pi *(r * r')^(3/2)) - 4 * q * (r' * r) / (3*pi*(r*r')^(5/2));

% Stokeslet Pressure
P_j = @(r, e) r * (2 * r * r' + 5 * e^2) / (r * r' + e^2)^(5/2);

e = [1 0 0];
h = 1;

g = @(f) 2*(f*e')*e - f;

Si_ij = @(r, f) -S_ij(r, f) - h^2*Pd_ij(r, g(f)) + 2*h*D_ij(r, e, g(f));

[X, Y, Z] = meshgrid(-1.5:0.075:1.5);

f = [1, 0, 0];
ep = 0.05;

r_0 = [0 1 0; 0.5 1 0; 1 1 0; -1 1 0; -0.5 1 0];

th = linspace(0, 2*pi);
S_rx = sin(th) * ep;
S_ry = cos(th) * ep;

P = zeros(length(X(:, 1, 1)), length(X(1, :, 1)), length(X(1, 1, :)));
U = zeros(length(X(:, 1, 1)), length(X(1, :, 1)), length(X(1, 1, :)), 3);

for i = 1:length(X(:, 1, 1))
    for j = 1:length(X(1, :, 1))
        for k = 1:length(X(1, 1, :))
            r = [X(i, j, k) Y(i, j, k) Z(i, j, k)];
            Ui = zeros(1,3);
            Pi = 0;
            for l = 1:length(r_0)
                x_0 = r_0(l, :);
                Ui = Ui + Si_ij(r - x_0, f);
                Pi = Pi + P_j(r - x_0, ep) * f';
            end
            U(i, j, k, :) = Ui;
            P(i, j, k) = Pi;
        end
    end
end

sl = 10;

figure
contour(X(:, :, sl), Y(:, :, sl), P(:, :, sl), 'ShowText','on')
title('Pressure plot')
hold on
plot(S_rx, S_ry);
hold off

figure
[c, h] = contour(X(:, :, sl), Y(:, :, sl), U(:, :, sl, 1), 'ShowText','on');
title('X vel plot')
hold on
plot(S_rx, S_ry);
hold off

figure
contour(X(:, :, sl), Y(:, :, sl), U(:, :, sl, 2), 'ShowText','on')
title('Y vel plot')
hold on
plot(S_rx, S_ry);
hold off

sk = 1;

figure
quiver(X(1:sk:end, 1:sk:end, sl), Y(1:sk:end, 1:sk:end, sl), U(1:sk:end, 1:sk:end, sl, 1), U(1:sk:end, 1:sk:end, sl, 2))
title('Velocity quiver plot')
hold on
plot(S_rx, S_ry);
hold off