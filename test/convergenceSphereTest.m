function [V_body] = convergenceSphereTest(dS)

    addpath('.\..\src', '.\..\SimulationScripts', '.\..\backgroundFlow');
    import SimulationClass.LowReynoldsSimulation.*

    
    
    % [V_Lab, V_G, V_LJ, M, mem, numS] = iterate_multi(r_, r_o, L_dot, a_, e_, mu, sig_LJ, e_LJ)
    % [errFig, E, C, Rsq] = convergenceFunction(geoFunct, dS, gam, cVar, fDir, name, verbose)

    Pos = [0, 0, 0, 0, 0, 0];
    
    backgroudFlowFileName = 'Vfield_100w_50h_r2000_spiral.csv';
    dat_dat = csvread(backgroudFlowFileName,10);
    r = dat_dat(:,1)';%which is x axis
    z = dat_dat(:,2)';%which is y axis
    u = dat_dat(:,3)'*10e6;%which is Ux axis
    v = dat_dat(:,5)'*10e6;%which is Uz axis
    w = dat_dat(:,4)'*10e6;%which is Uy axis
    
    U = scatteredInterpolant(r', z', u');
    V = scatteredInterpolant(r', z', v');
    W = scatteredInterpolant(r', z', w');
    
    r_s = generate_sphere_particle(Pos, dS, 0.5);
    
    %% interpolate velocities
    r_s_x_prime = (r_s(:,1).^2 + r_s(:,3).^2 ).^(1/2);
    u_s = -U(r_s_x_prime , r_s(:, 2));
    v_s = -V(r_s_x_prime , r_s(:, 2));
    w_s = -W(r_s_x_prime , r_s(:, 2));
    
    l_dot = [cell2mat(mat2cell([u_s v_s w_s], ones(length(v_s),1), 3)')'];
    
    V_body = iterate_multi({r_s}, {[0,0,0]}, l_dot, {dS/10}, {dS^0.9}, 0.1, 0, 0);

    
end