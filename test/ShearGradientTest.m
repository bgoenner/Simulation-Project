clc
clear
close all

addpath('.\..\src')
%addpath('.\..\src\SimulationClass')
import SimulationClass.*
import LowReynoldsGeometry.*


% This simulation test the ability of the regularized stokeslets simulation
% to produce a shear force.


% sphere constants
C_sg = 0.5;
rho = 1e3;%density, kg/m^3
Dh = 2e-4/3;
h = 0.5e-4;
w = 1e-4;
sx = 0;
% [1e-5, 0.5e-5, -0.5e-5];
sy = 0;
sz = -0.5e-5;
ds = [1.0e-6];%, 0.09e-6, 0.08e-6]; %delta

radius = 1.01e-6;
% ------------------------

xc = 0;%2000;%150, moves the particle radially (channel radius varies from 100-200 microns)
yc = 0;%4.95, It moves the particle into the channel
zc = 0;%-25, moves the particle up and down
axis = 0;
height= h;%50;
width = w;%10;
delta = radius/2;
%L_y = 20e-4;

L_y = radius*3;

%a = ds/10;
%e = ds^0.9; % these variable needed to be brought into the for loop
%because of the dependece of ds.
mu = 8.9e-4;

Umax = 1;
dPodZ = 1;

% r and z are cross-sectional components

U_chann = @(r, z) Umax.*2.*(1-(2.*r/w).^3).*(1-(2.*z/h).^2);%where alpha= h/w=0.5;
%Umax = U/((1-(r/w).^2.37).*(1-(z/h).^2));

% simple couette flow
U_couet = @(r, z) Umax./2 .* (1 + z ./ h); 

% simple poiseuille flow 
U_poise = @(r, z) Umax .* (1 - z.^2./h^2);


% ---- Velocity profile for test
%U_Ldot = U_poise;
U_Ldot = U_couet;
dt = 0.05;
iter = 30;

% init sphere

P_body = zeros(6, iter+1);
P_body(:, 1) = [sx; sy; sz; 0; 0; 0];

% for i = 1:length(ds)
%     for j = 1:length(sy)
i = 1;
j = 1;

for k = 1:iter

    ds_sim = ds(i);
    e = ds_sim^0.9;
    a = ds_sim/10;
    
    %r_sp = evenSphere(sx, sy, sz, radius, ds_sim);
    r_sp = evenSphere(P_body(1, k), sy, P_body(3, k), radius, ds_sim);
    r_ch = StraightChannel(0, width/4, 0, width, height, L_y, delta, 'Y');
    
    r = r_sp(:,1);
    z = r_sp(:,3);
    %P_body = [sx, sy, sz];

    L_dot_sp = repelem(U_Ldot(r, z), 3, 1) .* repmat([1;0;0], length(r_sp), 1);
    L_dot_ch = zeros(length(r_ch)*3, 1);
    
    L_dot = [L_dot_sp ;L_dot_ch];

    r_s = [r_sp; r_ch];
    
    % calc G matrix and IRX

    Ginv   = G_Matrix_sim_multi_S({r_s}, {a}, {e}, mu);
    IRX_sp = IRX_Matrix_sim_multi({r_sp}, {P_body});
    IRX_ch = IRX_Matrix_sim_multi({r_ch}, {P_body});

    M = [IRX_sp; IRX_ch]' * Ginv * [IRX_sp; IRX_ch]; % Resistance matrix calculation

    V_B(:, j) = -M^-1 * [IRX_sp; IRX_ch]' * Ginv * L_dot; %Body velocity

    L_dot_2 = [IRX_sp * V_B; L_dot_ch];
    
    V_B_2(:, j) = -M^-1 * [IRX_sp; IRX_ch]' * Ginv * L_dot_2;
    
    V = V_B + V_B_2;
    
    P_body(:, k+1) = P_body(:, k) + V * dt;
    
    F_SG(i) = C_sg * Umax .^2 * rho* a.^3/Dh;
    D_c(i) = 6 * pi * mu * radius;%it's not complete force

    U_b(i) = F_SG / D_c;
    
    fprintf('Iteration: %i\n', k);
    
end
%     end
% end
%% plot

U = U_Ldot;

plot_disx = 1e-5;
plot_disy = 1e-5;
plot_points = 30;

[XX, YY] = meshgrid(linspace(sx-plot_disx, sx+plot_disx, plot_points), linspace(sy-plot_disy, sy+plot_disy, plot_points));

U_u = U(XX, YY);

U_v = ones(size(XX)) * 0;

quiver(XX, YY, U_u, U_v);

% contour of velocity plot

[X_chan, Y_chan] = meshgrid(linspace(-w, w), linspace(-h, h));

U_contour = U(X_chan, Y_chan);

figure
[M_c, c] = contour(X_chan, Y_chan, U_contour);
hold on
    plot(r_ch(:,2), r_ch, 'r*');
    plot(r_sp(:,2), z, 'k*');
hold off
figure
surf(X_chan, Y_chan, U_contour);
% hold on
%     plot(r, z, '*');
% hold off

% Plot velocity profile in cross-section

Y_h = linspace(-h, h);
X_h = zeros(1, length(Y_h));

U_h = U(X_h, Y_h);
figure
plot(U_h, Y_h);

% ------------
figure
plot(P_body(1, :))
title('X Position')
figure
plot(P_body(2, :))
title('Y Position')
figure
plot(P_body(3, :))
title('Z Position')

% ------------

ssr = r_sp(:,1);
ssy = r_sp(:,2);
ssz = r_sp(:,3);

cx = r_ch(:,1);
cy = r_ch(:,2);
cz = r_ch(:,3);
figure
hold on
    plot3(ssr, ssy, ssz, '*k');
    plot3(cx, cy, cz, '*r');
hold off