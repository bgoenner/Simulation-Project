clc
clear all
close all

%
addpath('.\..\src')

test_Sphere()
test_channel()
test_ellipse()
%test_RBC()
%test_rod()
%test_cylinder()
%test_mesh()


function [] = plotGeometry(r_s)

	% removes duplicate points
    if iscell(r_s) 
        R_S = unique(cell2mat(r_s), 'rows');
    else
        R_S = r_s;
    end
    figure
    plot3(R_S(:,1), R_S(:,2), R_S(:,3), '.')

end %end plotGeometry

function [] = test_Sphere()

	xc = 0;
	yc = 0;
	zc = 0;
	radius = 1;
	delta = 0.1;

    r_s = LowReynoldsGeometry.evenSphere(xc, yc, zc, radius, delta);
    plotGeometry(r_s)

end % end test_sphere

function [] = test_curvedPlate()

    r_s = LowReynoldsGeometry;

end % end test_curvedPlate
 
function [] = test_channel()
    
    xc  = 100;
    yc  = 0;
    zc  = 0;
    r   = 10;
    w   = 2;
    h   = 2;
    ang = 360;
    ds  = 0.05;

    r_s = LowReynoldsGeometry.CurvedChannel(xc, yc, zc, r, w, h, ang, ds);
    plotGeometry(r_s)

end % end test_channel

function [] = test_rod()
    
    %r_s = LowReynoldsGeometry.evenSphere(0, 0 , 0, 1, 0.1);
    plotGeometry(r_s)

end % end test_rod

function [] = test_ellipse()
    xc  = 100;
    yc  = 0;
    zc  = 0;
    maj_a = 10;
    min_a = 5;
    off_a = 3;
    dS  = 0.1;
    
    r_s = LowReynoldsGeometry.Ellipse(xc, yc, zc, maj_a, min_a, off_a, dS);

    plotGeometry(r_s)
    
end % end test_ellipse

function [] = test_spremCell()
end % end test_spermCell

function [] = test_RBC()

end % end test_RBC
