clc
clear
close all

S_ij = @(r, e) eye(length(r)) * (r * r' + 5*e^2) / (r * r' + e^2)^(3/2) + (r' * r)/(r * r' + e^2)^(3/2);

% Doublelet
D_ij = @(r, g, b) ((g*b') * r + (g*r')*b - (b*r')*g)/(8*pi*(r*r')^(3/2)) - 3*(g*r')*(b*r')*r/(8*pi*(r*r')^(5/2));

% potential dipole
Pd_ij = @(r, q) q / (4 * pi *(r * r')^(3/2)) - 4 * q * (r' * r) / (3*pi*(r*r')^(5/2));

% Pressure term
P_j = @(r, e) r * (2 * r * r' + 5 * e^2) / (r * r' + e^2)^(5/2);

[X, Y, Z] = meshgrid(-1:0.05:1);

f = [5, 3, 0];
e = 0.05;

th = linspace(0, 2*pi);
S_rx = sin(th) * e;
S_ry = cos(th) * e;

P = zeros(length(X(:, 1, 1)), length(X(1, :, 1)), length(X(1, 1, :)));
%U = zeros(length(X(:, 1, 1)), length(X(1, :, 1)), length(X(1, 1, :)));

for i = 1:length(X(:, 1, 1))
    for j = 1:length(X(1, :, 1))
        for k = 1:length(X(1, 1, :))
            r = [X(i, j, k) Y(i, j, k) Z(i, j, k)];
            U(i, j, k, :) = S_ij(r, e) * f';
            P(i, j, k) = P_j(r, e) * f';
        end
    end
end

sl = 10;

figure
contour(X(:, :, sl), Y(:, :, sl), P(:, :, sl))
title('Pressure plot')
hold on
plot(S_rx, S_ry);
hold off

figure
contour(X(:, :, sl), Y(:, :, sl), U(:, :, sl, 1))
title('X vel plot')
hold on
plot(S_rx, S_ry);
hold off

figure
contour(X(:, :, sl), Y(:, :, sl), U(:, :, sl, 2))
title('Y vel plot')
hold on
plot(S_rx, S_ry);
hold off

sk = 4;

figure
quiver(X(1:sk:end, 1:sk:end, sl), Y(1:sk:end, 1:sk:end, sl), U(1:sk:end, 1:sk:end, sl, 1), U(1:sk:end, 1:sk:end, sl, 2))
title('Velocity quiver plot')
hold on
plot(S_rx, S_ry);
hold off