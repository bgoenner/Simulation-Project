clc
clear
close all

addpath('.\..\src')
%addpath('.\..\src\SimulationClass')
import SimulationClass.LowReynoldsSimulation.*
import LowReynoldsGeometry.*

% Generate sphere
dS = [0.5, 0.4, 0.38, 0.36, 0.34, 0.32, 0.3, 0.27, 0.25, 0.22, 0.2, 0.17, 0.15, 0.13];
radius = 2.5;

for i = 1:2
    
    deltaS = dS(i);
    
    r_s1 = unique(cell2mat(evenSphere(0, 0, 0, radius, deltaS)), 'rows');
    r_s2 = unique(cell2mat(evenSphere(5, 5, 5, radius, deltaS)), 'rows');% adding more varaibles
    
    % build different G_Matrix Tests
   
    % function constants
    
    a = deltaS / 10;
    e = deltaS^0.9;
    mu = 0.1;
    
    
    P = [0,0,0];
    l_dot = [0,0,0];
    a_LJ = 0;
    e_LJ = 0;
   
    % signature; function [G] = G_Matrix_sim_multi(r_s, a_, e_, mu)
    base_G_Matrix = @(r_s) G_Matrix_sim_multi(r_s, {a, a}, {e, e}, mu);
    
    % signature; function [G] = G_Matrix_sim_multi_S(r_s, a_, e_, mu)
    S_G_Matrix = @(r_s) G_Matrix_sim_multi_Sarr(r_s, {a, a}, {e, e}, mu);
    %GPU_G_Matrix = @(r_s) G_Matrix_sim_GPU({r_s}, {a}, {e}, mu);
    GPU_G_Matrix = @(r_s) G_Matrix_sim_GPU(r_s, {a, a}, {e, e}, mu);
    %GPU_G_Matrix_P = @(r_s) G_Matrix_sim_GPU_Pool(r_s, {a, a}, {e, e}, mu);
    
    % Call first function
    
    [G_base_time(i), base_G] =  stopWatch({r_s1, r_s2}, base_G_Matrix);
    
    
    % Call other functions
    
    [S_G_time(i), S_G] = stopWatch({r_s1, r_s2}, S_G_Matrix);
    %[GPU_G_time(i), GPU_G] = stopWatch(r_s, GPU_G_Matrix);
    [GPU_G_time(i), GPU_G] = stopWatch({r_s1, r_s2}, GPU_G_Matrix);
    %[GPU_GP_time(i), GPU_GP] = stopWatch({r_s1, r_s2}, GPU_G_Matrix_P);
    % Calculate error
    
    base_G32 = single(base_G);
    
    G_err(1, i) = sum(sum((base_G - S_G).^2));
    %G_err(2, i) = sum(sum((base_G - Sarr_G).^2));
    G_err(2, i) = sum(sum((base_G32 - GPU_G).^2));
    %G_err(3, i) = sum(sum((base_G32 - GPU_GP).^2));
    
    % Put times in a table
    numS(i) = length(r_s1) + length(r_s2);
    
    
    disp(strcat(['Finished iteration :' num2str(i)]))
end

figure
plot(G_base_time)
hold on
    plot(S_G_time)
    %plot(Sarr_G_time)
    plot(GPU_G_time)
    %plot(GPU_GP_time)
hold off
xlabel('Iteration Number')
ylabel('Time in seconds')
legend('Base', 'S_G', 'GPU_G', 'GPU_G Pool')

figure
plot(numS, G_base_time)
hold on
    plot(numS, S_G_time)
    %plot(Sarr_G_time)
    plot(numS, GPU_G_time)
    %plot(numS, GPU_GP_time)
hold off
xlabel('Number of Stokeslets')
ylabel('Time in seconds')
legend('Base', 'S_G', 'GPU_G', 'GPU_G Pool')


figure
plot(G_err(1, :))
hold on
    plot(G_err(2, :))
    %plot(G_err(3, :))

hold off

legend('S_G', 'GPU_G', 'GPU_G Pool')



