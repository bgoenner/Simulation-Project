clc
clear
close all

addpath('.\..\src')
import SimulationClass.LowReynoldsSimulation.*
import LowReynoldsGeometry.*

sx = 0;
sy = 0;
sz = 0;
st = 0;
sp = 0;
sg = 0;

radius = 1;
ds = 0.2;

rs = evenSphere(sx, sy, sz, radius, ds);
P_b = [sx, sy, sz, st, sp, sg];

u_test = repmat([0 0 1], length(rs), 1) * 2;

K = IRX_Matrix_sim_multi({rs}, {P_b});

U = K * [2 0 0 0 0 0]'; 
