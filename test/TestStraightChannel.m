clc
clear
close all


addpath('./../src');

import LowReynoldsGeometry.*

% xc = 0;%2000;%150, moves the particle radially (channel radius varies from 100-200 microns)
% yc = 0;%4.95, It moves the particle into the channel
% zc = 0;%-25, moves the particle up and down
% axis = 0;
% height= 5e-3;%;
% width = 5e-3;%;r_s1
%% Simulation test case

% ds1 = 0.12e-6;
% ds2 = 1.1e-6;%1.6;
% dS_chan       = ds2;
% eps_chan      = dS_chan^0.9;
% width    = 100e-6;    % um   
% height   = 50e-6;     % um 
% 
% L_y       = 0.00001;%250;
% xc= 50e-6;
% yc = 0;
% zc= 0;
% 
% [r_s1] = StraightChannel(xc, yc, zc, width, height, L_y, dS_chan, 'Y'); %delta=0.001, to get horizontal ellipsoid 2.5, 1.5 

%% Original test case

xc = 0;%2000;%150, moves the particle radially (channel radius varies from 100-200 microns)
yc = 0;%4.95, It moves the particle into the channel
zc = 0;%-25, moves the particle up and down
delta = 1e-3;
axis = 0;
height= 5e-3;%;
width = 5e-3;%;r_s1
L_y       = 0.1;%250;
[r_s1] = StraightChannel(xc, yc, zc, width, height, L_y, delta, 'Y');

%% plotting code
for k = 1:length(r_s1)
    x1(k) = r_s1(k, 1);
    y1(k) = r_s1(k, 2);
    z1(k) = r_s1(k, 3);
end
  
figure
hold on
xlabel('X axis')
ylabel('Y axis')
zlabel('Z axis')
plot3(x1, y1, z1, 'b*')  
