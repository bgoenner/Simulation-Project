clc
clear
close all


addpath('.\..\src')
%addpath('.\..\src\SimulationClass')
import SimulationClass.LowReynoldsSimulation.*
import LowReynoldsGeometry.*


radius = 2;

deltaS = 0.4;

P1 = [0, 0, 0];
P2 = [3, 3, 3];

r_s1 = unique(cell2mat(evenSphere(P1(1), P1(2), P1(3), radius, deltaS)), 'rows');
r_s2 = unique(cell2mat(evenSphere(P2(1), P2(2), P2(3), radius, deltaS * 1.2)), 'rows');

F_base = LennardJoneForce_sim({r_s1, r_s2}, 2, 3);
F_GPU = LennardJoneForce_sim_GPU({r_s1, r_s2}, 2, 3);


F_err(1) = sum(sum((F_base - F_GPU).^2));

%diff = F_base - F_GPU;