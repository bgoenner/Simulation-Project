function [time, output] = stopWatch(inputs, testFunction)


    start = cputime;
    
    output = testFunction(inputs);
    
    time = cputime - start;
    
end