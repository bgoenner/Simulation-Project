clc
clear
close all

addpath('.\..\src');
addpath('.\..\');
addpath('.\..\SimulationScripts')

import LowReynoldsGeometry.*
import SimlulationClass.LowReynoldsSimulation.*

% [V_Lab, V_G, V_LJ, M, mem, numS] = iterate_multi(r_, r_o, L_dot, a_, e_, mu, sig_LJ, e_LJ)
% [errFig, E, C, Rsq] = convergenceFunction(geoFunct, dS, gam, cVar, fDir, name, verbose)

dS = [0.3, 0.27, 0.24];
gam = 1:0.1:1.5;

a = @(dS) convergenceSphereTest(dS);

%[Efig, E, C, Rsq] = convergenceFunction(a, dS, gam, 1, '.\', 'testCase', '.jpg', 1);

dS1 = [0.3, 0.27, 0.24];
gam = 0.7:0.2:1.9;
ds1 = 0.2;
ds2 = 1.5;

RBC1 = @(dS) RBCconvergenceFunction(dS, ds2);
RBC2 = @(dS) RBCconvergenceFunction(ds1, dS);

RBCDir = '.\.\..\..\..\Marzieh\Simulation\ConvergenceResults\RedBloodCell';

RBCDir = '.\..\..\SimulationTests';

if exist(RBCDir)

    [E1, C1, Rsq1] = convergenceFunction(RBC1, dS1, gam, 1, RBCDir, 'RBCconvergence', '.jpg');

    dS2 = [2, 1.75, 1.5, 1.3, 1.0];

    [E2, C2, Rsq2] = convergenceFunction(RBC2, dS2, gam, 1, RBCDir, 'RBCconvergence', '.jpg');
end 
%[Efig, E, C, Rsq] = convergenceFunction(a, dS, gam, 1, '.\', 'testCase', '.jpg', 1);

%[Efig, E, C, Rsq] = convergenceFunction(a, dS, gam, 1, '.\', 'testCase', '.jpg', 1);