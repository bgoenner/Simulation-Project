clc
clear
close all

addpath('.\..\src')
%addpath('.\..\src\SimulationClass')
import SimulationClass.LowReynoldsSimulation.*
import LowReynoldsGeometry.*

C = 100;

U = @(y) [C * y, 0, 0];%linear
%U = @(y) [C * y^2, 0, 0];%Quadratic

% sphere constants
for j = 1:10
sx = 0;
sy = 0;
sz(j) = 0.05e-6*j;
ds = 0.1e-6;

radius = 1.01e-6;

a = ds/10;
e = ds^0.9;
mu = 8.9e-4;

% init sphere

r_s = evenSphere(sx, sy, sz, radius, ds);
r = r_s(:,1);
z = r_s(:,3);
P_body = [sx, sy, sz];

L_dot = zeros(3*length(z),1);

for i = 1:length(z)
    L_dot(i*3-2:i*3, 1) = U(z(i))';
end
% calc G matrix and IRX

quiver(r, z, L_dot(1:3:end), L_dot(2:3:end))

Ginv   = G_Matrix_sim_multi_Sarr({r_s}, {a}, {e}, mu);
IRX = IRX_Matrix_sim_multi({r_s}, {P_body});

M = IRX' * Ginv * IRX;

V_B(:, j) = -M^-1 * IRX' * Ginv * L_dot;

end

plot(sz, V_B(3,:));



