
F_0 = 0.8e-15;
sig = 2e1;


%
LJ = @(rm) F_0 ./ 6 .* ((sig./rm).^12-(sig./rm).^6);

r = linspace(-1e1, 1e1, 1e6);

y1 = LJ(r);

plot(r, y1)
%
F_0 = 0.8e-14;
sig = 2e1;
LJ = @(rm) F_0 ./ 6 .* ((sig./rm).^12-(sig./rm).^6);

y2 = LJ(r);
hold on
plot(r, y2)
hold off
%
F_0 = 0.8e-15;
sig = 4e1;
LJ = @(rm) F_0 ./ 6 .* ((sig./rm).^12-(sig./rm).^6);

y3 = LJ(r);
hold on
plot(r, y3)
hold off

legend('y1', 'y2', 'y3')

max(y1)
max(y2)
max(y3)
