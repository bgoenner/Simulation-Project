%Sides
function [R_S] = CylinderSection(xc, yc, zc, R, height, delta, angle)%Cylindersectfunction(R, height, delta), xc, yc,zc are center of cylinder

    n = 2*R*angle*pi/(180*delta);%Number of points

    x_ = [];
    y_ = [];
    z_ = [];
    
    x = @(the) R*cos(the);
    y = @(the) R*sin(the);
    %z = @(the) z;
    
    theta = linspace(-angle * pi /360, angle * pi /360, n);%pi gives us cylinder, pi/2 to give us 1/4 cylinder, Does pi/4 give us 1/8 cylinder?!

    h = linspace(0, height, height/delta);
    
    for i = 1:1:length(h)   
        
        for j = 1:1:length(theta)
            if mod(i, 2) == 0
            %   g = pi; %pi gives us cylinder but half rings in different heights,ip/2?gives us half of cylinder
%                 g = pi/270;%denominator more than 180 gives us 1/4 cylinder shell(90 degree)
                %g = pi/8;%denominator        gives us 1/8 cylinder shell(45 degree)
                g = delta/(2*pi*R);
            else
                g = 0;
            end
            
            x_ = [x_, xc+ x(theta(j) + g)];
            y_ = [y_, yc+ y(theta(j) + g)];
            z_ = [z_, zc + h(i)];
        end
        
    end
    
    R_S = cell(length(x_), 1);
    
    for i = 1:1:length(x_)
        R_S{i} = [x_(i), y_(i), z_(i)];
    end

end

