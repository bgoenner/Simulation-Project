%addpath('.\..\src')

function[R_S] = StraightChannel(x_center, y_center, z_center, width, height, L_channel, dS, ax)% X_channel,L_channel??
 %Creates a plot of the spiral Channel
 
 
    import LowReynoldsGeometry.*
    
    % FlatPlate(xc, yc, zc, axis, height, delta, L_y)

    if ax == 'X'
        ax1 = 'X';
        ax2 = 'Y';
        [r_s1] = LowReynoldsGeometry.FlatPlate(x_center, y_center, z_center+height/2, ax1, height, dS, L_channel) ; %Top
        [r_s2] = LowReynoldsGeometry.FlatPlate(x_center, y_center, z_center-height/2, ax1, height, dS, L_channel) ;%Bottom 
        [r_s3] = LowReynoldsGeometry.FlatPlate(x_center, y_center-width/2, z_center, ax2, L_channel, dS, width) ;%Left, x_center+L_channel/2, last term: L_channel/2
        [r_s4] = LowReynoldsGeometry.FlatPlate(x_center, y_center+width/2, z_center, ax2, L_channel, dS, width) ;%Right, , last term: L_channel/2
    elseif ax == 'Y'
        ax1 = 'Y';
        ax2 = 'Z';
        [r_s1] = LowReynoldsGeometry.FlatPlate(x_center, y_center, z_center+height/2, ax1, width, dS, L_channel) ; %Top
        [r_s2] = LowReynoldsGeometry.FlatPlate(x_center, y_center, z_center-height/2, ax1, width, dS, L_channel) ;%Bottom 
        [r_s3] = LowReynoldsGeometry.FlatPlate(x_center, y_center-width/2, z_center, ax2, height, dS, L_channel) ;%Left, x_center+L_channel/2, last term: L_channel/2
        [r_s4] = LowReynoldsGeometry.FlatPlate(x_center, y_center+width/2, z_center, ax2, height, dS, L_channel) ;%Right, , last term: L_channel/2
    elseif ax == 'Z'
        ax1 = 'Z';
        ax2 = 'Y';
        [r_s1] = LowReynoldsGeometry.FlatPlate(x_center, y_center, z_center+height/2, ax1, height, dS, L_channel) ; %Top
        [r_s2] = LowReynoldsGeometry.FlatPlate(x_center, y_center, z_center-height/2, ax1, height, dS, L_channel) ;%Bottom 
        [r_s3] = LowReynoldsGeometry.FlatPlate(x_center, y_center-width/2, z_center, ax2, width, dS, L_channel) ;%Left, x_center+L_channel/2, last term: L_channel/2
        [r_s4] = LowReynoldsGeometry.FlatPlate(x_center, y_center+width/2, z_center, ax2, width, dS, L_channel) ;%Right, , last term: L_channel/2
    end
    
	% call functions to build each wall -->straight channel
    
    % FlatPlate(xc, yc, zc, axis, height, delta, L_y) 
    
%     [r_s1] = LowReynoldsGeometry.FlatPlate(x_center, y_center-width/2, z_center-height/2, 'Y', height, dS, L_channel) ; %Bottom  %%channel length
%     [r_s2] = LowReynoldsGeometry.FlatPlate(x_center, y_center+width/2, z_center+height/2, 'Y', height, dS, L_channel) ;%Left 
%     [r_s3] = LowReynoldsGeometry.FlatPlate(x_center, y_center-width/2, z_center, 'Z', width, dS, L_channel) ;%Top  %%channel length
%     [r_s4] = LowReynoldsGeometry.FlatPlate(x_center, y_center+width/2, z_center, 'Z', width, dS, L_channel) ;%Right  

    % old implementation
    %[r_s1] = LowReynoldsGeometry.FlatPlate(x_center+width/2, y_center, z_center+height/2, 'Y', height, dS, L_channel) ; %Top
    %[r_s2] = LowReynoldsGeometry.FlatPlate(x_center-width/2, y_center, z_center-height/2, 'Y', height, dS, L_channel) ;%Bottom 
    %[r_s3] = LowReynoldsGeometry.FlatPlate(x_center+L_channel/2, y_center-height/2, z_center, 'Z', width, dS, height) ;%Left, x_center+L_channel/2, last term: L_channel/2
    %[r_s4] = LowReynoldsGeometry.FlatPlate(x_center-L_channel/2, y_center+height/2, z_center, 'Z', width, dS, height) ;%Right, , last term: L_channel/2
    
    %[r_s1] = LowReynoldsGeometry.FlatPlate(x_center, y_center, z_center+height/2, ax1, height, dS, L_channel) ; %Top
    %[r_s2] = LowReynoldsGeometry.FlatPlate(x_center, y_center, z_center-height/2, ax1, height, dS, L_channel) ;%Bottom 
    %[r_s3] = LowReynoldsGeometry.FlatPlate(x_center, y_center-width/2, z_center, ax2, width, dS, L_channel) ;%Left, x_center+L_channel/2, last term: L_channel/2
    %[r_s4] = LowReynoldsGeometry.FlatPlate(x_center, y_center+width/2, z_center, ax2, width, dS, L_channel) ;%Right, , last term: L_channel/2
    
	% since the output of the functions are cells the format must be changed
    % combine structure
    R_S = unique(cell2mat([r_s1; r_s2; r_s3; r_s4]), 'rows');
    %R_S = unique(cell2mat([r_s1; r_s2]), 'rows');

    r_s1 = [];
    r_s2 = [];
    r_s3 = [];
    r_s4 = [];

end   







