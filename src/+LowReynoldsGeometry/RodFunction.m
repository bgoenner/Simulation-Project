%RodFunction
function [R_S] = RodFunction(xp, yp, zp, R, L, ds)

[r_s6] = CircularPlateFunction(xp, yp, zp - L/2, R, 360, ds); 
[r_s7] = Cylindersectfunction(xp, yp, zp - L/2 + ds, R, L - 2*ds, ds, 360);
[r_s8] = CircularPlateFunction(xp, yp, zp + L/2, R, 360, ds);  

[R_S] = [r_s6;r_s7;r_s8];

end