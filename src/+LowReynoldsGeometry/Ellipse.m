function [R_S] = EllipseFunction(xe, ye, ze, major, minor, off, delta)%was (major, minor, off, delta)

    n = off*pi/delta;

    phi = linspace(0, pi, n);

    x_ = [];
    y_ = [];
    z_ = [];

    x = @(the,ph) major*cos(the) * sin(ph);
    y = @(the,ph) minor*sin(the) * sin(ph);
    z = @(the,ph) off*cos(ph);

    for i = 1:1:n
        
        len = calcLength(x, y, phi(i), [0, 2*pi], 100);
                
               
        m = len/delta;
        if m < 1
            m = 1;
        end
        
        theta = linspace(0, 2*pi, m);
        
        for j = 1:1:length(theta)
            if mod(i, 2) == 0
                g = pi;
            else
                g = 0;
            end
            
            x_ = [x_, xe + x(theta(j) + g, phi(i))];%
            y_ = [y_, ye + y(theta(j) + g, phi(i))];% 
            z_ = [z_, ze + z(theta(j), phi(i))];%
        end
    end
    
    R_S = cell(length(x_), 1);
    
    for i = 1:1:length(x_)
        R_S{i} = [x_(i), y_(i), z_(i)];
    end

end
 
function len = calcLength(funct1, funct2, phi, range, n)
    len = 0;
    
    LenDiff = (max(range) - min(range))/n;
    
    for i = min(range)+LenDiff:LenDiff:max(range)
        dx = funct1(i, phi) - funct1(i-LenDiff, phi);
        dy = funct2(i, phi) - funct2(i-LenDiff, phi);
        len = len + sqrt(dx^2 + dy^2);
    end

end