
function [R_S] = evenSphere(xp, yp, zp, R, delta)%Having xp, yp, zp to specify Sphere center in the spiral channel

    n = R*pi/delta;

    phi = linspace(0, pi, n);

	% initializes the empty arrays
    x_ = [];
    y_ = [];
    z_ = [];

	% Functions that define the sphere points given theta and phi
    x = @(the,ph) R*cos(the) * sin(ph);
    y = @(the,ph) R*sin(the) * sin(ph);
    z = @(the,ph) R*cos(ph);

    for i = 1:1:n
        
		% calculates the arc length around a ring of the sphere
        len = calcLength(x, y, phi(i), [0, 2*pi], 100);
        
		% determines the amount of points in the ring
        m = len/delta;
        
		% in some cases such as the top and bottom the ring length has a value < delta
		% This allows for a point to be made at the top and bottom most areas of the sphere
        if m < 1
            m = 1;
        end
        
		% gets theta spacing for arround the sphere
        theta = linspace(0, 2*pi, m);
        
        for j = 1:1:length(theta)
			% offsets the points on consecutive rings, so that there isn't a clump at the start of each ring
            if mod(i, 2) == 0
                g = pi;
            else
                g = 0;
            end
            
			% fills the arrays from theta and phi
            x_ = [x_, xp + x(theta(j) + g, phi(i))];
            y_ = [y_, yp + y(theta(j) + g, phi(i))];
            z_ = [z_, zp + z(theta(j), phi(i))];
        end
    end
    
	% Combines the arrays into a 3 x n structure where the columns are [x, y, z] for each point
	
    R_S = cell(length(x_), 1);
    
    for i = 1:1:length(x_)
        R_S{i} = [x_(i), y_(i), z_(i)];
    end

	% removes duplicate points
    R_S = unique(cell2mat(R_S), 'rows');
end

% a function that calculates an arc length

function len = calcLength(funct1, funct2, phi, range, n)
    len = 0;
    
    LenDiff = (max(range) - min(range))/n;
    
    for i = min(range)+LenDiff:LenDiff:max(range)
        dx = funct1(i, phi) - funct1(i-LenDiff, phi);
        dy = funct2(i, phi) - funct2(i-LenDiff, phi);
        len = len + sqrt(dx^2 + dy^2);
    end

end
