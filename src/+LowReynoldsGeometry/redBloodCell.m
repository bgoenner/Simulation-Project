%Red Blood Cell 

function [R_S] = redBloodCell(xp, yp, zp, delta)%Having xp, yp, zp to specify Sphere center in the spiral channel

%Define the constants 
D0 = 7.82; % um, cell diameter
a0 = 0.0518;% constant in the equation
a1 = 2.0026;% constant in the equation
a2 = -4.491; % constant in the equation

%Generate a set of points along a radius
%Generate a set of approximately evenly spaced x and y coordinates on a disk in polar coordinates. 
noR = round(D0 / (2* delta)); %number of points on disc radius(D0/2)
rMax = D0/2;
% generate a set of radei
r = linspace( 0,rMax, noR )';% 
% Add an additional point before the last radius. This improves the
% apreance of the surface.
fact = 0.8;
radd = (1-fact) * r(end-1) + fact * r(end);
r = [r(1:end-1); radd; r(end)];
% Use the step in the radius as a characteristic length
dr = r(2) - r(1); 
%Compute the polar coordinates for points on the disk 
Rs = [];
Phi = [];
% Do the computation for all the points except those exactly at the
% maximum radius. This is to avoid possible complex numbers due to
% numerical errors.
for k=1:noR
 % compute the circumfrential length
 L = 2 * pi * r(k);
 % determine the number of points if constant length is to be
 % obtained
 noPhi = round( L / dr );%L=dr*Phi
 phi = linspace( 0, 2 * pi * (noPhi - 1)/ noPhi, noPhi )';

 Rs = [Rs; r(k) * ones( noPhi, 1) ];
 Phi = [Phi; phi];
end

%Convert to cartesian coordinates 
[x, y] = pol2cart( Phi, Rs ); 
%Compute the z coordinates
t = x.^2 + y.^2;
z = D0 * sqrt( 1 - 4 * t / D0^2 ) .* (a0 + (a1 * t) / D0^2 + (a2 * t.^2) / D0^4);
%the avg unstressed shape of a single RBC measured in the experiments by
%evans & skalak is bicincave and is described by this equation
% Now do the computation for the points at the maximum radius
L = 2 * pi * rMax;
noPhi = round( L / dr ); 
phi = linspace( 0, 2 * pi * (noPhi - 1)/ noPhi, noPhi )';
[xm, ym] = pol2cart(phi, rMax * ones( noPhi, 1));
zm = zeros( noPhi, 1);
% Concatinate the coordinates
x = [x; xm];
y = [y; ym];
z = [z; zm]; 

x = [x; x]+xp;
y = [y; y]+yp;
z = [z; -z]+zp;

R_S = cell(length(x), 1);

for i = 1:1:length(x)
    R_S{i} = [x(i), y(i), z(i)];
end

%Perform Delauney Tesselation
%tri = delaunay( x, y ); 

%Present the results 
% figure;
% S1 = trisurf( tri, x, y, z, 'FaceColor', 'r', 'FaceLighting', 'Phong' );
% material shiny;
% hold on;
% trisurf( tri, x, y, -z, 'FaceColor', 'r', 'FaceLighting', 'Phong' );
% material shiny;
% axis off;
% camlight('headlight');
% axis equal; 
end