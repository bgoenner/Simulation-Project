%Top_Bottom Curved Plate 
function [R_S] = CurvedPlate(xc, yc, zc, IR, OR, Ang, z, delta)

    n = (OR-IR)/delta;%Number of points
    
    Ang = (Ang * pi)/180;%Radians to Degree, 1 degree = pi/180;

	% The positions around a circle are sin and cos functions for inputs X and Y
    x = @(the, R) R*cos(the);%
    y = @(the, R) R*sin(the);%
    
	% initializes the empty arrays
    x_ = [];
    y_ = [];
    z_ = [];

    %IR; OR; OR>IR; arcl; Ang;% Angle of the sector to which Plate needed, Ang = 90 or 45 ...;  
    
    % This defines the rings of arcs to form the plate bottom
    r = linspace(IR, OR, n);%we have variety of redia

    arcl = 2*r.*Ang;%Arclength for each radius , r(i).....arcl(i)
    
    for i = 1:1:length(r)
        
        if rem(i,2) == 0
			% calculates the number of points around the ith arc using the delta
            m = arcl(i)/delta;%Number of points
            theta = linspace(-Ang/2, Ang/2, m);
        else
            m = (arcl(i) - 2*delta)/delta;%Number of points
            deldeg = delta/(2*pi*r(i));
            theta = linspace(-Ang/2+deldeg, Ang/2-deldeg, m);
        end
        
        %m = arcl(i)/delta;%Number of points  
        %theta = linspace(-Ang/2, Ang/2, m);
        
        for j = 1:1:length(theta)
            
			% fills the arrays with the arch points
            x_ = [x_, xc + x(theta(j), r(i))];
            y_ = [y_, yc + y(theta(j), r(i))];
            z_ = [z_, zc + z];
           
        end
    end
    
	% combines the arrays (vectors) into a 3 x n structure that has
	% each row a single point on the structure
    R_S = cell(length(x_), 1);
    
    for i = 1:1:length(x_)
        R_S{i} = [x_(i), y_(i), z_(i)];
    end

end