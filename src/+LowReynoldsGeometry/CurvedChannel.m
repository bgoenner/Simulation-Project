function [r_s] = CurvedChannel(x_center, y_center, z_center, R_channel, width, height, ang_channel, dS)

    % Creates a plot of the spiral channel 

    import LowReynoldsGeometry.*
    


    %[r_s1] = LowReynoldsGeometry.CurvedPlate(x_center-R_channel, y_center, z_center, R_channel-width/2, R_channel+width/2, ang_channel, z_center-height/2, dS); %0 changed to -25 % 295points
    %[r_s2] = LowReynoldsGeometry.CylinderSection(x_center-R_channel ,y_center, z_center-height/2, R_channel-width/2, height, dS, ang_channel);% % 136points, Cylindersectfunction(2050,25,s);%50 changed to 25
    %[r_s3] = LowReynoldsGeometry.CurvedPlate(x_center-R_channel, y_center, z_center, R_channel-width/2, R_channel+width/2, ang_channel, z_center+height/2, dS);%% 295points , 50 changed to 25

	% call 4 different functions to build each wall
	%CurvedPlate(xc, yc, zc, IR, OR, Ang, z, delta)
    %CylinderSection(xc, yc, zc, R, height, delta, angle)
    [r_s1] = LowReynoldsGeometry.CurvedPlate(x_center-R_channel, y_center, z_center, R_channel-width/2+dS, R_channel+width/2-dS, ang_channel, z_center-height/2, dS); 
    [r_s2] = LowReynoldsGeometry.CylinderSection(x_center-R_channel ,y_center, z_center-height/2, R_channel-width/2, height, dS, ang_channel);
    [r_s3] = LowReynoldsGeometry.CurvedPlate(x_center-R_channel, y_center, z_center, R_channel-width/2+dS, R_channel+width/2-dS, ang_channel, z_center+height/2, dS);
    [r_s4] = LowReynoldsGeometry.CylinderSection(x_center-R_channel, y_center, z_center-height/2, R_channel+width/2, height, dS, ang_channel);

	% since the output of the functions are cells the format must be changed
    % combine structure
    r_s = unique(cell2mat([r_s1; r_s2; r_s3; r_s4]), 'rows');

    r_s1 = [];
    r_s2 = [];
    r_s3 = [];
    r_s4 = [];

end