function [r_s] = surfaceMeshSTL(geometry, dSmax, dSmin)

    % import the geometry as a file path, a file class, or a PDE model
    disp('Incomplete function')
    return

    if class(geometry) == 'char'
        % initialize PDE model
        model = createpde(3);

        % import STL
        importGeometry(model, './ThumbScrew.STL')
        
    elseif class(geometry) == 'pde.PDEModel'
        
        % initialize PDE model
        model = geometry;
        
    else
        disp('Not a valid geometry input')
        a = class(geometry);
        disp('A class of type ', a, ' was passed in the function')
        throw(inValidFileException)
        
    end


    % Generate mesh using geometry
    %generateMesh(model)
    %pdeplot3D(model)

    % Generate mesh using max element size 0.5
    mesh = generateMesh(model, 'Hmax', dSmax, 'Hmax' dSmin);
    %figure
    %pdeplot3D(model)

    % get node ids by face ID
    nodeIDs = findNodes(mesh, 'region', 'face', 1:10);

    % get node points
    nodes = mesh.Nodes(1:3, nodeIDs);

end