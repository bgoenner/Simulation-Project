%To Make straight channel
function [R_S] = FlatPlate(xc, yc, zc, axis, height, delta, L_y)%RectangularSfunction(X, Y, delta)%delta will be determined by doing convergence study
        
    ny = L_y/delta;%Number of points on Y axis,
    nz = height/delta;%Number of points on Z axis,

    Y_rot = [0, 1, 0; 0, 0, -1; 1, 0, 0];%plate faces Y axis, I do not understand it!
    
    Z_rot = [0, -1, 0; 1, 0, 0; 0, 0, 1];%plate faces Z axis

    % initialize arrays
    x_ = [];
    y_ = [];
    z_ = [];
    
     
    l = linspace(-height/2, height/2, nz);
    for i = 1:1:length(l)   
        
        h = linspace(-L_y/2, L_y/2, ny);
        
        for j = 1:1:length(h)
            
            m = 0;
            
            if mod(j, 2)
                m = delta/2;
            end
            
            x_ = [x_, 0];
            y_ = [y_, h(j)];
            z_ = [z_, l(i)];
           
        end
        
    end
    
    if axis == 'X'
        % do nothing
    elseif axis == 'Y'
        rot_plot = Y_rot * [x_', y_', z_']';
        x_ = rot_plot(1, :);
        y_ = rot_plot(2, :);
        z_ = rot_plot(3, :);
    elseif axis == 'Z'
        rot_plot = Z_rot * [x_', y_', z_']';
        x_ = rot_plot(1, :);
        y_ = rot_plot(2, :);
        z_ = rot_plot(3, :);
    else
        % throw exception
    end
    
    R_S = cell(length(x_), 1);
    
    for i = 1:1:length(x_)
        R_S{i} = [xc + x_(i), yc + y_(i), zc + z_(i)];
    end

end
