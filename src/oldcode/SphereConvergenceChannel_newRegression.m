% SphereConvergenceChannel_NewRegression.m
% Efigure = function(geoFunction, dS, gam, figSavePath, verbose)
% conv_var = geofunction(dS)
 
clc
clear
close all

% saveas(strcat([fDir, test, date, '_i', n, '_v', conv_var, gType, fFormat]))

cVar = 1;

% remember to change both dir and file name
%fileSavePath = 'C:\Users\bg\Box Sync\Simulation\bg\';
%fDir = 'C:\Users\bg\Box Sync\Simulation\SphereCaseStudy\ConvergncePlots\09192018\';
fDir = '.\';
test = 't19_radius_0_5';
date = '10082018_';
fFormat = '.jpg';

addpath('.\..');
import LowReynoldsGeometry.*
import SimulationClass.*

% sphere intial parameters
% position
xs = 2.0300e3 + 20;
ys = -0.0008e3;
zs = 0e3;

% radius
rs = 0.5;
% delta 


% create interpolant objects

%% Background flow

%initialize background flow
dat_dat = csvread('Vfield.csv',1);
r = dat_dat(:,1)';%which is x axis
z = dat_dat(:,2)';%which is y axis
u = dat_dat(:,3)'*10e6;%which is Ux axis
v = dat_dat(:,5)'*10e6;%which is Uz axis
w = dat_dat(:,4)'*10e6;%which is Uy axis

U = scatteredInterpolant(r', z', u');
V = scatteredInterpolant(r', z', v');
W = scatteredInterpolant(r', z', w');

dS1 = [0.35, 0.33, 0.3, 0.28, 0.25, 0.22, 0.18];
%dS1 = [0.4, 0.37, 0.35];%, 0.33, 0.3];

ang = 0.2;
ang0 = ang;
dAng = [0, 0.05, 0.1];

dS2 = [2, 1.75, 1.5, 1.3, 1.0];
s2 = dS2(1);


% channel center

% channel radius

% channel angle in radians


% delta time
%dt = t(2) - t(1);

% intial sphere position and velocity
P_body(1,:) = [xs, ys, zs, 0, 0, 0]';  % initial position
V_body(1,:) = [0, 0, 0, 0, 0, 0]';     % initial velocity is zero

% channel center
CC = [2000, 0 , 0];

% fluid viscosity
mu = 1.002*10^-4;
a = 0;


for conv_var = 1:2
   
    try % in case of out of memory error though it will catch all errors which may make debugging tough
    clear conv
    clear Rsq
    clear E
    clear C
    clear numS
    
    if conv_var == 1
        var = dS1;
    elseif conv_var == 3
        var = dAng;
    elseif conv_var == 2
        var = dS2;
    end 
    
    %for n = 1:length(var)


        % what needs to be changed in a new simulation
        % inital geometry definition

        % simulation parameters




        %% intialize geometry

        % move to inside of the loop to reduce duplication of code
        %[r_s] = unique(cell2mat(evenSphere(xs, ys, zs, rs, ss)), 'rows');% initilize sphere

        % combine structure

        %% geometry intial velocities
        %v_s  = ones(length(r_s),1) * 3;

        % Test with different LJ parameters
        % set these to zero if they are not needed
        e_LJ = 0;
        a_LJ = 0;

        %% do not modify
        % create interpolant objects

        gam = 0.7:0.2:2;
        
        for gamV = 1:length(gam)
                        
            for r = 1:length(var)
            
                start = cputime;

                %% convergence variable deltas
                if conv_var == 1
                    s1 = dS1(r)^gam(gamV);
                    e1 = s1^0.9;
                    %sx = s1;
                elseif conv_var == 3
                    ang_iter = ang0 + dAng(r)^gam(gamV);
                    %sx = ang_iter;
                    ang = ang_iter;
                elseif conv_var == 2
                    s2 = dS2(r)^gam(gamV);
                    e2 = s2^0.9;
                    %sx = s2_iter;
                end

                body = P_body;
                body(3) = 0;

                %
                e1 = s1^0.9;
                a1 = s1 / 10;

                e2 = s2^0.9;
                a2 = s2 / 10;

                %% remesh geometry
                r_s = evenSphere(body(1, 1), body(1, 2), body(1, 3), rs, s1);
                r_s = unique(cell2mat(r_s), 'rows');

                % plots walls of spiral channel
                [r_s4] = CurvedPlate(0,0,0,1950, 2050, ang, -25, s2); %0 changed to -25 % 295points
                [r_s2] = CylinderSection(0 , 0, -25, 2050, 50, s2, ang);% % 136points, Cylindersectfunction(2050,25,s);%50 changed to 25
                [r_s1] = CurvedPlate(0,0,0,1950, 2050, ang, 25, s2);%% 295points , 50 changed to 25
                [r_s3] = CylinderSection(0 , 0, -25, 1950, 50, s2, ang);% 128 points, 50 changed to 25

                % combine structure
                r_w = unique(cell2mat([r_s1; r_s2; r_s3; r_s4]), 'rows');
                r_s4 = []; r_s2 = []; r_s1 = []; r_s3 = [];

                %% L_dot
                Ldotw = zeros(length(r_w) * 3, 1);

                r_s_x_prime = (r_s(:,1).^2 + r_s(:,3).^2 ).^(1/2);
                u_s = -U(r_s_x_prime , r_s(:, 2));
                v_s = -V(r_s_x_prime , r_s(:, 2));
                w_s = -W(r_s_x_prime , r_s(:, 2));
                r_s_x_prime = [];

                l_dot = [cell2mat(mat2cell([u_s v_s w_s], ones(length(v_s),1), 3)')'; Ldotw];

                
                [V_body] = LowReynoldsSimulation.iterate_multi({r_s, r_w}, {body, body}, l_dot, {a1, a2}, {e1, e2}, mu, a_LJ, e_LJ);
                
                % display iteration time

                % conv 
                % 1 - x
                % 2 - y
                % 3 - z
                % 4 - theta
                % 5 - phi
                % 6 - gamma
                
                conv(r, gamV, 1) = V_body(1);
                conv(r, gamV, 2) = V_body(2);
                conv(r, gamV, 3) = V_body(3);
                conv(r, gamV, 4) = V_body(4);
                conv(r, gamV, 5) = V_body(5);
                conv(r, gamV, 6) = V_body(6);
                
                dSVals(gamV, r) = s1;


                iterationTime = cputime - start;

                fprintf('Iteration: %i \tGamma: %i \tCalculation Time: %i\n', r, gam(gamV), iterationTime);

                % saves iteration length data with number of stokeslet 
                user = memory;
                mem = user.MemUsedMATLAB;
                numS(r) = length(r_s) + length(r_w);
                iteraFileName = strcat([fDir, 'IterationData.csv']);
                
                if ~(exist(iteraFileName, 'file') == 2)
                    fid = fopen(iteraFileName, 'a+');
                    fprintf(fid, 'Num_Stokeslets, IterationTime, dS1, dS2,\n');
                else
                    fid = fopen(iteraFileName, 'a+');
                end


                fprintf(fid, strcat([int2str(numS(r)), ',' int2str(iterationTime), ',', num2str(s1), ',', num2str(s2), ',\n']));
                fclose(fid);
                 
            end
            % perform the regression for the gamma value
            %% Regression calculation
            regplot = figure('rend','painters','pos',[10 10 1100 800]);

            
            coor = ['X', 'Y', 'Z', 'Theta', 'Phi', 'Gamma'];
            
            for j = 1:6
                sx = var.^gam(gamV);
                cx = conv(:,gamV,j)';
                
                varMat = [sum(cx)*sum(sx.^2)-sum(sx)*sum(sx.*cx); 
                    length(sx)*sum(sx.*cx) - sum(sx)*sum(cx)];
                
                regVar = 1/(sum(sx.^2)-sum(sx)^2) * varMat;
                regVarPoly = polyfit(sx, cx, 1);
                E(j, gamV) = regVarPoly(2);
                C(j, gamV) = regVarPoly(1);
                rx = (E(j,gamV) + C(j,gamV).*(sx));
                Rsq(j, gamV) = sum((cx - rx).^2);
                
                xp = linspace(0, 1, 100);
                
                % regression and error plot for value of gamma
                
                yp = @(x) E(j,gamV) + C(j,gamV) * x;
                subplot(6, 3, j*3-2)
                % actual points
                plot(sx, cx, 'or')
                % regression line
                hold on
                plot(xp, yp(xp), 'k:')
                hold off
                xlabel('delta S')
                ylabel('Calculated Velocity')
                title('Regression Line')
                subplot(6, 3, j*3-1)
                err = abs(abs(E(j, gamV)- cx)./E(j, gamV));
                plot(sx, err)
                xlabel('delta S')
                ylabel('|d_e - d_i|/ d_e')
                title(strcat([coor(j) ' plot Error']))
                subplot(6, 3, j*3)
                plot(numS, err)
                xlabel('number of stokeslets')
                ylabel('|d_e - d_i|/ d_e')
                title('Stokelets vs error')
                
                
                
                
            end
            
            gType = '_RegressionPlots_';
            gamstr = strrep(num2str(gam(gamV)), '.', '_');
            
            % save .m of file
            saveas(regplot, strcat([fDir, test, '_', int2str(cVar), date, '_v', int2str(conv_var), '_g', gamstr, gType]))
            % save .jpg of file
            saveas(regplot, strcat([fDir, test, '_', int2str(cVar), date, '_v', int2str(conv_var), '_g', gamstr, gType, fFormat]))
            
            close all
        end
        
        %% plot E, C, and Rsq values
        
        errFig = figure;
        %subplot(3, 1, 1)
        %plot(Rsq(cVar,:), E(cVar, :))
        %xlabel('R^2')
        %ylabel('E')
        %title(strcat(['E, variable: ', int2str(conv_var)], ''));

        %subplot(3, 1, 2)
        %plot(Rsq(cVar,:), C(cVar,:))
        %xlabel('R^2')
        %ylabel('C')
        %title(strcat(['C, variable: ', int2str(conv_var)], ''));
              
        %subplot(3, 1, 3)
        plot(gam, Rsq(cVar,:))
        xlabel('gamma')
        ylabel('R^2')
        title(strcat(['R^2 - ', ' variable: ', int2str(conv_var)], ''));

        gType = 'error';

        
        % save the matlab plot
        saveas(errFig, strcat([fDir, test, '_', int2str(cVar), date, '_v', int2str(conv_var), '_g', gType]))
        % save the jpg plot
        saveas(errFig, strcat([fDir, test, '_', int2str(cVar), date, '_v', int2str(conv_var), '_g', gType, fFormat]))


    
        saveLoc = strcat([fDir, test, '_', int2str(cVar), date, '_convVar']);
        save(saveLoc, 'conv')

        close all
    catch ME
        
        % dumps the data of the current regression plot and saves the data
        % to the FDir
        
        coor = ['X', 'Y', 'Z', 'Theta', 'Phi', 'Gamma'];
        
        % plot the different coordinate residuals
        for c = 1:length(Rsq)
            subplot(1,6,c)
            plot(Rsq)
            title(strcat(['Residual: ',  coor(c)]))
            xlabel('Residual Value');
            ylabel('iteration number');
        end
        
        gType = 'error';
        
        % save the matlab plot
        saveas(errFig, strcat([fDir, test, '_', int2str(cVar), date, '_v', int2str(conv_var), '_g', gType]))
        % save as jpg plot
        saveas(errFig, strcat([fDir, test, '_', int2str(cVar), date, '_v', int2str(conv_var), '_g', gType, fFormat]))
        
        saveLoc = strcat([fDir, test, '_', int2str(cVar), date, '_convVar_failed']);
        save(saveLoc, 'conv')
    end
end
figure
plot3(r_s(:,1), r_s(:,2), r_s(:,3), '.')

f = 6 * pi * rs * mu;