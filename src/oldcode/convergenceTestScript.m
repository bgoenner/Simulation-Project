
clc
clear
close all

cVar = 1;

% remember to change both dir and file name
fileSavePath = 'C:\Users\bg\Box Sync\Simulation\bg\';
fDir = '.\ConvergencePlots\09132018\';
test = 't10_';
date = '09132018_';
fFormat = '.jpg';

% sphere intial parameters
% position
xs = 2.0300e3 + 20;
ys = -0.0008e3;
zs = 0e3;

% radius
rs = 3;
% delta 


% create interpolant objects

%% Background flow

%initialize background flow
dat_dat = csvread('Vfield.csv',1);
r = dat_dat(:,1)';%which is x axis
z = dat_dat(:,2)';%which is y axis
u = dat_dat(:,3)'*10e6;%which is Ux axis
v = dat_dat(:,5)'*10e6;%which is Uz axis
w = dat_dat(:,4)'*10e6;%which is Uy axis

U = scatteredInterpolant(r', z', u');
V = scatteredInterpolant(r', z', v');
W = scatteredInterpolant(r', z', w');

dS1 = [0.35, 0.33, 0.3];

ang = 0.2;
ang0 = ang;
dAng = [0, 0.05, 0.1];

dS2 = [2, 1.75, 1.5];
s2 = dS2(1);

% delta time
%dt = t(2) - t(1);

% intial sphere position and velocity
P_body(1,:) = [xs, ys, zs, 0, 0, 0]';  % initial position
V_body(1,:) = [0, 0, 0, 0, 0, 0]';     % initial velocity is zero

% channel center
CC = [2000, 0 , 0];

% fluid viscosity
mu = 1.002*10^-4;
a = 0;

gam = 0.7:0.3:2.1;

% defines the function to perform the convergnce study for the geometry
sphereChannelGeomConv = @(dS) sphereIteration(P_bodym, dS,      ang, dS2(1), mu, a_LJ, e_LJ, [U,V,W]);
% defines the function to perform the convergnce study for the channel
sphereChannelChanConv = @(dS) sphereIteration(P_bodym, dS1(1),  ang, dS,     mu, a_LJ, e_LJ, [U,V,W]);
% defines the function to perform the convergnce study for the channel angle
sphereChanneldAngConv = @(dS) sphereIteration(P_bodym, dS1,     dS,  dS2(1), mu, a_LJ, e_LJ, [U,V,W]);

% convergenceFunction(geoFunct,               dS, gam, cVar,              fDir, name,                       verbose)
  convergenceFunction(sphereChannelGeomConv, dS1, gam, 1, '.\ConvergencePlots', 'SphereChannelConvergence', 1)


function [V_Body] = sphereIteration(P_body, s1, ang, s2, mu, sig_LJ, e_LJ, field)

    body = P_body;
    body(3) = 0;

    %
    e1 = s1^0.9;
    a1 = s1 / 10;

    e2 = s2^0.9;
    a2 = s2 / 10;

    %% remesh geometry
    r_s = evenSphere(body(1, 1), body(1, 2), body(1, 3), rs, s1);
    r_s = unique(cell2mat(r_s), 'rows');

    % plots walls of spiral channel
    [r_s4] = CurvedPlateFunction(1950, 2050, ang, -25, s2); %0 changed to -25 % 295points
    [r_s2] = CylinderSectionFunction(0 , 0, -25, 2050, 50, s2, ang);% % 136points, Cylindersectfunction(2050,25,s);%50 changed to 25
    [r_s1] = CurvedPlateFunction(1950, 2050, ang, 25, s2);%% 295points , 50 changed to 25
    [r_s3] = CylinderSectionFunction(0 , 0, -25, 1950, 50, s2, ang);% 128 points, 50 changed to 25

    % combine structure
    r_w = unique(cell2mat([r_s1; r_s2; r_s3; r_s4]), 'rows');
    r_s4 = []; r_s2 = []; r_s1 = []; r_s3 = [];

    %% L_dot
    Ldotw = zeros(length(r_w) * 3, 1);

    U = field(1);
    V = field(2);
    W = field(3);
    
    r_s_x_prime = (r_s(:,1).^2 + r_s(:,3).^2 ).^(1/2);
    u_s = -U(r_s_x_prime , r_s(:, 2));
    v_s = -V(r_s_x_prime , r_s(:, 2));
    w_s = -W(r_s_x_prime , r_s(:, 2));
    r_s_x_prime = [];

    l_dot = [cell2mat(mat2cell([u_s v_s w_s], ones(length(v_s),1), 3)')'; Ldotw];


    [V_body] = LowReynoldsSimulation.iterate_multi({r_s, r_w}, {body, body}, l_dot, {a1, a2}, {e1, e2}, mu, sig_LJ, e_LJ);
end