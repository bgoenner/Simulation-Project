 % SpheretestClass.m
clc
clear
close all

fileSavePath = 'C:\Users\bg\Box Sync\Simulation\bg\';

% Set different Ns
N = [3, 3.5, 4]*4;

% sphere intial parameters
% position
xs = 2.0300e3;
ys = -0.0008e3;
zs = 0e3;

% radius
rs = 3;
% delta 
ss = 1.5;
s2 = 1.0;

dS = pi * rs ./ (N);

%% Background flow

%initialize background flow
dat_dat = csvread('Vfield.csv',1);
r = dat_dat(:,1)';%which is x axis
z = dat_dat(:,2)';%which is y axis
u = dat_dat(:,3)'*10e6;%which is Ux axis
v = dat_dat(:,5)'*10e6;%which is Uz axis
w = dat_dat(:,4)'*10e6;%which is Uy axis

% create interpolant objects

U = scatteredInterpolant(r', z', u');
V = scatteredInterpolant(r', z', v');
W = scatteredInterpolant(r', z', w');

P_body(1,:) = [xs, ys, zs, 0, 0, 0]';  % initial position
V_body(1,:) = [0, 0, 0, 0, 0, 0]';     % initial velocity is zero


%% Convergence Study
for conv_var = 1:3
    i = 0;
    
    for n = 1:length(N)

        R = 1.2;
        R2 = 1.2;
        dAng = 0.1;

        for r = 1:3
            i = i+1;
            if conv_var == 1
                ss(i) = dS(n) *R^(1-r);
                e = ss(r)^0.9;
            elseif conv_var == 2
                ang(i) = ang0 * dAng*(r);
            elseif conv_var == 3
                s2(i) = dS2(n) * R2(1-r);
            end
            

            r_s = evenSphere(P_body(1, 1), P_body(1, 2), 0, rs, ss(r));
            r_s = unique(cell2mat(r_s), 'rows');

            % what needs to be changed in a new simulation
            % inital geometry definition
            % plots walls of spiral channel
            [r_s4] = CurvedPlateFunction(1950, 2050, ang, -25, s2); %0 changed to -25 % 295points
            [r_s2] = CylinderSectionFunction(0 , 0, -25, 2050, 50, s2, ang);% % 136points, Cylindersectfunction(2050,25,s);%50 changed to 25
            [r_s1] = CurvedPlateFunction(1950, 2050, ang, 25, s2);%% 295points , 50 changed to 25
            [r_s3] = CylinderSectionFunction(0 , 0, -25, 1950, 50, s2, ang);% 128 points, 50 changed to 25
             % combine structure
            r_w = unique(cell2mat([r_s1; r_s2; r_s3; r_s4]), 'rows');


            % simulation time parameters
            itera  = 500;
            deltaT = 1e-6;

            t = linspace(deltaT, (itera)*deltaT, itera);



            % simulation parameters


            % channel center

            % channel radius

            % channel angle in radians
            ang = 0.5;

            % delta time
            dt = t(2) - t(1);

            % intial sphere position and velocity
            P_body(1,:) = [xs, ys, zs, 0, 0, 0]';  % initial position
            V_body(1,:) = [0, 0, 0, 0, 0, 0]';     % initial velocity is zero

            % channel center
            CC = [2000, 0 , 0];

            % stokeslet sphere radius
            a = 0.1;
            e = 1.5;

            % fluid viscosity
            mu = 1.002*10^-4;


<<<<<<< HEAD
            %% intialize geometry
=======
        r_s = evenSphere(P_body(1, 1), P_body(1, 2), P_body(1, 3), rs, ss(r));
        r_s = unique(cell2mat(r_s), 'rows');
        
        start = cputime;
>>>>>>> fba66e5696160033f0266eadb6c2bf866298f58e

            % move to inside of the loop to reduce duplication of code
            %[r_s] = unique(cell2mat(evenSphere(xs, ys, zs, rs, ss)), 'rows');% initilize sphere

            % combine structure

            %% geometry intial velocities
            %v_s  = ones(length(r_s),1) * 3;

            % Test with different LJ parameters
            % set these to zero if they are not needed
            %e_LJ = 1*10^st;
            %a_LJ = rs*10^-ro;

            %% do not modify
            % create interpolant objects

            start = cputime;

            % interpolate velocities
            % adjusts for x due to z position
            r_s_x_prime = (r_s(:,1).^2 + r_s(:,3).^2 ).^(1/2);
            u_s = -U(r_s_x_prime , r_s(:, 2));
            v_s = -V(r_s_x_prime , r_s(:, 2));
            w_s = -W(r_s_x_prime , r_s(:, 2));

            l_dot = [cell2mat(mat2cell([u_s v_s w_s], ones(length(v_s),1), 3)')'; Ldotw];

            [V_body] = LowReynoldsSimulation.iterate_multi({r_s, r_w}, {body, body}, l_dot, a, e, mu, a_LJ, e_LJ);
            % display iteration time

            x_conv(i) = V_body(1,1);
            y_conv(i) = V_body(2,2);
            z_conv(i) = V_body(3,3);
            the_conv(i) = V_body(4,4);
            phi_conv(i) = V_body(5,5);
            gam_conv(i) = V_body(6,6);

            iterationTime = cputime - start;

            fprintf('Iteration: %i \t\tCalculation Time: %i\n', n, iterationTime);

        end
            %c1 = x_conv(3);
            %c3 = x_conv(1);

            %x_conv(1) = c1;
            %x_conv(3) = c3;


            %gamma(n) = log((x_conv(2) - x_conv(3))/(x_conv(1) - x_conv(2)))/log(R);
            %C(n) = (x_conv(1) - x_conv(2))/ (dS(n)^gamma(n) * (1 - R^gamma(n)));
            %de(n) = (x_conv(1) - C(n) * dS(n)^gamma(n));

            figure
            plot([fliplr(ss)], [fliplr(x_conv)])
            xlabel('dS')
            ylabel('de, d1, d2, d3')
            err = de(n) - x_conv;
            figure
            loglog([dS(n), dS(n)/r, dS(n)/r^2], err)

            err2 = de(n) ./ x_conv;
            figure
            plot([dS(n), dS(n)/r, dS(n)/r^2], err2)
            ylabel('di/de')

    end
<<<<<<< HEAD
=======
        %c1 = x_conv(3);
        %c3 = x_conv(1);
        
        %x_conv(1) = c1;
        %x_conv(3) = c3;
        
   
        gamma(n) = log((x_conv(2) - x_conv(3))/(x_conv(1) - x_conv(2)))/log(R);
        C(n) = (x_conv(1) - x_conv(2))/ (dS(n)^gamma(n) * (1 - R^gamma(n)));
        de(n) = (x_conv(1) - C(n) * dS(n)^gamma(n));
        
        figure
        plot([0, fliplr(ss)], [de(n), fliplr(x_conv)])
        xlabel('dS')
        ylabel('de, d1, d2, d3')
        err = de(n) - x_conv;
        figure
        loglog([dS(n), dS(n)/r, dS(n)/r^2], err)
        xlabel('dS')
        ylabel('de - di')
        
        err2 = de(n) ./ x_conv;
        figure
        plot([dS(n), dS(n)/r, dS(n)/r^2], err2)
        ylabel('di/de')
        
end
>>>>>>> fba66e5696160033f0266eadb6c2bf866298f58e

    figure
    plot(dS, gamma)
    xlabel('data')
    ylabel('gamma')
    figure
    plot(dS, de)
    xlabel('data')
    ylabel('de')


end
f = 6 * pi * rs * mu;