 % SpheretestClass.m
clc
clear
close all

% saveas(join([fDir, test, date, '_i', n, '_v', conv_var, gType, fFormat]))

% remember to change both dir and file name
fileSavePath = 'C:\Users\bg\Box Sync\Simulation\bg\';
fDir = '.\ConvergencePlots\09132018\';
test = 't11_';
date = '09132018_';
fFormat = '.jpg';

% sphere intial parameters
% position
xs = 2.0300e3 + 20;
ys = -0.0008e3;
zs = 0e3;

% radius
rs = 3;
% delta 

dS = [0.37, 0.37, 0.33];
% create interpolant objects

%% Background flow

%initialize background flow
dat_dat = csvread('Vfield.csv',1);
r = dat_dat(:,1)';%which is x axis
z = dat_dat(:,2)';%which is y axis
u = dat_dat(:,3)'*10e6;%which is Ux axis
v = dat_dat(:,5)'*10e6;%which is Uz axis
w = dat_dat(:,4)'*10e6;%which is Uy axis

U = scatteredInterpolant(r', z', u');
V = scatteredInterpolant(r', z', v');
W = scatteredInterpolant(r', z', w');

ang = 0.2;
ang0 = ang;
dAng = [0, 0.05, 0.1];

dS2 = [2, 1.75, 1.5];
s2 = dS2(1);


% channel center

% channel radius

% channel angle in radians


% delta time
%dt = t(2) - t(1);

% intial sphere position and velocity
P_body(1,:) = [xs, ys, zs, 0, 0, 0]';  % initial position
V_body(1,:) = [0, 0, 0, 0, 0, 0]';     % initial velocity is zero

% channel center
CC = [2000, 0 , 0];

% fluid viscosity
mu = 1.002*10^-4;
a = 0;


for conv_var = 1:3
    
    if conv_var == 1
        var = dS;
    elseif conv_var == 2
        var = dAng;
    elseif conv_var == 3
        var = dS2;
    end 
    
for n = 1:length(var)


% what needs to be changed in a new simulation
% inital geometry definition

% simulation parameters




%% intialize geometry

% move to inside of the loop to reduce duplication of code
%[r_s] = unique(cell2mat(evenSphere(xs, ys, zs, rs, ss)), 'rows');% initilize sphere

% combine structure

%% geometry intial velocities
%v_s  = ones(length(r_s),1) * 3;

% Test with different LJ parameters
% set these to zero if they are not needed
e_LJ = 0;
a_LJ = 0;

%% do not modify
% create interpolant objects


    R = 1.2;

    for r = 1:3
        
        start = cputime;
        
        %% convergence variable deltas
        if conv_var == 1
            s1_iter(r) = dS(n) *R^(1-r);
            e1 = s1_iter(r)^0.9;
            sx = s1_iter;
            s1 = s1_iter(r);
        elseif conv_var == 2
            ang_iter(r) = ang0 + dAng(n) * R^(1-r);
            sx = ang_iter;
            ang = ang_iter(r);
        elseif conv_var == 3
            s2_iter(r) = dS2(n) *R^(1-r);
            e2 = s2_iter(r)^0.9;
            sx = s2_iter;
            s2 = s2_iter(r);
        end
        
        body = P_body;
        body(3) = 0;
        
        %
        e1 = s1^0.9;
        a1 = s1 / 10;
        
        e2 = s2^0.9;
        a2 = s2 / 10;
            
        %% remesh geometry
        r_s = evenSphere(body(1, 1), body(1, 2), body(1, 3), rs, s1);
        r_s = unique(cell2mat(r_s), 'rows');
        
        % plots walls of spiral channel
        [r_s4] = CurvedPlateFunction(1950, 2050, ang, -25, s2); %0 changed to -25 % 295points
        [r_s2] = CylinderSectionFunction(0 , 0, -25, 2050, 50, s2, ang);% % 136points, Cylindersectfunction(2050,25,s);%50 changed to 25
        [r_s1] = CurvedPlateFunction(1950, 2050, ang, 25, s2);%% 295points , 50 changed to 25
        [r_s3] = CylinderSectionFunction(0 , 0, -25, 1950, 50, s2, ang);% 128 points, 50 changed to 25
        
        % combine structure
        r_w = unique(cell2mat([r_s1; r_s2; r_s3; r_s4]), 'rows');
        r_s4 = []; r_s2 = []; r_s1 = []; r_s3 = [];
        
        %% L_dot
        Ldotw = zeros(length(r_w) * 3, 1);
                
        r_s_x_prime = (r_s(:,1).^2 + r_s(:,3).^2 ).^(1/2);
        u_s = -U(r_s_x_prime , r_s(:, 2));
        v_s = -V(r_s_x_prime , r_s(:, 2));
        w_s = -W(r_s_x_prime , r_s(:, 2));
        r_s_x_prime = [];
        
        l_dot = [cell2mat(mat2cell([u_s v_s w_s], ones(length(v_s),1), 3)')'; Ldotw];


        [V_body] = LowReynoldsSimulation.iterate_multi({r_s, r_w}, {body, body}, l_dot, {a1, a2}, {e1, e2}, mu, a_LJ, e_LJ);
    
        % display iteration time
        
        x_conv(r)   = V_body(1);
        y_conv(r)   = V_body(2);
        z_conv(r)   = V_body(3);
        the_conv(r) = V_body(4);
        phi_conv(r) = V_body(5);
        gam_conv(r) = V_body(6);
        
        
        iterationTime = cputime - start;

        fprintf('Iteration: %i \t\tCalculation Time: %i\n', n, iterationTime);

        
        
    end
        %c1 = x_conv(3);
        %c3 = x_conv(1);
        
        %x_conv(1) = c1;
        %x_conv(3) = c3;
        
   
        gamma(n) = log((x_conv(2) - x_conv(3))/(x_conv(1) - x_conv(2)))/log(R);
        C(n) = (x_conv(1) - x_conv(2))/ (sx(n)^gamma(n) * (1 - R^gamma(n)));
        de(n) = (x_conv(1) - C(n) * sx(n)^gamma(n));
        
        deConv = figure;
        plot([0, fliplr(sx)], [de(n), fliplr(x_conv)])
        xlabel('dS')
        ylabel('de, d1, d2, d3')
        title(join(['Iteration ', int2str(n), ' variable: ', int2str(conv_var)], ''));
        gType = 'deConv';
        
        saveas(deConv, join([fDir, test, date, '_i', int2str(n), '_v', int2str(conv_var), gType, fFormat]))
        
        err = de(n) - x_conv;
        errFig = figure;
        loglog([dS(n), dS(n)/r, dS(n)/r^2], err)
        xlabel('dS')
        ylabel('de - di')
        title(join(['Iteration ', int2str(n), ' variable: ', int2str(conv_var)], ''));
        gType = 'error';
        
        saveas(errFig, join([fDir, test, date, '_i', int2str(n), '_v', int2str(conv_var), gType, fFormat]))

        
        err2 = abs((x_conv - de(n)) ./ de(n));
        err2Fig = figure;
        plot([dS(n), dS(n)/r, dS(n)/r^2], err2)
        ylabel('di/de')
        title(join(['Error - ', 'Iteration ', int2str(n), ' variable: ', int2str(conv_var)], ''));
        gType = 'error2';
        
        saveas(err2Fig, join([fDir, test, date, '_i', int2str(n), '_v', int2str(conv_var), gType, fFormat]))

        
end

gammaFig = figure;
plot(sx, gamma)
xlabel('data')
ylabel('gamma')
gType = 'gamma';
title(join('gamma; var ', int2str(conv_var)));
saveas(gammaFig, join([fDir, test, date, '_i', int2str(n), '_v', int2str(conv_var), gType, fFormat]))


deFig = figure;
plot(sx, de)
xlabel('data')
ylabel('de')
title(join('de; var ', int2str(conv_var)));
gType = 'de';
saveas(deFig, join([fDir, test, date, '_i', int2str(n), '_v', int2str(conv_var), gType, fFormat]))

close all
end
figure
plot3(r_s(:,1), r_s(:,2), r_s(:,3), '.')

f = 6 * pi * rs * mu;