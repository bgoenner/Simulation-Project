# Regularized Stokeslet Functions

from numpy import eye
import numpy as np

from math import sqrt

def iteration(R_S, R_0, P_S, P_n, V_S, ep, mu, sig_LJ, F_LJ, Rot):

    # Get G_inv
    G_inv = G_Matrix(R_S, R_0, ep, mu)


    # Get IRX

    # Pressure

    # Steric Force Vector

    # Resistance Matrix
    # M = X' * G^-1 * X

    # V   = R^-1 * [force/torques mapping] * [Forces]
    # Viscous contributing velocity
    # V_V = -M^-1 * X' * G^-1 * L_dot


    # Pressure contributing velocity
    # V_P = -M^-1 * X' * (P*n)

    # Surface Forces
    # V_S = -M^-1 * X' * LJ


    # Sum velocities
    # V = V_V + V_P + V_S


    # convert to lab frame (This may be not as strait forward with interacting bodies)

    pass

# Regularized Velocity
def G_Matrix(Body, P_0, ep, mu):

    G_len = 0

    for i in Body:
        for l in i:
            G_len += l

    for i in Body:
        for j in Body:
            for alpha in i:
                for beta in j:
                    r_diff = alpha - beta
                    r_norm = sqrt(np.sum(np.power(r_diff, 2)))

                    S_ij = eye(3) * (pow(r_norm,2) + 2*pow(ep, 2))/pow((pow(r_norm,2) + pow(ep,2)), 3/2) \
                        np.cross(n_diff, n_diff) / pow(pow(r_norm, 2) + pow(ep, 2)), 3/2)

                    
    pass

# Regularized Pressure
def P_Vector(Body, R_n):
    for i in Body:
        for j in Body:
            for alpha in i:
                for beta in j:
                    r_diff = alpha - beta
                    r_norm = sqrt(np.sum(np.power(r_diff, 2)))

                    
    pass

# Force represented as a Lennard Jones Force on the surfaces differing bodies
# (Though flexible bodies should be calculated to prevent parts from phasing through)
def solidSurfaceForce():
    pass

def rotation():
    pass