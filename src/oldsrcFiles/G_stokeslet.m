function [G, rNorm, r_] = G_stokeslet(r_s, r_w, e_, a_, mu)

    %rMat = [cell2mat(r_s); cell2mat(r_w)];
      
    %[mat1, mat2] = meshgrid(rMat, rMat);%The grid represented by the coords X and Y has length(mat2) rows and length(mat1) columns.
    
    r_comb = [r_s; r_w];
    
    mat1 = repmat(r_comb, 1, length(r_comb));

    matdiff = cell2mat(mat1) - cell2mat(mat1');
    
    mat1diff = mat2cell(matdiff, ones(1, length(mat1)), ones(1, length(mat1))*3);
    
    mat2diff = mat2cell(matdiff', ones(1, length(mat1))*3, ones(1, length(mat1)));
    
    
    r_ = cellfun(@dotMatrix, mat2diff, mat1diff, 'UniformOutput', false);
    
    delta = repmat(eye(3), length(mat1), length(mat1));%ones on diagonal term,first row, then column
    
    %r_ = (mat1 - mat2) .* delta;
    
    rNorm = cellfun(@norm, mat1diff);%why do we do it????
    %r = ones(length(r_), length(r_));
    % rSize = size(rNorm);
    
    %for a = 1:rSize(1)/3
    %    for b = 1:rSize(2)/3            
    %        r(3*a-2:a*3, 3*b-2:b*3) = ones(3,3) * rCells(a,b);
    %    end
    %end
    
    r = repelem(rNorm, 3, 3);%replacement for for loops(above)    
%      size(delta)
%      size(rNorm)
%      size(r_)
%      size(r)
    
    a = a_;
    e = e_;
    %[r_1, r_2] = meshgrid(r_, r_);
    
    %r_dot = r_ .* 
    
    G = a/(8 * 3.141 * mu) * (delta .*(r.^2 + 2*e^2)./(r.^2 + e^2) + (cell2mat(r_))./(r.^2 + e^2).^(3/2));

end