function [ R_dt ] = Rotation_dt( Omega, theta, dt )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
    sinphi = @(O) norm(O) * dt + theta * (norm(O) * dt)^3;
    cosphi = @(O) 1 - 0.5 * (norm(O) * dt )^2;
    crossO = @(O, T) [0 -O(3)*T(1) O(2)*T(1);
                   O(3)*T(2) 0 -O(1)*T(2);
                   -O(2)*T(3) O(1)*T(3) 0]; % calc skew semetric matrix
               
     %sinphi(Omega)
     %cosphi(Omega)
               
    
    %% Change from lab omega to body Omega
    R_dt = Omega*Omega' + cosphi(Omega)*(eye(length(Omega)) - Omega*Omega') + crossO(Omega, sinphi(Omega));  
end

