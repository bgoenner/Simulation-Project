clear 
close all
    
    % ===================
    %     Initialize
    % ===================
    
    % Set configuration parameters
    nMols = 125;        % Number of diatomic molecules
    nPart = 2*nMols;    % Number of particles
    density = 0.365;    % Density of molecules
    mass = 1;           % Particles' mass
    
    % Set parameters for splitting the potential
    lambda = 0.3;       % Size of the switching interval
    Rm = 1.7;           % Outer switching radius
    Rc = 3.0;           % Cutoff radius for LJ force calculation
    Rc2 = Rc^2;         % Cutoff radius, squared
    
    % Set parameter for bonding potential
    lBond = 1;          % Inter-atomic distance
    kBond = 50000;      % Bond strength
    
    % Set simulation parameters
    dt =  0.001;        % Integration time for the long-range part
    nRepeat = 10;       % Number of short-range cycles before integrating the long-range part
    dtShort = dt / nRepeat;  % Integration time for the short-range part
    Temp = 3.0;         % Simulation temperature
    
    
    nSteps = 10000;     % Total simulation time (in integration steps)
    sampleFreq = 10;    % Sampling frequency
    sampleCounter = 0;  % Sampling counter
    printFreq = 100;    % Printing frequency
    
    % Set initial configuration
    % coords holds all atomic coordinates while coordsCOM holds molecules'
    % center of mass coordinates.
    [coords, coordsCOM, L] = initDiatomicConfig(nMols,density,lBond);
    
    % Set initial velocities with random numbers
    vels = zeros(3,nPart);
    for part=1:nPart
        vels(:,part) = randGauss(0,sqrt(Temp/mass),3);
    end
    
    % Set initial momentum to zero
    totV = sum(vels,2)/nPart; % Center of mass velocity
    for dim=1:3
        vels(dim,:) = vels(dim,:) - totV(dim);    % Fix any center-of-mass drift
    end
    
    % Set initial kinetic energy to 3*KbT/2
    totV2 = sum(dot(vels,vels))/nPart;  % Mean-squared velocity
    velScale = sqrt(3*Temp/totV2);      % Velocity scaling factor
    for dim=1:3
        vels(dim,:) = vels(dim,:)*velScale;    
    end
    
     
    % ===================
    % Molecular Dynamics
    % ===================
    
    % Calculate initial forces
    forcesLong = LJ_ForceLong(coordsCOM,L,Rc2,Rm,lambda);
    forcesShort = LJ_ForceShort(coordsCOM,L,Rm,lambda);
    forcesBond = bondForce(coords,L,kBond,lBond);
    
    
    time = 0; % Following simulation time
    
    for step = 1:nSteps
        
        % ==== Velocity Verlet with time step dt ==== %
         
        % Update velocities
        vels = vels + 0.5*dt*forcesLong;
        
        % ==== nRepeat steps of Velocity Verlet on the short-range interaction ==== %
        
        for subStep = 1:nRepeat
            
            % Update velocities - half time step
            vels = vels + 0.5*dtShort*(forcesShort + forcesBond);
            
            % Update positions 
            coords = coords + dtShort*vels;
    
            % Apply periodic boundary conditions
            for part=1:nPart
                coords(:,part) = PBC3D(coords(:,part),L);
            end
    
            
            % Calculate short range forces
            forcesShort = LJ_ForceShort(coordsCOM,L,Rm,lambda);
            forcesBond = bondForce(coords,L,kBond,lBond);
    
            % Update velocities - half time step
            vels = vels + 0.5*dtShort*(forcesShort + forcesBond);
            
        end
        
        % Calculate forces
        forcesLong = LJ_ForceLong(coordsCOM,L,Rc2,Rm,lambda);
        forcesShort = LJ_ForceShort(coordsCOM,L,Rm,lambda);    
        forcesBond = bondForce(coords,L,kBond,lBond);
    
        
        % Update velocities
        vels = vels + 0.5*dt*forcesLong;
        
           
        % === Move time forward ===
        time = time + dt;
        
        % === Sample ===
        if mod(step,sampleFreq) == 0
            sampleCounter = sampleCounter + 1;
            
            % Enter sampling code here
           
        end
        
        if mod(step,printFreq) == 0
            step % Print the step 
        end
        
    end
    
    
    % ===================
    % Simulation results
    % ===================
    
    % Plot your results here
    
    
   