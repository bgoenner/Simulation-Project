clc
clear
close all

xs = 0;%2000;%150, moves the particle radially (channel radius varies from 100-200 microns)
ys = 0;%4.95, It moves the particle into the channel
zs = -52.75;%-25, moves the particle up and down
xe = 0.2;%2000;%150, moves the particle radially (channel radius varies from 100-200 microns)
ye = 0;%4.95, It moves the particle into the channel
ze = 0;
Ang = 360;
s2 = 0.5;
s_e = 0.5;
s_t = 0.3;
s_e1 = 0.1;
R = 0.5;
ang = 1.5;%was 0.5 but sperm tail is bigger than te channel, so changed te angle to 1.5
[r_s6] = EllipseFunction(xe, ye, ze, 1.5, 1.5, 2.5, s_e);%delta=0.001, to get horizontal ellipsoid 2.5, 1.5 
[r_s7] = Cylindersectfunction(xs, ys, zs, 0.5, 50, s_t, Ang);%R, height, delta should be very small, angle
[r_s8] = CircularPlatefunction(R, Ang, -53, s_e1);%z = -53
 
[r_s4] = CurvedPlateTfunction(1950, 2050, ang, -25, s2); %0 changed to -25 % 295points
[r_s2] = Cylindersectfunction(0 , 0, -25, 2050, 50, s2, ang);% % 136points, Cylindersectfunction(2050,25,s);%50 changed to 25
[r_s1] = CurvedPlateTfunction(1950, 2050, ang, 25, s2);%% 295points , 50 changed to 25
[r_s3] = Cylindersectfunction(0 , 0, -25, 1950, 50, s2, ang);% 128 points, 50 changed to 25

% r_Sperm = [r_s6';r_s7';r_s8'];
% %size(r_s6) = 162*1; size(r_s7) = 1660*1; size(r_s8) = 76*1;
% beta = pi/2;%90 degrees
% Rot = [cos(beta) 0 sin(beta); 0 1 0 ; -sin(beta) 0  cos(beta)];
% %size(Rot)=3*3
% 
% r_SpNew = Rot*r_Sperm;


for k = 1:length(r_s4)
    x4(k) = r_s4{k}(1);
    y4(k) = r_s4{k}(2);
    z4(k) = r_s4{k}(3);
end

for k = 1:length(r_s2)
    x2(k) = r_s2{k,1}(1);
    y2(k) = r_s2{k,1}(2);
    z2(k) = r_s2{k,1}(3);
end

for k = 1:length(r_s1)
    x1(k) = r_s1{k}(1);
    y1(k) = r_s1{k}(2);
    z1(k) = r_s1{k}(3);
end

for k = 1:length(r_s3)
    x3(k) = r_s3{k,1}(1);
    y3(k) = r_s3{k,1}(2);
    z3(k) = r_s3{k,1}(3);
end

for k = 1:length(r_s6)
    x6(k) = r_s6{k}(1)+2000;
    y6(k) = r_s6{k}(2);
    z6(k) = r_s6{k}(3);
end

for k = 1:length(r_s7)
    x7(k) = r_s7{k}(1)+2000;
    y7(k) = r_s7{k}(2);
    z7(k) = r_s7{k}(3);
end

for k = 1:length(r_s8)
    x8(k) = r_s8{k}(1)+2000;
    y8(k) = r_s8{k}(2);
    z8(k) = r_s8{k}(3);
end

 x = [x6, x7, x8, x2, x1, x3, x4];y = [y6, y7, y8, y2, y1, y3, y4];z = [z6, z7, z8, z2, z1, z3,z4];
  
figure
hold on
axis equal
xlabel('X axis')
ylabel('Y axis')
zlabel('Z axis')
plot3(x, y, z, 'b*')  