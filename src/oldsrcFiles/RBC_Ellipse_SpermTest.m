clc
clear
close all
% In the new code it does not mattter what values yo assign to ys and zs,
% it does not put RBC in the wrong placee or cut it....
 xs = 2000;%150, moves the particle radially (channel radius varies from 100-200 microns)
 ys = 0;%4.95, It moves the particle into the channel
 zs = 0;%-52.75, moves the particle up and down
 xe = 0.2;%2000;%150, moves the particle radially (channel radius varies from 100-200 microns)
 ye = 0;%4.95, It moves the particle into the channel
 ze = 0;
 Ang = 360;
 s_e = 0.5;
 s_t = 0.3;
 s_e1 = 0.1;
 [r_s5] = RBCfunction(xs, ys, zs, 0.3);%116 points
 %[r_s6] = EllipseFunction(xe, ye, ze, 1.5, 1.5, 2.5, s_e);%delta=0.001, to get horizontal ellipsoid 2.5, 1.5 
 %[r_s7] = Cylindersectfunction(xs, ys, zs, 0.5, 50, s_t, Ang);%R, height, delta should be very small, angle
 %added this
 R = 0.5;
 
 %[r_s8] = CircularPlatefunction(R, Ang, -53, s_e1);%z = -53
for k = 1:length(r_s5)
    xl(k) = r_s5{k}(1);
    yl(k) = r_s5{k}(2);
    zl(k) = r_s5{k}(3);
end
% 
% for k = 1:length(r_s6)
%     xo(k) = r_s6{k}(1);
%     yo(k) = r_s6{k}(2);
%     zo(k) = r_s6{k}(3);
% end
% 
% for k = 1:length(r_s7)
%     x2(k) = r_s7{k}(1);
%     y2(k) = r_s7{k}(2);
%     z2(k) = r_s7{k}(3);
% end
% 
% for k = 1:length(r_s8)
%     x8(k) = r_s8{k}(1);
%     y8(k) = r_s8{k}(2);
%     z8(k) = r_s8{k}(3);
% end
%  x = [xo x2 x8]; y = [yo y2 y8]; z = [zo z2 z8];
 
figure
hold on
axis equal
% plot3(x, y, z, 'b*') 
% plot3(x8, y8, z8, 'r*') 
plot3(xl, yl, zl, 'r*') 

%plot3(xo, yo, zo, 'r*') 
% plot3(xo, yo, zo, 'b*')%blue figure of RBC 
