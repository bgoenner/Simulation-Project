function [r_plate] = stokesletplate(z, y, x, dy, dx, delta)

    x_pos = linspace(x, x + dx, delta);
    y_pos = linspace(y, y + dy, delta);
    
    r_plate = cell(length(x_pos) * length(y_pos));
    
    for a = 1:length(x)
        for b = 1:length(y)
            r_plate(b + a * (b-1)) = [x_pos(a), y_pos(b), z];
        end
    end

end