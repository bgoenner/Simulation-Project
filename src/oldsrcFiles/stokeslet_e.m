function [ velocity ] = stokeslet_e( position, epsilon, mu )
%Evaluation of the stokelet 'blob' equation
%   Detailed explanation goes here
    r = norm(position);
    r_ = position;
    e = epsilon;

    if length(position) == 2
        delta = [1, 0; 0, 1];
    end
    if length(position) == 3
        delta  = [1 0 0; 0 1 0; 0 0 1];
    end
    
    velocity = 1/(8 * 3.141 * mu) * (delta *(r^2 + 2*e^2)/(r^2 + e^2) + (r_' * r_)/(r^2 + e^2)^(3/2));
    
end

