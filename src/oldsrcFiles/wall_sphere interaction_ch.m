clc
clear
close all

t = linspace(0, 50*10e-7, 5);%had it 1.8*10e-5 and 50 instead of 5, 3*10e-5, 1.3*10e-5,1.5*10e-5

%all the numbers should have same format,how to make matlab output in
%exponential form?
format shortEng

%always when you open the file, save it and run it! Then try to print
%dat_dat = csvread('Vfield.csv', 10); %max(dat_dat) in command window!


%First load('matlab.mat')in command window, then order whatever you want
%there! this line reads the csv data into dat_dat starting from line 10 of
%the csv
dat_dat = csvread('Vfield.csv', 10);

max(dat_dat);
%checking all the dimensions
r = dat_dat(:,1)';%which is x axis
z = dat_dat(:,2)';%which is y axis

u = dat_dat(:,3)'*10e6;%which is Ux axis
v = dat_dat(:,5)'*10e6;%which is Uz axis
w = dat_dat(:,4)'*10e6;%which is Uy axis

% %Verification of what we got from Comsol, except here we have not added z component of velocity!! 2D plot

format long

quiver(r,z,u,v);% we have plotted Velocity Field!

%put sphere in the spiral channel we need to start from an initial
%position, we need to consider particle away from the wall!!

s = 1.5;%before 1.3 before.... 0.5
s2 = 1.5;%7
e_2 = s2/3;
%had it 7.5, delta for sphere and walls are different, cause walls
%dimensions(100w,50h) are way larger than sphere(10micron diameter) for
%angle=30 degrees=pi/6 and radius of r=2000, perimeter of given section of
%cylinder would be r*angle=1046 or 1000 microns.  
%we should get delta s1 for
%sphere and delta s2 for all the walls
ang = 0.3;%30. For 0.3, r*angle=10.46 or 10 microns. 
xs = 2000;%150, moves the particle radially (channel radius varies from 100-200 microns)
ys = 18;%4.95, It moves the particle into the channel
zs = 0;%-10;%15, moves the particle up and down

[r_s] = evenSphere(xs, ys, zs, 4.95, s);%116 points
[r_s4] = CurvedPlateTfunction(1950, 2050, ang, -25, s2); %0 changed to -25 % 295points
[r_s2] = Cylindersectfunction(0 , 0, -25, 2050, 50, s2, ang);% % 136points, Cylindersectfunction(2050,25,s);%50 changed to 25
[r_s1] = CurvedPlateTfunction(1950, 2050, ang, 25, s2);%% 295points , 50 changed to 25
[r_s3] = Cylindersectfunction(0 , 0, -25, 1950, 50, s2, ang);% 128 points, 50 changed to 25

r_w = [r_s1;r_s2;r_s3;r_s4];%length(r_w)=854 points on walls

r_s_mat = cell2mat(r_s);%Mehdi added this
r_w_mat = cell2mat(r_w);%Mehdi added this

%To get rid of overlap of points of each two perpendicular walls(at the cross section)
i = 1;
while 1
    xi = r_w_mat(i,:);%since we made the walls row by row. we go through each row to find overlaps
    xx = r_w_mat(i+1:end,:)-repmat(xi,length(r_w_mat(i+1:end,1)),1);%repmat(A,2,3)gives repetition of matrix A in a 2*3
    Normx = sqrt(dot(xx,xx,2));%dot(xx,xx,2)=xx.^2;
    [a,b] = find(Normx==0);
    r_w_mat(i+a,:) = [];
    i = i + 1;
    if i>length(r_w_mat(:,1)); break; end
end

%[G,IRX] = G_mat(r_s,r_w,s/3,s2/3);%%%%%%%%%%

%return

% for k = 1:length(r_s)
%     xo(k) = r_s{k}(1);
%     yo(k) = r_s{k}(2);
%     zo(k) = r_s{k}(3);
% end
% 
% for k = 1:length(r_s4)
%     x4(k) = r_s4{k}(1);
%     y4(k) = r_s4{k}(2);
%     z4(k) = r_s4{k}(3);
% end
% 
% for k = 1:length(r_s2)
%     x2(k) = r_s2{k,1}(1);
%     y2(k) = r_s2{k,1}(2);
%     z2(k) = r_s2{k,1}(3);
% end
% 
% for k = 1:length(r_s1)
%     x1(k) = r_s1{k}(1);
%     y1(k) = r_s1{k}(2);
%     z1(k) = r_s1{k}(3);
% end
% 
% for k = 1:length(r_s3)
%     x3(k) = r_s3{k,1}(1);
%     y3(k) = r_s3{k,1}(2);
%     z3(k) = r_s3{k,1}(3);
% end
% 
% x = [xo, x2, x1, x3, x4];y = [yo, y2, y1, y3, y4];z = [zo, z2, z1, z3,z4];%we need to put all of them in one array z in this part of coding,
% %unfortunately, gets related to the griddata and cause elements of X,Y and
% %z are not the sam gives error, so we commented them! 
% figure 
% plot3(x, y, z,'*') 
% hold on 
% plot3(xo, yo, zo, 'r*')

%t_w = length(r_s4)+length(r_s1)+length(r_s2)+length(r_s3);%total points on the walls

%% Update G & IRX
%r_s = cell2mat(r_s);%should I have these two here
%r_w = cell2mat(r_w);    

% [G,IRX] = G_mat(r_s_mat,r_w,s/3,s2/3);


  
 %%   
%interpolation to find velocity for points (on the particle)we do not know their velocities....

%r_s_mat = cell2mat(r_s);%should I change it to have same definition

v_s = -griddata(r, z, v, r_s_mat(:, 1), r_s_mat(:, 2));
u_s = -griddata(r, z, u, r_s_mat(:, 1), r_s_mat(:, 2));
w_s = -griddata(r, z, w, r_s_mat(:, 1), r_s_mat(:, 2));

%matri zeros(3*length(t_w),1), to add velocities of walls'stokeslets to
%Ldot
velCEll = [v_s u_s w_s];
%mat2cell(v_s, ones(length(v_s),1), 1)
mat2cell(velCEll, ones(length(v_s),1), 3);%when I get w
Ldot = cell2mat(mat2cell(velCEll, ones(length(v_s),1), 3)')';% Ldot matrix for particle(sphere)
Ldotw = zeros(3*length(r_w) ,1);% Ldot matrix for 4 walls

j = 1;

dt = t(2) - t(1);

P_Lab = [xs, ys, zs,0,0,0];%we put r_o = [0,0,0];
P_Lab_0 = [xs, ys, zs,0,0,0]';
V_Lab = zeros(6,1);%3components of translational V, 3 components of rotational V

s = 1.5;%delta s
e_1 = s / 3; %e_  is 1/3 of delta(distance bw each stokeslet)
mu = 0.001; %fluid viscosity
a_ = 0.05;%radius of each stokeslet
r_o = [0,0,0];%for sphere particle, r_o is the relative......
R = eye(3);

for k = 1:1:length(t)
    
    kt = cputime;
    
    %% Update velocities stokeslets
    
    r_s_mat = cell2mat(r_s);
    
    U = scatteredInterpolant(r', z', u');
    V = scatteredInterpolant(r', z', v');
    W = scatteredInterpolant(r', z', w');
    
    u_s = -U(r_s_mat(:, 1), r_s_mat(:, 2));
    v_s = -V(r_s_mat(:, 1), r_s_mat(:, 2));
    w_s = -W(r_s_mat(:, 1), r_s_mat(:, 2));
    
    %v_s = -(griddata(r, z, v, r_s_mat(:, 1), r_s_mat(:, 2)));
    %u_s = -(griddata(r, z, u, r_s_mat(:, 1), r_s_mat(:, 2)));
    %w_s = -(griddata(r, z, w, r_s_mat(:, 1), r_s_mat(:, 2)));
    
    r_o = P_Lab(1:3);%relative position of ....
    
    us(k) = max(u_s);
    %update L_dot
    velCEll = [u_s v_s w_s];% only L_dot sphere changes over time
    
    %L_dot = cell2mat(mat2cell(velCEll, ones(length(v_s),1), 3)')';% Ldot
    %matrix for particle
    
    L_dot = [cell2mat(mat2cell(velCEll, ones(length(v_s),1), 3)')'; Ldotw]; %when we have sphere and walls
    
    %Mehdi said I do not need it anymore, since we have removed singularties because of points overlap!
    % L_dot(isnan(L_dot)) = 0;
    
    
    % [G,IRX] = G_mat(r_s_mat,r_w,s/3,s2/3);
    
    r_ = [r_s; r_w];
    
    G_cell = cell(length(r_));% 116�116 cell array
    
    e_ = s;
    
    S_diag = 1/(4*pi*a_*e_);
    
    for alpha = 1:1:length(r_)     % Iterate over spheres
        for beta = 1:1:length(r_)
            if alpha == beta                         %This would mean the same sphere
                diag_cell = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
            else                              %Hydrodynamic Forces
                diag_cell = stokeslet_e(r_{alpha} - r_{beta}, e_, mu) * 1/a_;
            end
            G_cell{alpha,beta} = diag_cell;    % Append to G matrix
        end
    end
    G = cell2mat(G_cell);    
    
    IRX_cell = cell(length(r_), 1);
        
        for cll = 1:1:length(r_)
            IRX_cell{cll,1} = [1 0 0 0                     (r_{cll}(3)-r_o(3))     -(r_{cll}(2)-r_o(2)); 
                               0 1 0 -(r_{cll}(3)-r_o(3)) 0                        (r_{cll}(1)-r_o(1)); 
                               0 0 1 (r_{cll}(2)-r_o(2))  -(r_{cll}(1)-r_o(1))    0];
        end
        
    IRX = cell2mat(IRX_cell);
    
    LJ = Force_LJ(r_s, r_w);
         
    
    %G = G(1:3*length(r_s),1:3*length(r_s));     IRX = IRX(1:3*length(r_s),:); L_dot = L_dot(1:3*length(r_s));
    
    
    %% Update M
    %for sphere
    M = IRX' * G^-1 * IRX;
    
    
    %% Update body velocities
    
    V_Body = -M^-1 * (IRX)' * G^-1 * L_dot + M^-1* IRX* LJ ;%* (IRX)' * LJ^-1 * L_dot??????
    
    %% Transform to Lab Basis %from body frame to lab frame
    
    Omega = V_Body(4:6);
    V = V_Body(1:3);
    
    if k > 1
        %R = Rotation_dt(Omega, theta, dt) * R;%it does not matter in
        %sphere shape, rotation of sphere does not..... But we need to
        %consider it for Ellipsoid, RBC, Sperm and Chromosome!
    end
    
    if length(V(:,1)) ~= 3
        V = V';
    end
    if length(Omega(:,1)) ~= 3
        Omega = Omega';
    end
    
    V_Lab(1:3) = R * V;%3 components of translational velocity
    V_Lab(4:6) = R * Omega;%4:6 are 3 components of rotational velocity
    
    
    
    %% Integrate velocity to get position
    if length(P_Lab(:,1)) ~= 6
        P_Lab = P_Lab';
    end
    if length(V_Lab(:, 1)) ~= 6
        V_Lab = V_Lab';
    end
    
    % Update position in Lab frame
    if k > 1
        P_Lab = (P_Lab + V_Lab.* dt);
    end
    theta = P_Lab(4:6);
    
    r_s = evenSphere(P_Lab(1), P_Lab(2), P_Lab(3), 4.95, s);
    r_s_mat = cell2mat(r_s);
    
    hold on
    plot(r_s_mat(:,1), r_s_mat(:,2), 'b.')
    hold off
    
    
    vx_par(k) = V_Lab(1);
    vy_par(k) = V_Lab(2);
    
    px_par(k) = P_Lab(1) - P_Lab_0(1);%P_Lab_0(1) is x0
    py_par(k) = P_Lab(2) - P_Lab_0(2);%P_Lab_0(2) is y0
    
    
    Omegax_par(k) = V_Lab(4);
    Omegay_par(k) = V_Lab(5);
    
    thetax_par(k) = P_Lab(4) - P_Lab_0(4);
    thetay_par(k) = P_Lab(5) - P_Lab_0(5);
    
    calcTime = cputime - kt;
    fprintf('Calculation Time: %i \n', calcTime);
    fprintf('Iteration: %i\n', k);
    %j = k+1;
    
end

figure
plot(t, vx_par)

hold on
plot(t, vy_par)
%plot(t, vz_par)
hold off
title('velocity plot')
legend('x', 'y')

figure
plot(t, us)

figure
plot(t, px_par)
hold on

plot(t, py_par)
hold off
title('position plot')
legend('x', 'y')

figure
plot(t, Omegax_par)

hold on
plot(t, Omegay_par)
hold off
title('rotational velocity plot')
legend('x', 'y')

figure
plot(t, thetax_par)
hold on

plot(t, thetay_par)
hold off
title('rotational position plot')
legend('x', 'y')









