%CM of Sperm function 

function [C_M] = CM_Sperm(l, x, y, z, R)
    

%initialize cell array
%I had selected all of the parameters of a_= 1; e_=1; e=1; and sig = 1
     a_= 0.05; e_=1.5; e=1;
     
    F_cell = cell(length(r_w), length(r_s));% 116�116 cell array
    F_cell2 = cell(length(r_s), length(r_w));
    S_diag = 1/(4*pi*a_*e_);
    
    for alpha = 1:1:length(r_w)     % Iterate over spheres
        for beta = 1:1:length(r_s)
            %we never compare any point with itself
%             if alpha == beta                         %This would mean the same sphere
%                 diag_cell = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
%             else                              %Hydrodynamic Forces

            sig = a_;% is the characteristic diameter of the molecule
            %e is the characteristic energy, which is the maximum energy of attraction between the molecules
            rij = r_w{alpha} - r_s{beta};
            r = sqrt(rij * rij');%Checked line 4 of stokeslet_e.m file
            diag_cell = -48 * e / (r)^2 * ((sig/r)^12-1/2*(sig/r)^6).* [1 1 1]' * rij;
            %end
            F_cell{alpha,beta} = diag_cell; % Append to G matrix
        end
    end
    
     F1 = cell2mat(F_cell);  
    
     for alpha = 1:1:length(r_s)     % Iterate over spheres
        for beta = 1:1:length(r_w)
            %we never compare any point with itself
%             if alpha == beta                         %This would mean the same sphere
%                 diag_cell = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
%             else                              %Hydrodynamic Forces

            sig =  a_;%cell radius
            
            rij = r_s{alpha} - r_w{beta};
            r = sqrt(rij * rij');
            diag_cell = -48 * e / (r)^2 * ((sig/r)^12-1/2*(sig/r)^6).* [1 1 1]' * rij;
            %end
            F_cell2{alpha,beta} = diag_cell;    % Append to G matrix
        end
        
     end
    
    %size(diag_cell);%r_s and r-w
    F2 = cell2mat(F_cell2);  

    %size(F1)%= 3843       348
    %size(F2)%=  348        3843
    %It should be 1281*3 and 116*3
    X1 = zeros(3*length(r_w));%= 3843 3843
    X2 = zeros(3*length(r_s));%= 348 348
    
    F = [F1 X1; X2 F2];
    I  = ones(length(F),1);%size(I):Used identity matrix to make size(F)=4191*4191 to size(I)=4191*1
    F = F*I;
    size(F);
    
end