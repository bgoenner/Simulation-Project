function [G] = G_Matrix(r_, a_, mu, e_)

    G_cell = cell(length(r_));% 116�116 cell array
    
    S_diag = 1/(4*pi*a_*e_);
    
    for alpha = 1:1:length(r_)     % Iterate over spheres
        for beta = 1:1:length(r_)
            if alpha == beta                         %This would mean the same sphere
                diag_cell = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
            else                              %Hydrodynamic Forces
                diag_cell = stokeslet_e(r_{alpha} - r_{beta}, e_, mu) * 1/a_;
            end
            G_cell{alpha,beta} = diag_cell;    % Append to G matrix
        end
    end
    G = cell2mat(G_cell);  


end