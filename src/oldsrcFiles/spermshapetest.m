%test
clc
clear all


s = 1.5;%before 1.3 before.... 0.5
s2 = 1.5;%7
e_2 = s2/3;

ang = 0.3;%30

 xsp = 2000;%2000;%150, moves the particle radially (channel radius varies from 100-200 microns)
 ysp = 0;%4.95, It moves the particle into the channel
 zsp = -52.75;%-25, moves the particle up and down
 x_c = 2000;
 y_c = 0;
 z_c = -53;
 R = 0.5;
 Ang = 360;
 s_e = 0.5;
 s_t = 0.3;
 s_e1 = 0.1;
 delta = 0.1;
%bBuild sperm
 [r_s6] = EllipseFunction(xsp, ysp, zsp, 1.5, 1.5, 2.5, s_e);%delta=0.001, to get horizontal ellipsoid 2.5, 1.5 
 [r_s7] = Cylindersectfunction(xsp, ysp, zsp, 0.5, 50, s_t, Ang);%R, height, delta should be very small, angle
 [r_s8] = CircularPlatefunction(x_c, y_c, z_c, R, Ang, delta);

[r_s] = [r_s6;r_s7;r_s8];

[r_s4]  = CurvedPlateTfunction(1950, 2050, ang, -25, s2); %0 changed to -25 % 295points
[r_s2] = Cylindersectfunction(0 , 0, -25, 2050, 50, s2, ang);% % 136points, Cylindersectfunction(2050,25,s);%50 changed to 25
[r_s1] = CurvedPlateTfunction(1950, 2050, ang, 25, s2);%% 295points , 50 changed to 25
[r_s3] = Cylindersectfunction(0 , 0, -25, 1950, 50, s2, ang);% 128 points, 50 changed to 25

r_w = [r_s1;r_s2;r_s3;r_s4];%length(r_w)=854 points on walls

%To get rid of overlap of points of each two perpendicular walls(at the cross section)
r_s_mat = cell2mat(r_s);%Mehdi added this
r_w_mat = cell2mat(r_w);%Mehdi added this


for k = 1:length(r_s)
    xo(k) = r_s{k}(1);
    yo(k) = r_s{k}(2);
    zo(k) = r_s{k}(3);
end

for k = 1:length(r_s4)
    x4(k) = r_s4{k}(1);
    y4(k) = r_s4{k}(2);
    z4(k) = r_s4{k}(3);
end

for k = 1:length(r_s2)
    x2(k) = r_s2{k,1}(1);
    y2(k) = r_s2{k,1}(2);
    z2(k) = r_s2{k,1}(3);
end

for k = 1:length(r_s1)
    x1(k) = r_s1{k}(1);
    y1(k) = r_s1{k}(2);
    z1(k) = r_s1{k}(3);
end

for k = 1:length(r_s3)
    x3(k) = r_s3{k,1}(1);
    y3(k) = r_s3{k,1}(2);
    z3(k) = r_s3{k,1}(3);
end

x = [xo, x2, x1, x3, x4];y = [yo, y2, y1, y3, y4];z = [zo, z2, z1, z3,z4];%we need to put all of them in one array z in this part of coding,
%unfortunately, gets related to the griddata and cause elements of X,Y and
%z are not the sam gives error, so we commented them! 
figure 
plot3(x, y, z,'*') 
hold on 
plot3(xo, yo, zo, 'r*')

