
function [IRX] = IRX_Matrix(r_, r_o)

    IRX_cell = cell(length(r_), 1);
        
    for cll = 1:1:length(r_)
        IRX_cell{cll,1} = [1 0 0 0                     (r_{cll}(3)-r_o(3))     -(r_{cll}(2)-r_o(2)); 
                           0 1 0 -(r_{cll}(3)-r_o(3)) 0                        (r_{cll}(1)-r_o(1)); 
                           0 0 1 (r_{cll}(2)-r_o(2))  -(r_{cll}(1)-r_o(1))    0];
    end
        
    IRX = cell2mat(IRX_cell);

end