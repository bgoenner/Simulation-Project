 % SpheretestClass.m
clc
clear
close all

%fileSavePath = 'C:\Users\bg\Box Sync\Simulation\bg\';

close all

% what needs to be changed in a new simulation
% inital geometry definition


% simulation time parameters
itera  = 700;
deltaT = 1e-6;

t = linspace(deltaT, (itera)*deltaT, itera);

%% Background flow

%initialize background flow
dat_dat = csvread('Vfield.csv',1);
r = dat_dat(:,1)';%which is x axis
z = dat_dat(:,2)';%which is y axis
u = dat_dat(:,3)'*10e6;%which is Ux axis
v = dat_dat(:,5)'*10e6;%which is Uz axis
w = dat_dat(:,4)'*10e6;%which is Uy axis

%% simulation parameters


e_2 = s2/3;

% sphere intial parameters
% position
xs = 2.0000e3;
ys = -0.0008e3;
zs = 0e3;

% radius
rs = ro;
% delta 
s1 = 0.1;
s2 = 1.5;

% channel center

% channel radius

% channel angle in radians
ang = 0.1;

% delta time
dt = t(2) - t(1);

% intial sphere position and velocity
P_body(1,:) = [xs, ys, zs, 0, 0, 0]';  % initial position
V_body(1,:) = [0, 0, 0, 0, 0, 0]';     % initial velocity is zero

% channel center
CC = [2000, 0 , 0];

% stokeslet sphere radius
a = s1/10;
e = s1^0.9;

% fluid viscosity
mu = 8.9*10^-4;

% create interpolant objects

U = scatteredInterpolant(r', z', u');
V = scatteredInterpolant(r', z', v');
W = scatteredInterpolant(r', z', w');


%% intialize geometry

% move to inside of the loop to reduce duplication of code

r1 = 2;
r2 = 2;
dAng = 0.1;

% convergence study


% plots walls of spiral channel
[r_s4] = CurvedPlateFunction(1950, 2050, ang, -25, s2); %0 changed to -25 % 295points
[r_s2] = CylinderSectionFunction(0 , 0, -25, 2050, 50, s2, ang);% % 136points, Cylindersectfunction(2050,25,s);%50 changed to 25
[r_s1] = CurvedPlateFunction(1950, 2050, ang, 25, s2);%% 295points , 50 changed to 25
[r_s3] = CylinderSectionFunction(0 , 0, -25, 1950, 50, s2, ang);% 128 points, 50 changed to 25
 % combine structure
r_w = unique(cell2mat([r_s1; r_s2; r_s3; r_s4]), 'rows');



%% geometry intial velocities
%v_s  = ones(length(r_s),1) * 3;
Ldotw = zeros(length(r_w) * 3, 1);



% Test with different LJ parameters
% set these to zero if they are not needed
e_LJ = 0;%1*10^2;
a_LJ = 0;%rs;

%% do not modify


channelPlot = figure;
% plot velocity field
hold on
    quiver(r,z,u,v);
    %plot(r_s(:,1), r_s(:,2), '.k')
hold off

red = 0;
green = 0;
blue = 0;


%% Simulation  iteration
t_(1) = 0;
V_G(1,:)  = [0,0,0,0,0,0];
V_LJ(1,:) = [0,0,0,0,0,0];



for k = 1:length(t)
    %% do not modify
    start = cputime;

    % regenerate body rotated
    % z is set with zero


    bodyk1 = P_body(k, :);
    bodyk1(3) = 0;

    %% body defintion
    % modify the definition of r_s as the body geometry of the 
    % particle
    %
    % r_s need to be defined as a 3xn matrix
    r_s = evenSphere(P_body(k, 1), P_body(k, 2), 0, rs, s1);
    r_s = unique(cell2mat(r_s), 'rows');

    %% do not modify
    

    % rotates body

    r_s = LowReynoldsSimulation.rotation_Body(r_s, bodyk1);

    % plot sphere position

    if red >= 0.9 || green >= 0.9
        if green < 0.9
            green = green + 0.05;

        elseif green >= 0.9
            red = 0;
            blue = blue + 0.05;

            if blue >= 0.9
                red = 0;
                green = 0;
                  blue = 0;
            end
        end
    else
        red = red + 0.05;
    end

    hold on
        plot(r_s(:,1), r_s(:,2), '.', 'color', [red, green, blue])
    hold off


    % interpolate velocities
    % adjusts for x due to z position
    r_s_x_prime = (r_s(:,1).^2 + r_s(:,3).^2 ).^(1/2);
    u_s = -U(r_s_x_prime , r_s(:, 2));
    v_s = -V(r_s_x_prime , r_s(:, 2));
    w_s = -W(r_s_x_prime , r_s(:, 2));

    l_dot = [cell2mat(mat2cell([u_s v_s w_s], ones(length(v_s),1), 3)')'; Ldotw];

    body = P_body(k, :);
    body(3) = 0;
    %V_body(k+1,:) = LowReynoldsSimulation.iterate_multi({r_s, r_w}, {P_body(k, 1:3), CC}, l_dot, a, e, mu, a_LJ, e_LJ);
    [V_body(k+1,:), V_G(k+1, :), V_LJ(k+1, :)] = LowReynoldsSimulation.iterate_multi({r_s, r_w}, {body, body}, l_dot, a, e, mu, a_LJ, e_LJ);
    %V_body(k+1,:) = LowReynoldsSimulation.iterate_multi({r_s}, {P_body(k, 1:3)}, cell2mat(mat2cell([u_s v_s w_s], ones(length(u_s),1), 3)')', a, e, mu, a_LJ, e_LJ);

    P_body(k+1,:) = P_body(k,:) + dt * V_body(k + 1,:);

   
    % display iteration time
    iterationTime = cputime - start;

    fprintf('Iteration: %i \t\tCalculation Time: %i\n', k, iterationTime);

    t_(k+1) = t(k);

    if P_body(k+1, 1) > 2057 || P_body(k+1, 1) < 1958 || P_body(k+1, 2) > 25 || P_body(k+1, 2) < -25
        fprintf('Particle has moved outside the channel current position: \n x:%i \n y:%i \n z:%i \n\n\n', P_body(1), P_body(2), P_body(3)) 
        break; 
    end

end

today = replace(replace(datestr(datetime()), ':', ''), ' ', '_');
fileName = '_SphereTest_';
user = getenv('username');
workspaceFile = 'Workspaces\';
figureFile = 'Figures\';
fileType = '.jpeg';

%savefig(join([fileSavePath, figureFile, fileName, '_channelPlot_', user, today], ''))

saveas(channelPlot, join([fileSavePath, figureFile, fileName, '_channelPlot_', user, today, fileType], ''))

velPlot = figure; % velocity plot
subplot(4, 1, 1)
hold on
plot(t_, V_body(:, 1))
plot(t_, V_body(:, 2))
plot(t_, V_body(:, 3))
hold off
title('velocity plot')
legend('x', 'y', 'z')

%figure % position plot
subplot(4, 1, 2)
hold on
plot(t_, (P_body(:, 1) - P_body(1,1)))
plot(t_, P_body(:, 2) - P_body(1,2))
%plot(t_, P_body(:, 3) - - P_body(3))
hold off
title('position plot')
legend('x', 'y', 'z')

% rotational velocity plot
subplot(4, 1, 3)
hold on
plot(t_, V_body(:, 4))
plot(t_, V_body(:, 5))
plot(t_, V_body(:, 6))
hold off
title('rotational velocity plot')
legend('x', 'y', 'z')

subplot(4, 1, 4)
hold on
plot(t_, P_body(:, 4) - P_body(1,4))
plot(t_, P_body(:, 5) - P_body(1,5))
plot(t_, P_body(:, 6) - P_body(1,6))
hold off
title('rotational position plot')
legend('x', 'y', 'z')

% Saves the current workspace

saveas(velPlot, join([fileSavePath, figureFile, fileName, '_velocityPlot_', user, today, fileType], ''))

%savefig(join([fileSavePath, figureFile, fileName, '_velocityPlot_', user, today], ''))
save(join([fileSavePath, workspaceFile, fileName, user, today], ''))

fig = figure;
plot(t_, V_G)
hold on
plot(t_, V_LJ)
hold off

% you can load the workspace by load(filename)

