%Top_Bottom Circular Plate 

function [R_S] = CircularPlatefunction(x_c, y_c, z_c, R, Ang, delta)%CircularPlatefunction(R, Ang, delta)


    n = (R)/delta;%Number of points,
    %1 degree = pi/180;
    
    Ang = (Ang * pi)/180;%Radians to Degree

    x = @(the, R) R*cos(the);%
    y = @(the, R) R*sin(the);%
       
    x_ = [];
    y_ = [];%the original code was without z_ = []
    z_ = [];

    %arcl; Ang;% Angle of the sector to which Plate needed, Ang = 90 or 45 ...;  
    
    
    r = linspace(0, R, n);%we have variety of redia

    arcl = r.*Ang;%Arclength for each radius , r(i).....arcl(i)
    
    for i = 1:1:length(r)
        
        m = arcl(i)/delta;%Number of points  
        theta = linspace(0, Ang, m);
        
        for j = 1:1:length(theta)
            
            x_ = [x_, x(theta(j), r(i)) + x_c];
            y_ = [y_, y(theta(j), r(i)) + y_c];%original code was without z_ = [z_, z]
            z_ = [z_, z_c];
        end
    end
    
    R_S = cell(length(x_), 1);
    
    for i = 1:1:length(x_)
        R_S{i} = [x_(i), y_(i), z_(i)];%original code was without z_(i)
    end

end