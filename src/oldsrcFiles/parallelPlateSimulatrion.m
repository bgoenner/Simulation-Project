% Wall simulation



p1 = stokesletplate(5, -2, -2, 4, 4, 0.5);
p2 = stokesletplate(0, -2, -2, 4, 4, 0.5);

p = cell2mat([p1, p2]);

plot3(p)