%Top_Bottom Curved Plate 
function [R_S] = CurvedPlateTfunction(IR, OR, Ang, z, delta)

    n = (OR-IR)/delta;%Number of points
    
    Ang = (Ang * pi)/180;%Radians to Degree, 1 degree = pi/180;

    x = @(the, R) R*cos(the);%
    y = @(the, R) R*sin(the);%
    
    x_ = [];
    y_ = [];
    z_ = [];

    %IR; OR; OR>IR; arcl; Ang;% Angle of the sector to which Plate needed, Ang = 90 or 45 ...;  
    
    
    r = linspace(IR, OR, n);%we have variety of redia

    arcl = r.*Ang;%Arclength for each radius , r(i).....arcl(i)
    
    for i = 1:1:length(r)
        
        m = arcl(i)/delta;%Number of points  
        theta = linspace(-Ang/2, Ang/2, m);
        
        for j = 1:1:length(theta)
            
            x_ = [x_, x(theta(j), r(i))];
            y_ = [y_, y(theta(j), r(i))];
            z_ = [z_, z];
           
        end
    end
    
    R_S = cell(length(x_), 1);
    
    for i = 1:1:length(x_)
        R_S{i} = [x_(i), y_(i), z_(i)];
    end

end