
clc
clear
close all

%

% initialize PDE model
model = createpde(3);

% import STL
importGeometry(model, './ThumbScrew.STL')

% Generate mesh using geometry
generateMesh(model)
pdeplot3D(model)

% Generate mesh using max element size 0.5
mesh = generateMesh(model, 'Hmax', 0.5);
figure
pdeplot3D(model)

% get node ids by face ID
try
nodeIDs = findNodes(mesh, 'region', 'face', 1:11);
catch ME
    
end
% get node points
nodes = mesh.Nodes(1:3, nodeIDs);

% plot node points
figure
plot3(nodes(1,:), nodes(2,:), nodes(3,:), '.')
