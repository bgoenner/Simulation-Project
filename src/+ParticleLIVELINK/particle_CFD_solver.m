function [Force, Moment] = particle_CFD_solver(model_name, boundary, constant, arg_particle)

    %% COMSOL Parameters
    
    % Channel parameters
    % ch - channel height [um]
    % cw = channel width [um]
    % cr - channel radius [um]
    % Dhyd - hydrolic diameter [m]
    % 
    %
    % Fluid parameters
    % FR - flow rate [L/min]
    % mu - viscosity [Pa s]
    % rho - density [kg m^-3]
    % Uavg - average inlet velocity
    % VoIF - volumeteric inflow force
    % 
    % 
    % Particle parameters
    % da - particle parameters [um]
    % xp - particle center postion X [um]
    % yp - particle center postion Y [um]
    % zp - particle center postion Z [um]
    % rrx0 - particle init rotational velocity [1/s]
    % rry0 - particle init rotational velocity [1/s]
    % rrz0 - particle init rotational velocity [1/s]
    % Ipart - rotational moment of inertia
    % vvv - particle velocity
    % some additional parameters will need to be defined by other means
    % possilbly with and array [ para1, value1, unit1; para2, value2, unit2; ... ]
    %
    % Other parameters
    % alpha


    %% imports and path inits
    
    comsol_dir = 'C:\Program Files\COMSOL\COMSOL54\Multiphysics\mli';
    try
        mphstart(2036);
    end
    
    addpath(comsol_dir)
    
    import com.comsol.model.*;
    import com.comsol.model.util.*;
    
    
    %model = ModelUtil.create('Model');
    model = mphload(model_name);

    format compact
    format long
    datestr(now)
    
    %% Iteration Settings
    maxcount=20; % Maximum number of iterations allowed per position
    Dt=8e-3; % 5e-3 for 9.9um Time step for calculating changes in velocities
    NN=10; % Number of positions per axis
    damping=1; % Damping value for all velocity updates
    
    %model.hist.disable
    %% FEM setup
    
    %% Parameters from COMSOL
    % Fluid Density and Viscosity
    mustr=char(model.param.get('mu'));
    muunit=mustr(strfind(mustr,'['):length(mustr));
    mu=str2num(mustr(1:strfind(mustr,'[')-1));
    
    rhostr=char(model.param.get('rho'));
    rhounit=rhostr(strfind(rhostr,'['):length(rhostr));
    rho=str2num(rhostr(1:strfind(rhostr,'[')-1));
    
    % This is missing from one of the models
    %FRstr=char(model.param.get('FR'));
    %FRunit=FRstr(strfind(FRstr,'['):length(FRstr));
    %FR=str2num(FRstr(1:strfind(FRstr,'[')-1));
    

    cwval = model.param.evaluate('cw');
    chval = model.param.evaluate('ch');
    %crval = model.param.set('cr',strcat(num2str(crval),crunit));
    alpha1=1;
    %FRval = str2num(model.param.get('FR'));
    %Uavgval=FRval/1000/60/(chval/1e6)/(cwval/1e6);

    
%     alpha1=0.75;
%     aunit='[m/m]';
%     model.param.set('alpha',strcat(num2str(alpha1),aunit));
%     disp('soln run 1')
%     model.sol('sol1').run;
%     Uavgm75=mphglobal(model,'comp1.Uavgm','dataset','dset1');
%     alpha1=1;
%     aunit='[m/m]';
%     model.param.set('alpha',strcat(num2str(alpha1),aunit));
%     disp('soln run 2')
%     model.sol('sol1').run;
%     Uavgm100=mphglobal(model,'comp1.Uavgm','dataset','dset1');
%     alpha1=1.25;
%     aunit='[m/m]';
%     model.param.set('alpha',strcat(num2str(alpha1),aunit));
%     disp('soln run 3')
%     model.sol('sol1').run;
%     Uavgm125=mphglobal(model,'comp1.Uavgm','dataset','dset1');
%     p=polyfit([Uavgm75 Uavgm100 Uavgm125],[0.75 1 1.25],2);
%     alpha1=polyval(p,Uavgval);
%     aunit='[m/m]';
%     model.param.set('alpha',strcat(num2str(alpha1),aunit));
%     disp('Done init sol1')
%     disp('Running sol1...')
    
%     model.sol('sol1').run;

    % VolF=alpha1*Uavgval/1.413655369454722*10000000;
    % VolFunit='[N/m^3]'; ' mod1.Uavgm'
    % Uavgm=mphglobal(model,'comp1.Uavgm','dataset','dset1');
    % Dhyd(ll)=(2*cwval*chval)/(cwval+chval); % Hydraulic diameter in microns 
    % ReyC(ll)=rho*Uavgval/mu*Dhyd(ll)*10^-6*3/2; % Channel Reynolds number
    % Dean(ll)=ReyC(ll)*(Dhyd(ll)/(2*crval))^0.5; % Dean number for given channel parameters
    % alphafinal(ll)=alpha1;
    
    
    % particle definitions
%     daunit='[um]';
%     model.param.set('da',strcat(num2str(daval),daunit));
%     ypunit='[um]';
%     model.param.set('yp',strcat(num2str(ypval),ypunit));
%     zpunit='[um]';
%     model.param.set('zp',strcat(num2str(zpval),zpunit));
    
%     for kk = 1:1:length(arg_particle(:,1))
%         param_unit = arg_particle(kk,2);
%         model.param.set(arg_particle(kk,1), strcat(num2str(arg_particle(kk,3)),param_unit))
%         
%     end
    %velunit='[m/s]';
    %model.param.set('vvv',strcat(num2str(velval),velunit));
    %yomunit='[1/s]';
    %model.param.set('rry0',strcat(num2str(yomval0),yomunit));
    %zomunit='[1/s]';
    %model.param.set('rrz0',strcat(num2str(zomval0),zomunit));
    
    %% Solver
    
    Force =  cell(length(boundary{1}), length(boundary{2}));
    Moment = cell(length(boundary{1}), length(boundary{2}));

    
    Dt1=Dt; % Reset Dt1 for each position
    count=1;
    cut=.005;
    
    
    daval = model.param.evaluate('da'); % particle diameter
    
    disp('Running sol1...')
    model.sol('sol1').runAll;
    
    % These two loops iterate over the different particle positions
    for ii = 1:length(boundary{1})
        for ij = 1:length(boundary{2})
            % this will require multiple checks
            %while (Force{1}(ii, ij) > 1e-6) || (Force{2}(ii, ij) > 1e-6) || (Force{3}(ii, ij) > 1e-6) ...
            %        || (Moment{1}(ii, ij) > 1e-6) || (Moment{2}(ii, ij) > 1e-6) || (Moment{3}(ii, ij) > 1e-6)
                % These two loops iterate to reduce the force of the simulation
                % to zero

            velval =0.0;%particle velocity value
            yomval0=0.0;%rotational velocities
            zomval0=0.0;
            xomval0=0.0;

            xounit='[um]';
            xoval=boundary{1}(ii);
            model.param.set('xp',strcat(num2str(xoval),xounit));
            yomunit='[um]';
            yoval=boundary{2}(ij);
            model.param.set('yp',strcat(num2str(yoval),yomunit));
            zounit='[um]';
            zomval=constant;
            model.param.set('zp',strcat(num2str(zoval),zomunit));
                
            velunit='[m/s]';
            model.param.set('vvv',strcat(num2str(velval),velunit));
            yomunit='[1/s]';
            model.param.set('rry0',strcat(num2str(yomval0),yomunit));
            zomunit='[1/s]';
            model.param.set('rrz0',strcat(num2str(zomval0),zomunit));

            datestr(now)
            strtemp=['sol2 i=',num2str(count),'   vel=',num2str(velval),'   yomval=',num2str(yomval),'   zomval=',num2str(zomval),'   Dt1=',num2str(Dt1)];
            disp(strcat('Running sol2:[', num2str(boundary{1}(ii)),', ', num2str(boundary{2}(ij)), ']...'))
            
            model.sol('sol2').runAll;

            [Force1, Force2, Force3, Moment1, Moment2, Moment3] = getForceAndTorque(model);
            %Force1  =   mphint2(model, '-spf2.T_stressx', 'surface', 'dataset', 'dset2');%Drag X
            %Force2  =-1*mphint2(model, '-spf2.T_stressz', 'surface', 'dataset', 'dset2');%Drag Y
            %Force3  =   mphint2(model, '-spf2.T_stressy', 'surface', 'dataset', 'dset2');%Drag Z
            %Moment1 =   mphint2(model, '(y-yp2)*(-spf2.T_stressz)-(z-zp2)*(-spf2.T_stressy)', 'surface', 'dataset', 'dset2');%Moment X
            %Moment2 =   mphint2(model, '(z-zp2)*(-spf2.T_stressx)-(x-xp2)*(-spf2.T_stressz)', 'surface', 'dataset', 'dset2');%Moment Y
            %Moment3 =   mphint2(model, '(x-xp2)*(-spf2.T_stressy)-(y-yp2)*(-spf2.T_stressx)', 'surface', 'dataset', 'dset2');%Moment Z

            Mass1 =    model.param.evaluate('Mp');
            Minertia = model.param.evaluate('Ip');
            
            veldiff=Force1/Mass1*Dt1;
            M2diff=Moment2/Minertia*Dt1;
            M3diff=Moment3/Minertia*Dt1;  
            
            %end
            while (count<maxcount)
            count=count+1;
                if M3diff~=0 || M2diff~=0 || veldiff~=0
                datestr(now)
                strtemp=['sol2 i=',num2str(count),'   vel=',num2str(velval),'   yomval=',num2str(yomval),'   zomval=',num2str(zomval),'   DragX=',num2str(Force1),'   MomentY=',num2str(Moment2),'   MomentZ=',num2str(Moment3),'   Dt1=',num2str(Dt1)];
                disp(strtemp); 
                if (abs(Force1)>(1e-11)/(10/daval))&&abs(veldiff)>10e-5
                    veldiff=damping*Force1/Mass1*Dt1;
                    if (abs(veldiff)>0.25*max(abs(velval),1.0e-3))
                    veldiff=0.25*max(abs(velval),1.0e-3)*sign(veldiff);
                    %disp('yveldamped')
                    end
                else
                    veldiff=0;
                end
                if abs(Moment2)>1.e-15/(10/daval)&&abs(M2diff)>min(200,(yomval0+100)*0.2)
                    M2diff=damping*Moment2/Minertia*Dt1/2;
                    if (abs(M2diff)>0.2*max(abs(yomval),200))
                    M2diff=0.2*max(abs(M2diff),200)*sign(M2diff);
                    %disp('M1damped')
                    end
                else
                    M2diff=0;
                end
                if abs(Moment3)>1.e-15/(10/daval)&&abs(M3diff)>min(200,(zomval0+100)*0.2)
                    M3diff=damping*Moment3/Minertia*Dt1/2;
                    if (abs(M3diff)>0.2*max(abs(zomval),200))
                    M3diff=0.2*max(abs(M3diff),200)*sign(M3diff);
                    %disp('M3damped')
                    end
                else
                    M3diff=0;
                end

                velval=velval+veldiff;
                yomval=yomval+M2diff;
                zomval=zomval+M3diff;

                model.param.set('vvv',strcat(num2str(velval),velunit));
                model.param.set('rry0',strcat(num2str(yomval),yomunit));
                model.param.set('rrz0',strcat(num2str(zomval),zomunit));

                disp(count);
                datestr(now);
                disp(strcat('X:', num2str(xoval) ,'_Y:', num2str(yoval) ,'_Z:', num2str(zoval0) , '_vvv:', num2str(velval), '_rry0:', num2str(yomval), '_rrz0:', num2str(zomval)))
                model.sol('sol2').runAll;

                [Force1, Force2, Force3, Moment1, Moment2, Moment3] = getForceAndTorque(model);
                %Force1  =   mphint2(model, '-spf.T_stressx', 'surface', 'dataset', 'dset2', 'dataset', 'dset2');%Drag X
                %Force2  =-1*mphint2(model, '-spf.T_stressz', 'surface', 'dataset', 'dset2', 'dataset', 'dset2');%Drag Y
                %Force3  =   mphint2(model, '-spf.T_stressy', 'surface', 'dataset', 'dset2', 'dataset', 'dset2');%Drag Z
                %Moment1 =   mphint2(model, '(y-yp2)*(-spf.T_stressz)-(z-zp2)*(-spf.T_stressy)', 'surface', 'dataset', 'dset2', 'dataset', 'dset2');%Moment X
                %Moment2 =   mphint2(model, '(z-zp2)*(-spf.T_stressx)-(x-xp2)*(-spf.T_stressz)', 'surface', 'dataset', 'dset2', 'dataset', 'dset2');%Moment Y
                %Moment3 =   mphint2(model, '(x-xp2)*(-spf.T_stressy)-(y-yp2)*(-spf.T_stressx)', 'surface', 'dataset', 'dset2', 'dataset', 'dset2');%Moment Z


                if count>3
                    incvar1=0;
                    incvar2=0;
                    incvar3=0;
                    if veldiff*(vellist(count-1)-vellist(count-2))>0.0 || abs(veldiff)<1e-5
                        incvar1=1;
                    end
                    if M2diff*(yomvallist(count-1)-yomvallist(count-2))>0.0 || abs(M2diff)<10
                        incvar2=1;
                    end
                    if M3diff*(zomvallist(count-1)-zomvallist(count-2))>0.0 || abs(M3diff)<10
                        incvar3=1;
                    end
                    if incvar1*incvar2*incvar3>0
                        Dt1=min(Dt1*2.5,4*Dt);
                    else
                        Dt1=max(Dt1*2./2.9,0.2*Dt);
                    end
                end
                end
            end;
            
            [Force{1}(ii, ij), Force{2}(ii, ij), Force{3}(ii, ij), Moment{1}(ii, ij), Moment{2}(ii, ij), Moment{3}(ii, ij)] = getForceAndTorque(model);
%             Force{1}(ii, ij)  =   mphint2(model, '-spf.T_stressx', 'surface');%Drag X
%             Force{2}(ii, ij)  =-1*mphint2(model, '-spf.T_stressz', 'surface');%Drag Y
%             Force{3}(ii, ij)  =   mphint2(model, '-spf.T_stressy', 'surface');%Drag Z
%             Moment{1}(ii, ij) =   mphint2(model, '(y-yp2)*(-spf.T_stressz)-(z-zp2)*(-spf.T_stressy)', 'surface');%Moment X
%             Moment{2}(ii, ij) =   mphint2(model, '(z-zp2)*(-spf.T_stressx)-(x-xp2)*(-spf.T_stressz)', 'surface');%Moment Y
%             Moment{3}(ii, ij) =   mphint2(model, '(x-xp2)*(-spf.T_stressy)-(y-yp2)*(-spf.T_stressx)', 'surface');%Moment Z

        end
    end
   
    %% Postprocessing
    
    
end

function [out] = javaString2num(str)

    out = char(str);
    out(find(out == '['):end) = [];
    out = num2str(out);
end

function [Force1, Force2, Force3, Moment1, Moment2, Moment3] = getForceAndTorque(model)

    Force1  =   mphint2(model, '-spf2.T_stressx', 'surface', 'dataset', 'dset2');%Drag X
    Force2  =-1*mphint2(model, '-spf2.T_stressz', 'surface', 'dataset', 'dset2');%Drag Y
    Force3  =   mphint2(model, '-spf2.T_stressy', 'surface', 'dataset', 'dset2');%Drag Z
    Moment1 =   mphint2(model, '(y-yp2)*(-spf2.T_stressz)-(z-zp2)*(-spf2.T_stressy)', 'surface', 'dataset', 'dset2');%Moment X
    Moment2 =   mphint2(model, '(z-zp2)*(-spf2.T_stressx)-(x-xp2)*(-spf2.T_stressz)', 'surface', 'dataset', 'dset2');%Moment Y
    Moment3 =   mphint2(model, '(x-xp2)*(-spf2.T_stressy)-(y-yp2)*(-spf2.T_stressx)', 'surface', 'dataset', 'dset2');%Moment Z
end