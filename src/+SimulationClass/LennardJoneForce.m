function [F] = LennardJoneForce_sim(Body, F_0, sig)
      %initialize cell array
      %for i = 1:length(r_s)
      %  l = length(r_s{i}) + l;
      %end 
      
      F = cell(length(Body));

      for i = 1:length(Body)
        for j = 1:length(Body)
          if (i ~= j)
            F_cell = cell(length(Body{i}), length(Body{j}));
              for alpha = 1:length(Body{i})     % Iterate over spheres
                  
                  
                  for beta = 1:1:length(Body{j})
                      %we never compare any point with itself
            %             if alpha == beta                         %This would mean the same sphere
            %                 diag_cell = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
            %             else                              %Hydrodynamic Forces

                      %sig % is the characteristic diameter of the molecule
                      %e is the characteristic energy, which is the maximum energy of attraction between the molecules
                      rij = Body{i}(alpha, :) - Body{j}(beta, :);
                      rm = (rij * rij')^(1/2);%Checked line 4 of stokeslet_e.m file
                      %diag_cell = -48 * e / (r)^2 * ((sig/r)^12-1/2*(sig/r)^6).* [1 1 1]' * rij;
                      diag_cell = F_0 * sig / 6 * ((sig/rm)^12-(sig/rm)^6).* [1 1 1]' * rij/norm(rij);
                      %end
                      F_cell{alpha,beta} = diag_cell; % Append to G matrix
                  end
              end
            
              F{i, j} = cell2mat(F_cell);
            
          else
              X = zeros(length(Body{i})*3);

              F{i, j} = X;  
          end
        end   
      end
      
      F = cell2mat(F);
      
      I  = ones(length(F),1);%size(I):Used identity matrix to make size(F)=4191*4191 to size(I)=4191*1
      F = F*I;
end % end LennardJoneForce