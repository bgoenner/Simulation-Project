function [Pv] = Pressure_Stokeslets(Body, ep)
	
    l = 0;

    for i = 1:length(Body)
        l = l + length(Body{i});
    end

    Pv = zeros(3*l,3*l);

    l_m = 0;

    for i = 1:length(Body)
        l_j = 0;
        for j = 1:length(Body)
            for alpha = 1:length(Body{i})
                for beta = 1:length(Body{j})

                    % distance between stokeslets
                    r = Body{i}(alpha, :) - Body{j}(beta, :);
                    rm = sqrt(r * r');

                    % regularized Pressure
                    Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m) = Pv(alpha+l_m:alpha+l_m+2,1) + (r .* (2*rm^2 + 5*ep{i}^2)/(rm^2 + ep{i}^2)^(5/2))';
                    Pv(beta*3+l_j-1,alpha*3+l_m-2:alpha*3+l_m) = Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m);
                    Pv(beta*3+l_j-0,alpha*3+l_m-2:alpha*3+l_m) = Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m);
                end % end for beta
                % sum pressure at stokeslet

                % store in matrix
            end	% end for alpha		
            l_j = l_j + length(Body{j});
        end % end for j

        l_m = l_m + length(Body{i});
    end % end for i
	
end % end Pressure Function