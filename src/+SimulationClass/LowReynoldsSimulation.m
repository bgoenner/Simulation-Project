classdef LowReynoldsSimulation

  % A body structure that can be used to run simulations
  % the idea being the code will use the Object Oriented (OO) components
  % of matlab (how ever well they may work)

  properties
  % instance variables
  
    body = struct('points',[0,0,0], 'c_m',[0,0,0]);
    
    M = eye(6) 
    l_dot = [0,0,0] 
    BodyVel = [0,0,0]
    BodyPos = [0,0,0]
    s = 1 
    a = 0.1
    % c_m = [x y z]
    % bodyStructure = body[vector[x y z]]    
    % vel[linear, rotational]
    % pos[linear, rotational]
    % epsilon
    % stokeslet radius
    
    bodies = []; % array of bodies in class
    flow = [];

    % interpolation classes
    U_interp = [];
    V_interp = [];
    W_interp = [];
    
  end
  
  % initial body positions
  % 
    
  methods
    % Constructor

    % structure can be an arrayCell or matrix with equations
    % equations must be in body frame

    % simulation(structure)
%     function obj = LowReynoldsSimulation(structure)
% 
%       if iscell(structure)
%         % check cells are 3 x 1
%         
%         if size(structure{1}) == [3 1]
%           obj.bodyStructure = cell2mat(structure);
%           obj.bodyStructure = unique(self.bodyStructure, 'rows'); % remove repeated points
%         return
%         end % end if
%       end % end if
%       
%       if size(structure)[1] == [3]
%         
%         obj.bodyStructure = structure;
%         obj.bodyStructure = unique(self.bodyStructure, 'rows'); % remove repeated points
%         return
%       end % end if
%       
%       disp('Structure is not in correct format. Structure is of type: ', class(structure))
%       
%       
%     end % end LowReynoldsSimulation(structure)
    
    %function obj = LowReynoldsSimulation(structure, backgroundFlow)
    %  self = LowReynoldsSimulation(structure);
    %  
    %  self.flow = backgroundFlow;
    %
    %end % end LowReynoldsSimulation(structure, backgroundFlow)
  
    % simulation functions

    % try to use matrix operations as these run efficiently in matlab

%     function [] = calcVelocity(self)
%       
%       u_s = -self.U_interp(self.flow(:, 1), self.flow(:, 2));
%       v_s = -self.V_interp(self.flow(:, 1), self.flow(:, 2));
%       w_s = -self.W_interp(self.flow(:, 1), self.flow(:, 2));
%       
%       %self.l_dot = [u_s' v_s' w_s'];
%       
%     end % end calcVelocity
    
    %function [] = calcPosition(self)
      
    %end % end calcPosition
    
%     function [] = runSimulation(self)
% 
%       % check variables before simulation
%               
%       % check body
%              
%       % check time
%           
%       % instanciate body
%       
%       % instanciate flow
%       
%       % setup temporary variables
%       S_diag = 1/(4*pi*a_*e_);
%       
%       % simulation
%         
%       for i = 1:length(t_)
%         
%         % get new positions
%         r_s = calcPosition();
%                   
%         % interpolate velocities
%         v_s = calcVelocity();
%         
%         % get stokeslet velocity, L_dot
%         [pos(i), vel(i)] = iterate(r_s, v_s)
%         
%         % store body velocities and positions
%       end % end for loop
%     end % end runSimulation

%     function [] = displayFlow(time)
%       
%       % get flow field
%       
%       % positions during time frame
%       
%       % plot flow and positions
%       
%     end

%     function [] = getVelocity()
%       
%     end % end getVelocity

%     function [] = getPosition()
%       
%     end % end getPosition
    
%     function [P_Lab, V_Lab] = addBackgroundFlow(backgroundFlow)
%     end % end addBackgroundFlow
  
  end % methods - nonstatic
  
  methods(Static)
    function [V_Lab, V_G, V_LJ, M, mem, numS] = iterate(r_, r_o, L_dot, a_, e_, mu, sig_LJ, e_LJ)
      %numS= number of stokeslets, mem= momory, M=  
      [V_Lab, V_G, V_LJ, M, mem, numS] = iterate_multi(r_, r_o, L_dot, a_, e_, mu, sig_LJ, e_LJ);
      
    end % end iterate
	
	function [V_Lab, V_G, V_LJ, M, mem, numS, V_P] = iterate_multi_Pressure(r_, r_o, L_dot, r_n, a_, e_, mu, sig_LJ, e_LJ)
      
       V_Lab = zeros(6,1);
       V_G   = zeros(6,1);
       V_LJ  = zeros(6,1);
       M     = zeros(6,6);
       V_P   = zeros(6,1); 
        
	  % V_Lab the velocity of the 
      import SimulationClass.*
        
      if length(r_) ~= length(r_o) % r_o, body centers. % r_, simulation bodies
        disp('Number of body centers and simulation bodies are not equal')
        return
      end

      % get G_Matrix
      [Ginv] = LowReynoldsSimulation.G_Matrix_sim_multi_S(r_, a_, e_, mu);% single particle
      
      % get IRX
      IRX = LowReynoldsSimulation.IRX_Matrix_sim_multi(r_, r_o);
      
      % get LJ Forces
      LJ = LowReynoldsSimulation.LennardJoneForce_sim(r_, sig_LJ, e_LJ);
      
      P_mat = LowReynoldsSimulation.PressureForceStokeslets(r_, r_o, e_);
      
      % Calculate resistance Matrix
      M = IRX' * Ginv * IRX;
      
	  % V =   R^-1 * (force/torques mapping) * [Forces]  
	  
      V_LJ =  -M^-1 * (IRX)' * LJ;	  
      V_G  =  -M^-1 * (IRX)' * Ginv * L_dot;
	  
	  % Prssure Force
      
      % V_P = -IRX' * Ginv * (r_n .* (P_mat * Ginv * L_dot));
      
      % Calculate Body velocity
      V_Body = V_G + V_LJ + V_P;
      
      % convert to labFrame
      V_Lab = V_Body;
      
      user = memory;
      mem = user.MemUsedMATLAB;
      numS = length(Ginv)/3;
      

    end % end iterate_multi
    
%     function [V_Lab, V_G, V_LJ, M, mem, numS, V_P, P_mat] = iterate_multi_PressureEx(r_, r_o, L_dot, r_n, P, a_, e_, mu, sig_LJ, e_LJ)
%         
%         V_Lab = zeros(6,1);
%         V_G   = zeros(6,1);
%         V_LJ  = zeros(6,1);
%         M     = zeros(6,6);
%         V_P   = zeros(6,1); 
%         
%         
%         % V_Lab the velocity of the 
%         import SimulationClass.*
% 
%         %if length(r_) ~= length(r_o) % r_o, body centers. % r_, simulation bodies
%         %    disp('Number of body centers and simulation bodies are not equal')
%         %    return
%         %end
% 
%         % get G_Matrix
%         [Ginv] = LowReynoldsSimulation.G_Matrix_sim_multi_S(r_, a_, e_, mu);% single particle
% 
%         % get IRX
%         IRX = LowReynoldsSimulation.IRX_Matrix_sim_multi(r_, r_o);
% 
%         % get LJ Forces
%         LJ = LowReynoldsSimulation.LennardJoneForce_sim(r_, sig_LJ, e_LJ);
% 
%         P_mat = PressureForceStokeslets(r_, r_o, e_);
% 
%         % Calculate resistance Matrix
%         M = IRX' * Ginv * IRX;
% 
%         % V =   R^-1 * (force/torques mapping) * [Forces]  
%         V_LJ =  -M^-1 * (IRX)' * LJ;	  
%         V_G  =  -M^-1 * (IRX)' * Ginv * L_dot;
% 
%         % Prssure Force
%         V_P = -M^-1 * IRX' * ((P_mat) .* (P));%we do not know if we gor it correctly??
% 
%         % Calculate Body velocity
%         V_Body = V_G + V_LJ + V_P;
% 
%         % convert to labFrame
%         V_Lab = V_Body;
% 
%         user = memory;
%         mem = user.MemUsedMATLAB;
%         numS = length(Ginv)/3;
%         
%         %Assumed Pressure Force (r_n .* repelem(P, 3, 1))
%         %V_Pex = -M^-1 * (IRX)' * Ginv * (r_n .* repelem(P, 3, 1));
%         
%         %V_Lab = V_Lab + V_Pex;
%         V_Lab = V_Lab;
%     end % end iterate function
    
    function [V_Lab, V_G, V_LJ, M, mem, numS] = iterate_multi(r_, r_o, L_dot, a_, e_, mu, sig_LJ, e_LJ)
      
      import SimulationClass.*
        
      if length(r_) ~= length(r_o) % r_o, body centers. % r_, simulation bodies
        disp('Number of body centers and simulation bodies are not equal')
        return
      end

      % get G_Matrix
      [Ginv] = LowReynoldsSimulation.G_Matrix_sim_multi_S(r_, a_, e_, mu);% single particle
      
      % get IRX
      IRX = LowReynoldsSimulation.IRX_Matrix_sim_multi(r_, r_o);
      
      % get LJ Forces
      LJ = LowReynoldsSimulation.LennardJoneForce_sim(r_, sig_LJ, e_LJ);
      
      % Calculate resistance Matrix
      M = IRX' * Ginv * IRX;
      
	  % V =   R^-1 * (force/torques mapping) * [Forces]  
	  
      V_LJ  =  -M^-1 * (IRX)'* LJ;	  
      V_G =  -M^-1 * (IRX)' * Ginv * L_dot;
	  
	  % Prssure Force
	  % V_P = -M^-1 * (IRX)' * (P * n);
      
      % Calculate Body velocity
      V_Body = V_G + V_LJ;
      
      % convert to labFrame
      V_Lab = V_Body;
      
      user = memory;
      mem = user.MemUsedMATLAB;
      numS = length(Ginv)/3;
      

    end % end iterate_multi
    
    function [V_Lab, V_G, V_LJ, M, mem, numS] = iterate_GPU(r_, r_o, L_dot, a_, e_, mu, sig_LJ, e_LJ)
      import SimulationClass.*
        
      if length(r_) ~= length(r_o)
        disp('Number of body centers and simulation bodies are not equal')
        return
      end
      
      %V_LJ = [0;0;0;0;0;0];

      % get G_Matrix
      [Ginv] = LowReynoldsSimulation.G_Matrix_sim_GPU(r_, a_, e_, mu);% multiple particle
      
      % get IRX
      IRX  = LowReynoldsSimulation.IRX_Matrix_sim_GPU(r_, r_o);
      
      % get LJ Forces
      LJ = LowReynoldsSimulation.LennardJoneForce_sim_GPU(r_, sig_LJ, e_LJ);
      
      % Calculate resistance Matrix
      M = IRX' * Ginv * IRX;
               
      V_LJ  =  -M^-1 * (IRX)'* LJ;
      V_G =  -M^-1 * (IRX)' * Ginv * L_dot;
      
      % Calculate Body velocity
      V_Body = V_G + V_LJ;
      
      % convert to labFrame
      V_Lab = V_Body;
      
      user = memory;
      mem = user.MemUsedMATLAB;
      numS = length(Ginv)/3;
    end
    
%% function under development
%     function iterate_Ex(r_s, r_o, L_dot, a_, e_, mu, sig_LJ, e_LJ)
%         for i = 1:length(r_s)
%             for j = 1:length(r_s)
%                 parfor alpha = 1:length(r_s{i})
%                     SinvSum = zeros(1,6);
%                     SinvVSum = zeros(1,6);
%                     for beta = 1:length(r_s{j})
%                         % regularized stokeslet
%                         r_d = r_s{i}(alpha, :) - r_s{j}(beta, :);
%                         
%                         r = (r_d * r_d')^(1/2);
%                         r_ = r_d;
%                         r0 = r_s{i}(alpha, :) - r_o;
%                         
%                         Sinv = (1/(8 * pi * mu) * (delta *(r^2 + 2*e_{i}^2)/(r^2 + e_{i}^2)^(3/2) + (r_' * r_)/(r^2 + e_{i}^2)^(3/2)))^-1;
%                         
%                         vel = L_dot(alpha);
%                         
%                         SinvVSum(1) = SinvVSum(1) + Sinv(1,1) * vel(1) 
%                         SinvSum(1) = SinvSum(1) + Sinv(1,1)
%                         SinvVSum(2) = SinvVSum(2) + Sinv(2,2) * vel(2)
%                         SinvSum(2) = SinvSum(2) + Sinv(2,2)
%                         SinvVSum(3) = SinvVSum(3) + Sinv(3,3) * vel(3)
%                         SinvSum(3) = SinvSum(3) + Sinv(3,3)
%                         SinvSum(4) = SinvSum(4) + (Sinv(2,2) * r_d(3) +Sinv(2,3)*r_d(2))*r_d(3) - (-Sinv(3,2)*r_d(3) + Sinv(3,3)*r_d(2))*r_d(2)
%                         SinvVSum(4) = (vel(2)*ro(3) - vel(3)*ro(2))
%                         SinvSum(5) = SinvSum(5) - (-Sinv(1,1) * r_d(3) +Sinv(1,3)*r_d(1))*r_d(3) + (-Sinv(3,1)*r_d(3) + Sinv(3,3)*r_d(1))*r_d(1)  
%                         SinvVSum(5) = (vel(1)*ro(3) - vel(3)*ro(1))
%                         SinvSum(6) = SinvSum(6) + (Sinv(2,1) * r_d(2) +Sinv(2,2)*r_d(1))*r_d(2) - (-Sinv(3,1)*r_d(2) + Sinv(3,2)*r_d(1))*r_d(1)
%                         SinvVSum(6) = (vel(1)*ro(2) - vel(2)*ro(1))
%                     end % end beta
%                 end % end alpha
%             end % end j
%         end % end j
%     end % end iterate_Ex

%% Static functions continued
    
    function [G] = G_Matrix_sim(r_s, a_, e_, mu)
      % Currently more efficient than the stokeslet matrix technique     
      G = G_Matrix_sim_multi({r_s}, {a_}, {e_}, mu);
      
    end % end G_Matrix
    
    function [Ginv] = G_Matrix_sim_multi(r_s, a_, e_, mu)
      % Currently more efficient than the stokeslet matrix technique
      
      l = 0; 
      
      % calculate length of cell
      for i = 1:length(r_s)
        l = length(r_s{i}) + l;
      end      
      
      indA = 0;
      indB = 0;
      G_cell = cell(l);
      
      
      delta = eye(3);
      for i = 1:length(r_s) % iterate over bodies
        S_diag = 1/(6*pi*a_{i}*mu);
        for j = 1:length(r_s)
          for alpha = 1:length(r_s{i})     % Iterate over spheres
            for beta = 1:length(r_s{j})
              if alpha == beta && i == j                    %This would mean the same sphere
                  velocity = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
              else                              %Hydrodynamic Forces
                  position = r_s{i}(alpha, :) - r_s{j}(beta, :);
              
                  % inlined stokeslet_e reduces function calls
                  % TODO verify working code
                  
                  r = (position * position')^(1/2);
                  r_ = position;
                  
                  velocity = 1/(8 * pi * mu) * (delta *(r^2 + 2*e_{i}^2)/(r^2 + e_{i}^2)^(3/2) + (r_' * r_)/(r^2 + e_{i}^2)^(3/2));       
              end
                            
              G_cell{indB + alpha,indA + beta} = velocity;    % Append to G matrix
            end
          end
          indA = indA + length(r_s{j});
        end
        indB = indB + length(r_s{i});
      end  
          
      Ginv = cell2mat(G_cell)^-1;
      
      
      
    end % end G_Matrix_sim_multi
    
    
    % Currently the largest simulation is 1k stokeslets
    % techniques breaking up a geometry may give a way to overcome this
    % limitation as the code breaks running out of memory on the GPU.
    
%     function [Ginv] = G_Matrix_sim_GPU(r_s, a_, e_, mu)
%       % Currently more efficient than the stokeslet matrix technique
%       
%       l = 0;
%       
%       % calculate length of cell
%       for i = 1:length(r_s)
%         l = length(r_s{i}) + l;
%       end      
% 
%       B_cell = cell(length(r_s));
% 
%         for i = 1:length(r_s) % iterate over bodies
%             S_diag = 1/(6*pi*a_{i}*mu);
%             for j = i:length(r_s)
%                 %G_cell = cell(length(r_s{i}), length(r_s{j}));
%                 %G_arr = gpuArray(zeros(length(r_s{i})*3, length(r_s{j})*3));
%                 %if (length(r_s{i}) * length(r_s{j}) > 625000000)
%                 % setup calculations
% 				G_arr = zeros(length(r_s{i}) * 3, length(r_s{j}) * 3);
% 				
% 				% move the stokeslet bodies onto the GPU				
% 				
% 				R1 = gpuArray(r_s{i});
% 				R2 = gpuArray(r_s{j});
% 				for alpha = 1:length(r_s{i})     % Iterate over spheres
% 					prevBeta = 1;
% 					for beta = 1:2000:length(r_s{j})
% 					
% 						if beta > length(r_s{j})
% 							%beta = length(r_s{j});
% 						end
% 					
% 						if i ~= j
% 							% calculate the differences between stokeslets
% 							R3  = repmat(R1(alpha, prevBeta:beta), length(R2), 1)  - R2;%repmat Creates a length(R2)-by-1 matrix whose elements contain the value R1(alpha, :)
% 						else % i == j
% 							R3  = repmat(R1(alpha, prevBeta:beta), length(R2)-alpha, 1)  - R2(alpha+1:end, prevBeta:beta);
% 						end
% 					
% 						% get the norm of each difference vector and create diagonal matrices
% 						R3S = repelem(sqrt(sum(R3.^2, 2)), 3, 3); %(R3 * R3').^(1/2);
% 					  
% 						deltaGPU = repmat(gpuArray.eye(3), length(R3(:,1)), 1);
% 						
% 						% calculation done in regularized stokeslet
% 						R3A = reshape(repelem(R3, 1, 3)', 3, [])' .* repelem(R3, 3, 1);% refer to definition of B = reshape(A,sz1,...,szN)....
% 						
% 						% epsilon value matrix
% 						e_G = gpuArray.ones(length(R3(:,1))*3, 3) * e_{i};
% 						
% 						% regularized stokeslet
% 						G_GPU =  1/(8 * pi * mu) * (deltaGPU .* (R3S.^2 + 2*e_G.^2)./(R3S.^2 + e_G.^2).^(3/2) + (R3A)./(R3S.^2 + e_G.^2).^(3/2)); 
% 						
% 						% copy array to full system matrix
% 						G_arr(alpha*3-2:alpha*3, prevBeta:beta) = gather(G_GPU)';
% 						
% 						prevBeta = beta + 1;
% 					end % end for beta
% 				end % end for alpha
% 
% 				if i ~= j
% 					B_cell{i, j} = G_arr;
% 				else % i == j
% 					G_arr = G_arr + S_diag.*eye(length(G_arr)) + G_arr';
% 					B_cell{i, j} = G_arr;
% 				end % end if
%             end % end for j
%         end % end for i
%         
%         for i = 1:length(r_s)
%             for j = 1:i
%                 if i ~= j
%                     B_cell{i, j} = B_cell{j, i}';
%                 end
%             end
%         end
%         
% % %         if length(G)<3000 
%             
%            G = gpuArray(cell2mat(B_cell));
% 
%            Ginv = gather(G^-1);
% %         else 
% %            Ginv = cell2mat(B_cell)^-1;
% %         
% %         end
% 
%         %Ginv
%         
%     end % end G_Matrix_sim_GPU
    
    function [Ginv] = G_Matrix_sim_GPU_Pool(r_s, a_, e_, mu)
      % Currently more efficient than the stokeslet matrix technique
      
      
      
      l = 0;
      
      % calculate length of cell
      for i = 1:length(r_s)
        l = length(r_s{i}) + l;
      end      

      B_cell = cell(length(r_s));

        for i = 1:length(r_s) % iterate over bodies
            S_diag = 1/(6*pi*a_{i}*mu);
            for j = i:length(r_s)
                %G_cell = cell(length(r_s{i}), length(r_s{j}));
                %G_arr = gpuArray(zeros(length(r_s{i})*3, length(r_s{j})*3));
                %if (length(r_s{i}) * length(r_s{j}) > 625000000)
                if i ~= j

                    G_cell= cell(1, length(r_s{i}));
                    R1 = gpuArray(r_s{i});
                    R2 = gpuArray(r_s{j});
                    parfor alpha = 1:length(r_s{i})-1     % Iterate over spheres
                        
                        R3  = repmat(R1(alpha, :), length(R2)-alpha, 1)  - R2(alpha+1:end, :);
                        
                        R3S = repelem(sqrt(sum(R3.^2, 2)), 3, 3);%R3 * R3').^(1/2);
                        
                        deltaGPU = repmat(gpuArray.eye(3), length(R3(:,1)), 1);
                      
                        R3A = reshape(repelem(R3, 1, 3)', 3, [])' .* repelem(R3, 3, 1);
                        
                        e_G = gpuArray.ones(length(R3(:,1))*3, 3) * e_{i};
                        
                        G_GPU =  1/(8 * pi * mu) * (deltaGPU .* (R3S.^2 + 2*e_G.^2)./(R3S.^2 + e_G.^2).^(3/2) + (R3A)./(R3S.^2 + e_G.^2).^(3/2)); 

                        G_cell{alpha} = gather(G_GPU)

                        
                    end % end for alpha

                    G_arr = cell2mat(G_cell)';
                    B_cell{i, j} = G_arr;
                    
                    clear G_arr

                elseif i == j

                    G_cell= cell(1, length(r_s{i}));
                    R1 = gpuArray(r_s{i});
                    R2 = gpuArray(r_s{j});
                    
                    parfor alpha = 1:length(r_s{i})     % Iterate over spheres
                        
                        
                        %R3p  = repmat(R1(alpha, :), length(R2p), 1)  - R2p;
                        %R3Sp = repelem(sqrt(sum(R3p.^2, 2)), 3, 3).* repmat(eye(3), length(R2p), 1);%eye(length(R2p)) .* (R3p * R3p').^(1/2);
                        
                        %R3  = repmat(R1(alpha, :), length(R2), 1)  - R2;
                        R3  = repmat(R1(alpha, :), 1+length(R2)-alpha, 1)  - R2(alpha:end, :);
                        
                        R3S = repelem(sqrt(sum(R3.^2, 2)), 3, 3);%R3 * R3').^(1/2);
                      
                        deltaGPU = repmat(gpuArray.eye(3), length(R3(:,1)), 1);
                        
                        R3A = reshape(repelem(R3, 1, 3)', 3, [])' .* repelem(R3, 3, 1);
                        
                        e_G = gpuArray.ones(length(R3(:,1))*3, 3) * e_{i};
                        
                        G_GPU = gpuArray.zeros(length(R2)*3, 3);
                        
                        G_GPU(alpha*3-2:end, :) =  1/(8 * pi * mu) * (deltaGPU .* (R3S.^2 + 2*e_G.^2)./(R3S.^2 + e_G.^2).^(3/2) + (R3A)./(R3S.^2 + e_G.^2).^(3/2)); 

                        G_cell{alpha} = gather(G_GPU)
                        
                    end % end for alpha

                    G_arr = cell2mat(G_cell);
                    %G_arr = gather(G_GPU);
                    G_arr = G_arr + S_diag.*eye(length(G_arr)) + G_arr';
                    %G_arr = G_arr + G_arr';
                    B_cell{i, j} = G_arr;
                    
                    clear G_arr
                end % end if
            end
        end
        
        for i = 1:length(r_s)
            for j = 1:i
                if i ~= j
                    B_cell{i, j} = B_cell{j, i}';
                end
            end
        end
        
        G = gpuArray(cell2mat(B_cell));

        Ginv = gather(G^-1);
    end % end G_Matrix_sim_GPU_Pool
    
    function [vel] = stokeslet(r_, r_n, e)
        
        vel = (eye(3) *(r_^2 + 2*e^2)/(r_^2 + e^2)^(3/2) + (r_n' * r_n)/(r_^2 + e^2)^(3/2));
        
    end
    
%     function [Ginv] = G_Matrix_sim_multi_S(r_s, a_, e_, mu)
%       % Currently more efficient than the stokeslet matrix technique
%       
%       l = 0;
%       
%       % calculate length of cell
%       for i = 1:length(r_s)
%         l = length(r_s{i}) + l;
%       end      
% 
%       B_cell = cell(length(r_s));
%       
%       delta = eye(3);
%       velocity = zeros(3);
%       for i = 1:length(r_s) % iterate over bodies
%         
%         for j = i:length(r_s)
%             S_diag = 1/(6*pi*a_{i}*mu);
%             G_cell = cell(length(r_s{i}), length(r_s{j}));
%             if i ~= j
%               for alpha = 1:length(r_s{i})     % Iterate over spheres
%                 for beta = 1:length(r_s{j})
%                                                 %Hydrodynamic Forces
%                   position = r_s{i}(alpha, :) - r_s{j}(beta, :);
% 
%                   % inlined stokeslet_e reduces function calls
% 
%                   r = (position * position')^(1/2);
%                   r_ = position;
% 
%                   velocity = 1/(8 * pi * mu) * (delta *(r^2 + 2*e_{i}^2)/(r^2 + e_{i}^2)^(3/2) + (r_' * r_)/(r^2 + e_{i}^2)^(3/2));       
% 
%                   G_cell{alpha,beta} = velocity;    % Append to G matrix
%                   
%                 end % end for beta
%               end % end for alpha
%               
%               G_arr = cell2mat(G_cell);
%               B_cell{i, j} = G_arr;
%               B_cell{j, i} = G_arr';
%           
%             elseif i == j
%                 for alpha = 1:length(r_s{i})     % Iterate over spheres
%                     for beta = 1:length(r_s{j})
%                       if alpha == beta && i == j                    %This would mean the same sphere
%                           %velocity = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
%                             % do nothing
%                       elseif beta > alpha   %Hydrodynamic Forces
%                           position = r_s{i}(alpha, :) - r_s{j}(beta, :);
% 
%                           % inlined stokeslet_e reduces function calls
% 
%                           r = (position * position')^(1/2);
%                           r_ = position;
% 
%                           velocity = 1/(8 * pi * mu) * (delta *(r^2 + 2*e_{i}^2)/(r^2 + e_{i}^2)^(3/2) + (r_' * r_)/(r^2 + e_{i}^2)^(3/2));       
%                       else
%                         velocity = [0 0 0; 0 0 0; 0 0 0];
%                       end % end if
% 
%                       G_cell{alpha,beta} = velocity;    % Append to G matrix
% 
%                     end % end for beta
% 
%                 end % end for alpha
%               
%               G_arr = cell2mat(G_cell);
%               G_arr = G_arr + S_diag.*eye(length(G_arr)) + G_arr';
%               B_cell{i, j} = G_arr;
%             end % end if
%             
%         end
%       end  
%           
%       Ginv = cell2mat(B_cell)^-1;
%       
%       
%       
%     end % end G_Matrix_sim_multi

%     function [Ginv] = G_Matrix_sim_multi_Sarr(r_s, a_, e_, mu)
%       % Currently more efficient than the stokeslet matrix technique
%       
%       l = 0;
%       
%       % calculate length of cell
%       for i = 1:length(r_s)
%         l = length(r_s{i}) + l;
%       end      
% 
%       B_cell = cell(length(r_s));
%       
%       delta = eye(3);
%       velocity = zeros(3);
%       for i = 1:length(r_s) % iterate over bodies
%         for j = i:length(r_s)
%             S_diag = 1/(6*pi*a_{i}*mu);
%             %G_cell = cell(length(r_s{i}), length(r_s{j}));
%             G_arr = zeros(length(r_s{i})*3, length(r_s{j})*3);
%             if i ~= j
%               for alpha = 1:length(r_s{i})     % Iterate over spheres
%                 for beta = 1:length(r_s{j})
%                   if alpha == beta && i == j                    %This would mean the same sphere
%                       %velocity = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
%                         % do nothing
%                   else                              %Hydrodynamic Forces
%                       position = r_s{i}(alpha, :) - r_s{j}(beta, :);
% 
%                       % inlined stokeslet_e reduces function calls
% 
%                       r = (position * position')^(1/2);
%                       r_ = position;
% 
%                       velocity = 1/(8 * pi * mu) * (delta *(r^2 + 2*e_{i}^2)/(r^2 + e_{i}^2)^(3/2) + (r_' * r_)/(r^2 + e_{i}^2)^(3/2));       
%                   end % end if
% 
%                   G_arr(alpha*3-2:alpha*3,beta*3-2:beta*3)  = velocity;    % Append to G matrix
%                   
%                 end % end for beta
% 
% 
% 
%               end % end for alpha
%               
%               %G_arr = cell2mat(G_cell);
%               B_cell{i, j} = G_arr;
%               B_cell{j, i} = G_arr';
%           
%             elseif i == j
%                 for alpha = 1:length(r_s{i})     % Iterate over spheres
%                     for beta = 1:length(r_s{j})
%                       if alpha == beta && i == j                    %This would mean the same sphere
%                           %velocity = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
%                             % do nothing
%                       elseif beta > alpha   %Hydrodynamic Forces
%                           position = r_s{i}(alpha, :) - r_s{j}(beta, :);
% 
%                           % inlined stokeslet_e reduces function calls
% 
%                           r = (position * position')^(1/2);
%                           r_ = position;
% 
%                           G_arr(alpha*3-2:alpha*3,beta*3-2:beta*3) = 1/(8 * pi * mu) * (delta *(r^2 + 2*e_{i}^2)/(r^2 + e_{i}^2)^(3/2) + (r_' * r_)/(r^2 + e_{i}^2)^(3/2));       
%                       else
%                         G_arr(alpha*3-2:alpha*3,beta*3-2:beta*3) = [0 0 0; 0 0 0; 0 0 0];
%                       end % end if
% 
%                       %G_arr(alpha*3-2:alpha*3,beta*3-2:beta*3) = velocity;    % Append to G matrix
% 
%                     end % end for beta
% 
%                 end % end for alpha
%               
%               %G_arr = cell2mat(G_cell);
%               G_arr = G_arr + S_diag.*eye(length(G_arr)) + G_arr';
%               B_cell{i, j} = G_arr;
%             end % end if
%             
%         end
%       end  
%           
%       Ginv = cell2mat(B_cell)^-1;
%       
%       
%       
%     end % end G_Matrix_sim_multi
    
%     function [G] = G_Matrix_Ex(r_s, r_w, a_, e_)
% 
%       r_comb = [r_s; r_w];
%       
%       mat1 = repmat(r_comb, 1, length(r_comb));
% 
%       % calculate stokeslet differences
%       matdiff = cell2mat(mat1) - cell2mat(mat1');
%       mat1diff = mat2cell(matdiff, ones(1, length(mat1)), ones(1, length(mat1))*3);
%       mat2diff = mat2cell(matdiff', ones(1, length(mat1))*3, ones(1, length(mat1)));
%       
%       % TODO possible use an anonmous function 
%       % efficiently.
%       dotM = @(a, b) a * b;
%       
%       %r_ = cellfun(@dotM, mat2diff, mat1diff, 'UniformOutput', false);
%       
%       %ones on diagonal term,first row, then column
%       delta = repmat(eye(3), length(mat1), length(mat1));
% 
%       % creates an array of the norm of the differences of the stokeslet positions
%       rNorm = cellfun(@norm, mat1diff);
%       r = repelem(rNorm, 3, 3);
%       
%       G = a_/(8 * 3.141 * mu) * (delta .*(r.^2 + 2*e_^2)./(r.^2 + e_^2) + (cell2mat(r_))./(r.^2 + e_^2).^(3/2));
% 
%     end % end G_Matrix_Ex

    function [IRX] = IRX_Matrix_sim(r_, r_o)
            
      IRX = IRX_Matrix_sim_multi(r_, r_o);
    end % end IRX_Matrix
    
%     function [IRX] = IRX_Matrix_sim_multi(r_, r_o)
%       
%       indB = 0;
%       l = 0;
%       
%       for i = 1:length(r_)
%         l = length(r_{i}) + l;
%       end 
%       
%       IRX_cell = cell(l, 1);
%       
%       for i = 1:length(r_)
%         for cll = 1:1:length(r_{i})
%             IRX_cell{indB + cll,1} = [1 0 0 0                     (r_{i}(cll, 3)-r_o{i}(3))     -(r_{i}(cll, 2)-r_o{i}(2)); 
%                                       0 1 0 -(r_{i}(cll, 3)-r_o{i}(3)) 0                        (r_{i}(cll, 1)-r_o{i}(1)); 
%                                       0 0 1 (r_{i}(cll, 2)-r_o{i}(2))  -(r_{i}(cll, 1)-r_o{i}(1))    0];
%         end
%         
%         indB = indB + length(r_{i});
%       end
%           
%       IRX = cell2mat(IRX_cell);
%     end % end IRX_Matrix
    
    function [IRX] = IRX_Matrix_sim_GPU(r_, r_o)

      l = 0;
      
      for i = 1:length(r_)
        l = length(r_{i}) + l;
      end 
      
      IRX_cell = cell(length(r_), 1);
      
      for i = 1:length(r_)
          
          skewMatrix = repelem(r_{i}(:,3)-r_o{i}(3), 3, 3) .* repmat( [0 1 0; -1 0 0; 0 0 0], length(r_{i}), 1) + ...
            repelem(r_{i}(:,2) - r_o{i}(2), 3, 3) .* repmat( [0 0 -1; 0 0 0; 1 0 0], length(r_{i}), 1) + ...
            repelem(r_{i}(:,1) - r_o{i}(1), 3, 3) .* repmat( [0 0 0; 0 0 1; 0 -1 0], length(r_{i}), 1);
          
          IRX_cell{i} = [repmat(eye(3), length(r_{i}), 1), skewMatrix];

      end
          
      IRX = cell2mat(IRX_cell);
    end % end IRX_Matrix_sim_GPU
    
    function [IRX] = IRX_Matrix_sim_multi_s(r_, r_o)
      
      indB = 0;
      l = 0;
      
      for i = 1:length(r_)
        l = length(r_{i}) + l;
      end 
      
      IRX_cell = cell( l, length(r_));
      
      for i = 1:length(r_)   
        for cll = 1:1:length(r_{i})
            IRX_cell{indB + cll,i} = [1 0 0 0                     (r_{i}{cll}(3)-r_o{i}(3))     -(r_{i}{cll}(2)-r_o{i}(2)); 
                               0 1 0 -(r_{i}{cll}(3)-r_o{i}(3)) 0                        (r_{i}{cll}(1)-r_o{i}(1)); 
                               0 0 1 (r_{i}{cll}(2)-r_o{i}(2))  -(r_{i}{cll}(1)-r_o{i}(1))    0];
        end
        
        indB = indB + length(r_{i});
      end
          
      IRX = cell2mat(IRX_cell);
    end % end IRX_Matrix
		
% 	function [Pv] = PressureForceStokeslets(Body, P_body, ep)
% 	
%         l = 0;
%         
%         for i = 1:length(Body)
%             l = l + length(Body{i});
%         end
%         
%         Pv = zeros(3*l,1);
%         
%         l_m = 0;
%         
%         for i = 1:length(Body)
%             %l_j = 0;
%             %for j = 1:length(Body)
%                 for alpha = 1:length(Body{i})
%                     %for beta = 1:length(Body{j})
% 
%                         % distance between stokeslets
%                         r_o = P_body{i};
%                         r = Body{i}(alpha, :) - r_o(1:3);%normal vector around center
%                         r = r/norm(r);
%                         rm = sqrt(sum(r.^2));
% 
%                         % regularized Pressure
%                         %Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m);
%                         %Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m) = (r .* (2*rm^2 + 5*ep{i}^2)/(rm^2 + ep{i}^2)^(5/2));
%                         Pv(alpha*3+l_m-2:alpha*3+l_m) = (r .* (2*rm^2 + 5*ep{i}^2)/(rm^2 + ep{i}^2)^(5/2)).^-1;
%                         %Pv(beta*3+l_j-1,alpha*3+l_m-2:alpha*3+l_m) = Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m);
%                         %Pv(beta*3+l_j-0,alpha*3+l_m-2:alpha*3+l_m) = Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m);
%                     %end % end for beta
%                     % sum pressure at stokeslet
% 
%                     % store in matrix
%                 end	% end for alpha		
%                 %l_j = l_j + length(Body{j});
%             %end % end for j
% 
%             l_m = l_m + length(Body{i});
%         end % end for i
% 	
%     end % end Pressure Function

%     function [F] = LennardJoneForce_sim(Body, F_0, sig)
%       %initialize cell array
%       %for i = 1:length(r_s)
%       %  l = length(r_s{i}) + l;
%       %end 
%       
%       F = cell(length(Body));
% 
%       for i = 1:length(Body)
%         for j = 1:length(Body)
%           if (i ~= j)
%             F_cell = cell(length(Body{i}), length(Body{j}));
%               for alpha = 1:length(Body{i})     % Iterate over spheres
%                   
%                   
%                   for beta = 1:1:length(Body{j})
%                       %we never compare any point with itself
%             %             if alpha == beta                         %This would mean the same sphere
%             %                 diag_cell = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
%             %             else                              %Hydrodynamic Forces
% 
%                       %sig % is the characteristic diameter of the molecule
%                       %e is the characteristic energy, which is the maximum energy of attraction between the molecules
%                       rij = Body{i}(alpha, :) - Body{j}(beta, :);
%                       rm = (rij * rij')^(1/2);%Checked line 4 of stokeslet_e.m file
%                       %diag_cell = -48 * e / (r)^2 * ((sig/r)^12-1/2*(sig/r)^6).* [1 1 1]' * rij;
%                       diag_cell = F_0 * sig / 6 * ((sig/rm)^12-(sig/rm)^6).* [1 1 1]' * rij/norm(rij);
%                       %end
%                       F_cell{alpha,beta} = diag_cell; % Append to G matrix
%                   end
%               end
%             
%               F{i, j} = cell2mat(F_cell);
%             
%           else
%               X = zeros(length(Body{i})*3);
% 
%               F{i, j} = X;  
%           end
%         end   
%       end
%       
%       F = cell2mat(F);
%       
%       I  = ones(length(F),1);%size(I):Used identity matrix to make size(F)=4191*4191 to size(I)=4191*1
%       F = F*I;
%     end % end LennardJoneForce
    
%     function [F] = LennardJoneForce_sim_GPU(Body, F_0, sig)
%       %initialize cell array
%       %for i = 1:length(r_s)
%       %  l = length(r_s{i}) + l;
%       %end 
%       F = cell(length(Body));
% 
%       for i = 1:length(Body)
%         for j = 1:length(Body)
%           if (i ~= j)
%             F_arr = zeros(3*length(Body{i}), 3*length(Body{j}));
%             R1 = gpuArray(Body{i});
%             R2 = gpuArray(Body{j});
%             for alpha = 1:length(Body{i})     % Iterate over spheres
%                 
%                 
%                 % calculate the differences between stokeslets
%                 R3  = repmat(R1(alpha, :), length(R2), 1)  - R2;
% 
%                 % get the norm of each difference vector and create
%                 % diagonal matrices
%                 R3S = repelem(sqrt(sum(R3.^2, 2)), 3, 3);%R3 * R3').^(1/2);
% 
%                 % diag_cell = F_0 * sig / 6 * ((sig/rm)^12-(sig/rm)^6).* [1 1 1]' * rij/norm(rij);
%                 F_GPU = F_0 * sig ./ 6 .* ((sig./R3S).^12-(sig./R3S).^6).* (reshape(repelem(R3, 1, 3)', 3, [])' ./ R3S);
% 
%                 % copy array to full system matrix
%                 F_arr(alpha*3-2:alpha*3, :) = gather(F_GPU)';
% 
%             end
%             
%             F{i, j} = F_arr;
%             
%           else
%             X = zeros(length(Body{i})*3);
%         
%             F{i, j} = X;  
%           end
%         end   
%       end
%       
%       F = cell2mat(F);
%       
%       I  = ones(length(F),1);%size(I):Used identity matrix to make size(F)=4191*4191 to size(I)=4191*1
%       F = F*I;
%     end % end LennardJoneForce

    function [r_s_mat] = rotation_Body(r_s_mat, P_Lab)
          
      phi = P_Lab(4);
      theta = P_Lab(5);
      gama = P_Lab(6);
      
      Rot = [cos(theta)*cos(gama)+sin(theta)*sin(phi)*sin(gama) -sin(gama)*cos(theta)+cos(gama)*sin(theta)*sin(phi) sin(theta)*cos(phi);
          cos(phi)*sin(gama) cos(phi)*cos(gama) -sin(phi);
          -sin(theta)*cos(gama)+cos(theta)*sin(gama)*sin(phi) sin(theta)*sin(gama)+cos(theta)*cos(gama)*sin(phi) cos(phi)*cos(theta)];
      
      P = repmat([P_Lab(1), P_Lab(2), P_Lab(3)]', 1, length(r_s_mat(:,1)));
      
      r_s_mat = (Rot*(r_s_mat-P')' + P)';
    end % end rotation_Body
    
    function [E, C, Rsq] = convergenceFunction(geoFunct, dS, gam, cVar, fDir, name, fFormat)
%          try
            % This function performs a convergence study on a geometry and output
            % variable. To perform the convergence study a variable must be passed
            % in the form geoFunct(dS). The easiest way to do this to wrap the
            % function in another function with the function signature required.

            %setup
            E = zeros(6, length(gam));
            C = zeros(6, length(gam));
            Rsq = zeros(6, length(gam));
            conv = zeros(length(dS), length(gam));

            date = strrep(strrep(datestr(datetime()), ':', ''), ' ', '_');
            workspaceFile = 'Workspaces\';

            for gamV = 1:length(gam)
                for r = 1:length(dS)

                    start = cputime;

                    %% convergence variable delta
                    %
                    ss = dS(r)^gam(gamV);

                    convVar(:) = geoFunct(ss);

                    % convergence variable stored in a compatible variable to
                    % perform regression
                    for k = 1:length(convVar)
                        conv(r, gamV, k) = convVar(k);
                    end

                    iterationTime = cputime - start;
                   
                    fprintf('Iteration: %i \tGamma: %i \tCalculation Time: %i\n', r, gam(gamV), iterationTime);
                    
                    save(strcat([fDir workspaceFile simName '_ConvergenceWorkspace_' date])) 
                end
                % perform the regression for the gamma value
                regplot = figure('rend','painters','pos',[10 10 1100 800]);

                for j = 1:6
                    sx = dS.^gam(gamV);
                    cx = conv(:,gamV,j)';

                    %varMat = [sum(cx)*sum(sx.^2)-sum(sx)*sum(sx.*cx); 
                    %length(sx)*sum(sx.*cx) - sum(sx)*sum(cx)];

                    %regVar = 1/(sum(sx.^2)-sum(sx)^2) * varMat;
                    regVarPoly = polyfit(sx, cx, 1);
                    E(j, gamV) = regVarPoly(2);
                    C(j, gamV) = regVarPoly(1);
                    rx = (E(j,gamV) + C(j,gamV).*(sx));
                    Rsq(j, gamV) = sum((cx - rx).^2);

                    xp = linspace(0, 1, 100);

                    % regression and error plot for value of gamma

                    yp = @(x) E(j,gamV) + C(j,gamV) * x;
                    subplot(6, 2, j*2-1)
                    % actual points
                    plot(sx, cx, 'or')
                    % regression line
                    hold on
                    plot(xp, yp(xp), 'k:')
                    hold off
                    xlabel('delta S')
                    ylabel('Calculated Velocity')
                    title('Regression Line')
                    subplot(6, 2, j*2-0)
                    err = abs(abs(E(j, gamV)- cx)./E(j, gamV));
                    plot(sx, err)
                    xlabel('delta S')
                    ylabel('|d_e - d_i|/ d_e')
                    title('X plot Error')
                    %subplot(6, 3, j*3)
                    %plot(numS, err)
                    %xlabel('number of stokeslets')
                    %ylabel('|d_e - d_i|/ d_e')
                    %title('Stokelets vs error')
                end

                gType = '_RegressionPlots_';
                gamstr = strrep(num2str(gam(gamV)), '.', '_');

                saveas(regplot, strcat([fDir, name, '_', date, '_v', int2str(cVar), '_g', gamstr, gType]))

                saveas(regplot, strcat([fDir, name, '_', date, '_v', int2str(cVar), '_g', gamstr, gType, fFormat]))

                close all
            end


            % plot E, C, and Rsq values

            errFig = figure;
            plot(gam, Rsq(cVar,:))
            xlabel('gamma')
            ylabel('R^2')
            title(strcat(['R^2 - ', ' variable: ', int2str(cVar)], ''));

            gType = '_ErrorFigrue';

            saveas(errFig, strcat([fDir, name, '_', date, '_v', int2str(cVar), gType], ''))

            saveas(errFig, strcat([fDir, name, '_', date, '_v', int2str(cVar), gType, fFormat], ''))
%         catch ME
%             disp(ME)
%         end
    end % end convergnce function
  
    function stokesletPlot(R_S, L_dot, limits)
        [X,y] = meshgrid(limit(1,1):limit(1,2),limit(2,1):limit(2,2));
    end
  
    function stokesletPlotG()

    end
    
    function [err] = convergence_plot_3(value, dS, r)
        
        % regression model
        % y     = C r^gam dS^gam
        % ln(y) = ln(C) + gam ln(r*dS)
        
        minSize = 3;
        
        % validation that a regression can be performed on the data
        % check that dS and value are vectors
        if ~(size(value,2) == minSize && size(value,1) == 1)
            disp('THE VALUES PASSED ARE NOT OF CORRECT FORM')
            return
        end
        if ~(size(dS,2) == minSize && size(dS,1) == 1)
            disp('THE dS VALUES PASSED ARE NOT OF CORRECT FORM')
            return
        end
        
        % check that the vector sizes
        
        try
            %logdS = log(r.*dS);
            %logV  = log(values);
            
            %n = size(dS,2);
                        
            %gam = sum(logV)*(sum(logdS.^2)) - sum(logdS)*sum(logV.*logdS)/(n*sum(logdS.^2) - sum(logdS)^2);
            
            %logC = (n*sum(logdS.*logV) - sum(logdS)*sum(logV))/(n*sum(logdS.^2) - sum(logdS)^2);
            gamma = log((x_conv(2) - x_conv(3))/(x_conv(1) - x_conv(2)))/log(r);
            C = (x_conv(1) - x_conv(2))/ (dS^gamma * (1 - r^gamma));
            de = (x_conv(1) - C * dS^gamma);
            
            err = value./de;
            
        catch 'MATLAB:samelen';
            disp('Make sure the vectors are the same length')
        end
        
    
    end % end convergence plot
  end % end method(Static)
end