function [Pv] = PressureForceStokeslets(Body, P_body, ep)
	
        l = 0;
        
        for i = 1:length(Body)
            l = l + length(Body{i});
        end
        
        Pv = zeros(3*l,1);
        
        l_m = 0;
        
        for i = 1:length(Body)
            %l_j = 0;
            %for j = 1:length(Body)
                for alpha = 1:length(Body{i})
                    %for beta = 1:length(Body{j})

                        % distance between stokeslets
                        r_o = P_body{i};
                        r = Body{i}(alpha, :) - r_o(1:3);%normal vector around center
                        %r = r/norm(r);
                        rm = sqrt(sum(r.^2));

                        % regularized Pressure
                        %Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m);
                        %Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m) = (r .* (2*rm^2 + 5*ep{i}^2)/(rm^2 + ep{i}^2)^(5/2));
                        Pv(alpha*3+l_m-2:alpha*3+l_m) = (r .* ((2*rm^2 + 5*ep{i}^2)/(rm^2 + ep{i}^2)^(5/2)).^-1);
                        %Pv(beta*3+l_j-1,alpha*3+l_m-2:alpha*3+l_m) = Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m);
                        %Pv(beta*3+l_j-0,alpha*3+l_m-2:alpha*3+l_m) = Pv(beta*3+l_j-2,alpha*3+l_m-2:alpha*3+l_m);
                    %end % end for beta
                    % sum pressure at stokeslet

                    % store in matrix
                end	% end for alpha		
                %l_j = l_j + length(Body{j});
            %end % end for j

            l_m = l_m + length(Body{i});
        end % end for i
	
        %Pv(isinf(Pv))=0;
    end % end Pressure Function