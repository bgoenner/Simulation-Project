function [E, C, Rsq] = convergenceFunction(geoFunct, dS, gam, cVar, fDir, name, fFormat)
%          try
            % This function performs a convergence study on a geometry and output
            % variable. To perform the convergence study a variable must be passed
            % in the form geoFunct(dS). The easiest way to do this to wrap the
            % function in another function with the function signature required.

            %setup
            E = zeros(6, length(gam));
            C = zeros(6, length(gam));
            Rsq = zeros(6, length(gam));
            conv = zeros(length(dS), length(gam));

            date = strrep(strrep(datestr(datetime()), ':', ''), ' ', '_');
            workspaceFile = 'Workspaces\';

            for gamV = 1:length(gam)
                for r = 1:length(dS)

                    start = cputime;

                    %% convergence variable delta
                    %
                    ss = dS(r)^gam(gamV);

                    convVar(:) = geoFunct(ss);

                    % convergence variable stored in a compatible variable to
                    % perform regression
                    for k = 1:length(convVar)
                        conv(r, gamV, k) = convVar(k);
                    end

                    iterationTime = cputime - start;
                   
                    fprintf('Iteration: %i \tGamma: %i \tCalculation Time: %i\n', r, gam(gamV), iterationTime);
                    
                    save(strcat([fDir workspaceFile simName '_ConvergenceWorkspace_' date])) 
                end
                % perform the regression for the gamma value
                regplot = figure('rend','painters','pos',[10 10 1100 800]);

                for j = 1:6
                    sx = dS.^gam(gamV);
                    cx = conv(:,gamV,j)';

                    %varMat = [sum(cx)*sum(sx.^2)-sum(sx)*sum(sx.*cx); 
                    %length(sx)*sum(sx.*cx) - sum(sx)*sum(cx)];

                    %regVar = 1/(sum(sx.^2)-sum(sx)^2) * varMat;
                    regVarPoly = polyfit(sx, cx, 1);
                    E(j, gamV) = regVarPoly(2);
                    C(j, gamV) = regVarPoly(1);
                    rx = (E(j,gamV) + C(j,gamV).*(sx));
                    Rsq(j, gamV) = sum((cx - rx).^2);

                    xp = linspace(0, 1, 100);

                    % regression and error plot for value of gamma

                    yp = @(x) E(j,gamV) + C(j,gamV) * x;
                    subplot(6, 2, j*2-1)
                    % actual points
                    plot(sx, cx, 'or')
                    % regression line
                    hold on
                    plot(xp, yp(xp), 'k:')
                    hold off
                    xlabel('delta S')
                    ylabel('Calculated Velocity')
                    title('Regression Line')
                    subplot(6, 2, j*2-0)
                    err = abs(abs(E(j, gamV)- cx)./E(j, gamV));
                    plot(sx, err)
                    xlabel('delta S')
                    ylabel('|d_e - d_i|/ d_e')
                    title('X plot Error')
                    %subplot(6, 3, j*3)
                    %plot(numS, err)
                    %xlabel('number of stokeslets')
                    %ylabel('|d_e - d_i|/ d_e')
                    %title('Stokelets vs error')
                end

                gType = '_RegressionPlots_';
                gamstr = strrep(num2str(gam(gamV)), '.', '_');

                saveas(regplot, strcat([fDir, name, '_', date, '_v', int2str(cVar), '_g', gamstr, gType]))

                saveas(regplot, strcat([fDir, name, '_', date, '_v', int2str(cVar), '_g', gamstr, gType, fFormat]))

                close all
            end


            % plot E, C, and Rsq values

            errFig = figure;
            plot(gam, Rsq(cVar,:))
            xlabel('gamma')
            ylabel('R^2')
            title(strcat(['R^2 - ', ' variable: ', int2str(cVar)], ''));

            gType = '_ErrorFigrue';

            saveas(errFig, strcat([fDir, name, '_', date, '_v', int2str(cVar), gType], ''))

            saveas(errFig, strcat([fDir, name, '_', date, '_v', int2str(cVar), gType, fFormat], ''))
%         catch ME
%             disp(ME)
%         end
end % end convergnce function