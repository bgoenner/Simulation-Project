function [r_s_mat] = rotation_Body(r_s_mat, P_Lab)
          
      phi = P_Lab(4);
      theta = P_Lab(5);
      gama = P_Lab(6);
      
      Rot = [cos(theta)*cos(gama)+sin(theta)*sin(phi)*sin(gama) -sin(gama)*cos(theta)+cos(gama)*sin(theta)*sin(phi) sin(theta)*cos(phi);
          cos(phi)*sin(gama) cos(phi)*cos(gama) -sin(phi);
          -sin(theta)*cos(gama)+cos(theta)*sin(gama)*sin(phi) sin(theta)*sin(gama)+cos(theta)*cos(gama)*sin(phi) cos(phi)*cos(theta)];
      
      P = repmat([P_Lab(1), P_Lab(2), P_Lab(3)]', 1, length(r_s_mat(:,1)));
      
      r_s_mat = (Rot*(r_s_mat-P')' + P)';
end % end rotation_Body