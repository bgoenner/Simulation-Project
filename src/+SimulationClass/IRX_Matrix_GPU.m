function [IRX] = IRX_Matrix_GPU(r_, r_o)

      l = 0;
      
      for i = 1:length(r_)
        l = length(r_{i}) + l;
      end 
      
      IRX_cell = cell(length(r_), 1);
      
      for i = 1:length(r_)
          
          skewMatrix = repelem(r_{i}(:,3)-r_o{i}(3), 3, 3) .* repmat( [0 1 0; -1 0 0; 0 0 0], length(r_{i}), 1) + ...
            repelem(r_{i}(:,2) - r_o{i}(2), 3, 3) .* repmat( [0 0 -1; 0 0 0; 1 0 0], length(r_{i}), 1) + ...
            repelem(r_{i}(:,1) - r_o{i}(1), 3, 3) .* repmat( [0 0 0; 0 0 1; 0 -1 0], length(r_{i}), 1);
          
          IRX_cell{i} = [repmat(eye(3), length(r_{i}), 1), skewMatrix];

      end
          
      IRX = cell2mat(IRX_cell);
end % end IRX_Matrix_sim_GPU