function [Ginv] = G_Matrix_sim_GPU(r_s, a_, e_, mu)
      % Currently more efficient than the stokeslet matrix technique
      
      l = 0;
      
      % calculate length of cell
      for i = 1:length(r_s)
        l = length(r_s{i}) + l;
      end      

      B_cell = cell(length(r_s));

        for i = 1:length(r_s) % iterate over bodies
            S_diag = 1/(6*pi*a_{i}*mu);
            for j = i:length(r_s)
                %G_cell = cell(length(r_s{i}), length(r_s{j}));
                %G_arr = gpuArray(zeros(length(r_s{i})*3, length(r_s{j})*3));
                %if (length(r_s{i}) * length(r_s{j}) > 625000000)
                % setup calculations
				G_arr = zeros(length(r_s{i}) * 3, length(r_s{j}) * 3);
				
				% move the stokeslet bodies onto the GPU				
				
				R1 = gpuArray(r_s{i});
				R2 = gpuArray(r_s{j});
				for alpha = 1:length(r_s{i})     % Iterate over spheres
					prevBeta = 1;
					for beta = 1:2000:length(r_s{j})
					
						if beta > length(r_s{j})
							%beta = length(r_s{j});
						end
					
						if i ~= j
							% calculate the differences between stokeslets
							R3  = repmat(R1(alpha, prevBeta:beta), length(R2), 1)  - R2;%repmat Creates a length(R2)-by-1 matrix whose elements contain the value R1(alpha, :)
						else % i == j
							R3  = repmat(R1(alpha, prevBeta:beta), length(R2)-alpha, 1)  - R2(alpha+1:end, prevBeta:beta);
						end
					
						% get the norm of each difference vector and create diagonal matrices
						R3S = repelem(sqrt(sum(R3.^2, 2)), 3, 3); %(R3 * R3').^(1/2);
					  
						deltaGPU = repmat(gpuArray.eye(3), length(R3(:,1)), 1);
						
						% calculation done in regularized stokeslet
						R3A = reshape(repelem(R3, 1, 3)', 3, [])' .* repelem(R3, 3, 1);% refer to definition of B = reshape(A,sz1,...,szN)....
						
						% epsilon value matrix
						e_G = gpuArray.ones(length(R3(:,1))*3, 3) * e_{i};
						
						% regularized stokeslet
						G_GPU =  1/(8 * pi * mu) * (deltaGPU .* (R3S.^2 + 2*e_G.^2)./(R3S.^2 + e_G.^2).^(3/2) + (R3A)./(R3S.^2 + e_G.^2).^(3/2)); 
						
						% copy array to full system matrix
						G_arr(alpha*3-2:alpha*3, prevBeta:beta) = gather(G_GPU)';
						
						prevBeta = beta + 1;
					end % end for beta
				end % end for alpha

				if i ~= j
					B_cell{i, j} = G_arr;
				else % i == j
					G_arr = G_arr + S_diag.*eye(length(G_arr)) + G_arr';
					B_cell{i, j} = G_arr;
				end % end if
            end % end for j
        end % end for i
        
        for i = 1:length(r_s)
            for j = 1:i
                if i ~= j
                    B_cell{i, j} = B_cell{j, i}';
                end
            end
        end
        
% %         if length(G)<3000 
            
           G = gpuArray(cell2mat(B_cell));

           Ginv = gather(G^-1);
%         else 
%            Ginv = cell2mat(B_cell)^-1;
%         
%         end

        %Ginv
        
    end % end G_Matrix_sim_GPU