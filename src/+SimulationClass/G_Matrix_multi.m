function [Ginv] = G_Matrix_multi(r_s, a_, e_, mu)
    % Currently more efficient than the stokeslet matrix technique

    l = 0; 

    % calculate length of cell
    for i = 1:length(r_s)
        l = length(r_s{i}) + l;
    end      

    indA = 0;
    indB = 0;
    G_cell = cell(l);


    delta = eye(3);
    for i = 1:length(r_s) % iterate over bodies
        S_diag = 1/(6*pi*a_{i}*mu);
        for j = 1:length(r_s)
          for alpha = 1:length(r_s{i})     % Iterate over spheres
            for beta = 1:length(r_s{j})
              if alpha == beta && i == j                    %This would mean the same sphere
                  velocity = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
              else                              %Hydrodynamic Forces
                  position = r_s{i}(alpha, :) - r_s{j}(beta, :);

                  % inlined stokeslet_e reduces function calls
                  % TODO verify working code

                  r = (position * position')^(1/2);
                  r_ = position;

                  velocity = 1/(8 * pi * mu) * (delta *(r^2 + 2*e_{i}^2)/(r^2 + e_{i}^2)^(3/2) + (r_' * r_)/(r^2 + e_{i}^2)^(3/2));       
              end

              G_cell{indB + alpha,indA + beta} = velocity;    % Append to G matrix
            end
          end
          indA = indA + length(r_s{j});
        end
        indB = indB + length(r_s{i});
    end  

    Ginv = cell2mat(G_cell)^-1;



end % end G_Matrix_sim_multi