function [] = JonesWallsSim(x, y, z, W, z0)

% define anonmous functions

q_x = x;
q_y = y;

syms q_ phi_

[q, phi] = solve(q_x == q_ * cos(phi_), q_y == q_ * sin(phi_));

q = q(1);

s = sqrt(x^2+y^2);

u = @(q, W) q * W;
v = @(q, z0) q * z0;
w = @(q, z) q * z;

A_p = @(u) u + 0.5*(1 - e^(-2*u));
B_p = @(u) u + 0.5*(1 + e^(-2*u));
C_p = @(u) u*(1 + u) + 0.5*(1 - e^(-2*u));
D_p = @(u) u*(1 + u) + 0.5*(1 + e^(-2*u));
E_p = @(u) 1 / (sinh(u)*cosh(u) + u);

A_n = @(u) u - 0.5*(1 - e^(-2*u));
B_n = @(u) u - 0.5*(1 + e^(-2*u));
C_n = @(u) u*(1 + u) - 0.5*(1 - e^(-2*u));
D_n = @(u) u*(1 + u) - 0.5*(1 + e^(-2*u));
E_n = @(u) 1 / (sinh(u)*cosh(u) - u);


t_1nn = @(u, v, w) ...
    E_n(u) * (-v*cosh(v) + B_p(u)*sinh(v))*w*cosh(w) + ...
    E_p(u) * (-v*sinh(v) + A_p(u)*cosh(v))*w*sinh(w) + ...
    E_p(u) * (A_p*v*sinh(v) - C_p(u) * cosh(v))*cosh(w) + ...
    E_n(u) * (B_p*v*cosh(v) - D_p(u) * sinh(v))*sin(w);

t_1np = @(u, v, w) ...
    E_n(u) * (v*sinh(v) - A_n(u)*cosh(v))*w*cosh(w) + ...
    E_p(u) * (v*cosh(v) + B_n(u)*sinh(v))*w*sinh(w) + ...
    E_p(u) * (-A_p*v*cosh(v) + u^2 * sinh(v))*cosh(w) + ...
    E_n(u) * (-B_p*v*sinh(v) + u^2 * cosh(v))*sin(w);

t_1pn = @(u, v, w) ...
    E_p(u) * (-v*sinh(v) + A_p(u)*cosh(v))*w*cosh(w) + ...
    E_n(u) * (-v*cosh(v) + B_p(u)*sinh(v))*w*sinh(w) + ...
    E_n(u) * (A_n*v*cosh(v) - u^2 * sinh(v))*cosh(w) + ...
    E_p(u) * (B_n*v*sinh(v) - u^2 * cosh(v))*sinh(w);

t_1pp = @(u, v, w) ...
    E_p(u) * (v*cosh(v) - B_n(u)*sinh(v))*w*cosh(w) + ...
    E_n(u) * (v*sinh(v) - A_n(u)*cosh(v))*w*sinh(w) + ...
    E_n(u) * (-A_n*v*sinh(v) + C_n(u) * cosh(v) - 2*u*tanh(u)*cosh(v))*cosh(w) + ...
    E_+(u) * (-B_n*v*cosh(v) + D_n(u) * sinh(v) - 2*u*coth(u)*sinh(v))*sin(w);

r_1pp = @(u, v, w) ...
    (-2*e^(-u) * cosh(v)/cosh(u)) * cosh(w) + ...
    (-2*e^(-u) * sinh(v)/sinh(u)) * sinh(w);


G_2W_xx = @(x, y, z, q, z0, s) ...
    -0.5 * quadgk(besselj(0, q*s) + (y^2 - x^2)/s^2 * besselj(2, q*s)*t_1pp(q, z, z0), 0, Inf) + ...
    quadgk(besselj(0, q*s) * r_1pp(q, z, z0), 0, Inf);

G_2W_yy = @(x, y, z, q, z0, s) ...
    -0.5 * quadgk(besselj(0, q*s) + (x^2 - y^2)/s^2 * besselj(2, q*s)*t_1pp(q, z, z0), 0, Inf) + ...
    quadgk(besselj(0, q*s) * r_1pp(q, z, z0), 0, Inf);

G_2W_zz = @(x, y, z, q, z0, s) ...
    quadgk(besselj(0, q*s) * t_1nn(q, z, z0), 0, Inf);

G_2W_xy = @(x, y, z, q, z0, s) ...
    x*y / s^2 * quadgk(besselj(2, q*s) * t_1pp(q, z, z0), 0, Inf);

G_2W_yx = @(x, y, z, q, z0, s) ...
    G_2W_xy(x, y, z, q, z0, s);

G_2W_xz = @(x, y, z, q, z0, s) ...
    - x/s * quadgk(besselj(1, q*s) * t_1pn(q, z, z0), 0, Inf);

G_2W_yz = @(x, y, z, q, z0, s) ...
    - y/s * quadgk(besselj(1, q*s) * t_1pn(q, z, z0), 0, Inf);

G_2W_zx = @(x, y, z, q, z0, s) ...
    - x/s * quadgk(besselj(1, q*s) * t_1np(q, z, z0), 0, Inf);

G_2W_zy = @(x, y, z, q, z0, s) ...
    - y/s * quadgk(besselj(1, q*s) * t_1np(q, z, z0), 0, Inf);

G_2W = @(x, y, z, q, z0, s) ...
    [G_2W_xx(x, y, z, q, z0, s), G_2W_xy(x, y, z, q, z0, s), G_2W_xz(x, y, z, q, z0, s); ...
    G_2W_yx(x, y, z, q, z0, s), G_2W_yy(x, y, z, q, z0, s), G_2W_yz(x, y, z, q, z0, s); ...
    G_2W_zx(x, y, z, q, z0, s), G_2W_zy(x, y, z, q, z0, s), G_2W_zz(x, y, z, q, z0, s)];


% Pressure vector

x_0 = [x, y, z];

Q = @(x, x0) 2 * x / norm(x)^3;

p_1p = @(u, v, w) E_n(u)*(A_n(u)*cosh(v) - v*sinh(v))*cosh(w) + E_p(u)*(B_n(u)*sinh(v) - v*cosh(v))*sinh(w);
p_1n = @(u, v, w) E_n(u)*(B_p(u)*sinh(v) - v*cosh(v))*cosh(w) + E_p(u)*(A_p(u)*cosh(v) - v*sinh(v))*sinh(w);

Q_2W_x = @(W, x, x0, s)  2 * x / s * quadqk(q * besselj(1, q*s) * p_1p(u, v, w), 0, Inf);
Q_2W_y = @(W, x, x0, s)  2 * y / s * quadqk(q * besselj(1, q*s) * p_1p(u, v, w), 0, Inf);
Q_2W_z = @(W, x, x0, s)  2 * quadqk(q * besselj(1, q*s) * p_1n(u, v, w), 0, Inf);


end


