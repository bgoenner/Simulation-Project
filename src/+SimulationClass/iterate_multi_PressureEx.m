function [V_Lab, V_G, V_LJ, M, mem, numS, V_P, P_mat] = iterate_multi_PressureEx(r_, r_o, L_dot, r_n, P, a_, e_, mu, sig_LJ, e_LJ)
        
        V_Lab = zeros(6,1);
        V_G   = zeros(6,1);
        V_LJ  = zeros(6,1);
        M     = zeros(6,6);
        V_P   = zeros(6,1); 
        
        
        % V_Lab the velocity of the 
        import SimulationClass.*

        %if length(r_) ~= length(r_o) % r_o, body centers. % r_, simulation bodies
        %    disp('Number of body centers and simulation bodies are not equal')
        %    return
        %end

        % get G_Matrix
        [Ginv] = G_Matrix_sim_multi_S(r_, a_, e_, mu);% single particle

        % get IRX
        IRX = IRX_Matrix_sim_multi(r_, r_o);

        % get LJ Forces
        LJ = LennardJoneForce_sim(r_, sig_LJ, e_LJ);

        P_mat = PressureForceStokeslets(r_, r_o, e_);

        % Calculate resistance Matrix
        M = IRX' * Ginv * IRX;

        % V =   R^-1 * (force/torques mapping) * [Forces]  
        V_LJ =  -M^-1 * (IRX)' * LJ;	  
        V_G  =  -M^-1 * (IRX)' * Ginv * L_dot;

        % Prssure Force
        V_P = -M^-1 * IRX' * ((P_mat) .* (P));%we do not know if we gor it correctly??

        % Calculate Body velocity
        V_Body = V_G + V_LJ + V_P;

        % convert to labFrame
        V_Lab = V_Body;

        user = memory;
        mem = user.MemUsedMATLAB;
        numS = length(Ginv)/3;
        
        %Assumed Pressure Force (r_n .* repelem(P, 3, 1))
        %V_Pex = -M^-1 * (IRX)' * Ginv * (r_n .* repelem(P, 3, 1));
        
        %V_Lab = V_Lab + V_Pex;
        V_Lab = V_Lab;
    end % end iterate function