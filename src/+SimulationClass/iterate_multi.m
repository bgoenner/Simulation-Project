function [V_Lab, V_G, V_LJ, M, mem, numS] = iterate_multi(r_, r_o, L_dot, a_, e_, mu, sig_LJ, e_LJ)

    import SimulationClass.*

    if length(r_) ~= length(r_o) % r_o, body centers. % r_, simulation bodies
    disp('Number of body centers and simulation bodies are not equal')
    return
    end

    % get G_Matrix
    [Ginv] = LowReynoldsSimulation.G_Matrix_sim_multi_S(r_, a_, e_, mu);% single particle

    % get IRX
    IRX = LowReynoldsSimulation.IRX_Matrix_sim_multi(r_, r_o);

    % get LJ Forces
    LJ = LowReynoldsSimulation.LennardJoneForce_sim(r_, sig_LJ, e_LJ);

    % Calculate resistance Matrix
    M = IRX' * Ginv * IRX;

    % V =   R^-1 * (force/torques mapping) * [Forces]  

    V_LJ  =  -M^-1 * (IRX)'* LJ;	  
    V_G =  -M^-1 * (IRX)' * Ginv * L_dot;

    % Prssure Force
    % V_P = -M^-1 * (IRX)' * (P * n);

    % Calculate Body velocity
    V_Body = V_G + V_LJ;

    % convert to labFrame
    V_Lab = V_Body;

    user = memory;
    mem = user.MemUsedMATLAB;
    numS = length(Ginv)/3;


end % end iterate_multi