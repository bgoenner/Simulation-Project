function [IRX] = IRX_Matrix_multi(r_, r_o)
      
      indB = 0;
      l = 0;
      
      for i = 1:length(r_)
        l = length(r_{i}) + l;
      end 
      
      IRX_cell = cell(l, 1);
      
      for i = 1:length(r_)
        for cll = 1:1:length(r_{i})
            IRX_cell{indB + cll,1} = [1 0 0 0                     (r_{i}(cll, 3)-r_o{i}(3))     -(r_{i}(cll, 2)-r_o{i}(2)); 
                                      0 1 0 -(r_{i}(cll, 3)-r_o{i}(3)) 0                        (r_{i}(cll, 1)-r_o{i}(1)); 
                                      0 0 1 (r_{i}(cll, 2)-r_o{i}(2))  -(r_{i}(cll, 1)-r_o{i}(1))    0];
        end
        
        indB = indB + length(r_{i});
      end
          
      IRX = cell2mat(IRX_cell);
end % end IRX_Matrix