function [err] = convergence_plot(value, dS, r)
        
        % regression model
        % y     = C r^gam dS^gam
        % ln(y) = ln(C) + gam ln(r*dS)
        
        minSize = 3;
        
        % validation that a regression can be performed on the data
        % check that dS and value are vectors
        if ~(size(value,2) == minSize && size(value,1) == 1)
            disp('THE VALUES PASSED ARE NOT OF CORRECT FORM')
            return
        end
        if ~(size(dS,2) == minSize && size(dS,1) == 1)
            disp('THE dS VALUES PASSED ARE NOT OF CORRECT FORM')
            return
        end
        
        % check that the vector sizes
        
        try
            %logdS = log(r.*dS);
            %logV  = log(values);
            
            %n = size(dS,2);
                        
            %gam = sum(logV)*(sum(logdS.^2)) - sum(logdS)*sum(logV.*logdS)/(n*sum(logdS.^2) - sum(logdS)^2);
            
            %logC = (n*sum(logdS.*logV) - sum(logdS)*sum(logV))/(n*sum(logdS.^2) - sum(logdS)^2);
            gamma = log((x_conv(2) - x_conv(3))/(x_conv(1) - x_conv(2)))/log(r);
            C = (x_conv(1) - x_conv(2))/ (dS^gamma * (1 - r^gamma));
            de = (x_conv(1) - C * dS^gamma);
            
            err = value./de;
            
        catch 'MATLAB:samelen';
            disp('Make sure the vectors are the same length')
        end
        
    
end % end convergence plot