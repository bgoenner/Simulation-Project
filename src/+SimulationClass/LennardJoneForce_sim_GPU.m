function [F] = LennardJoneForce_sim_GPU(Body, F_0, sig)
      %initialize cell array
      %for i = 1:length(r_s)
      %  l = length(r_s{i}) + l;
      %end 
      F = cell(length(Body));

      for i = 1:length(Body)
        for j = 1:length(Body)
          if (i ~= j)
            F_arr = zeros(3*length(Body{i}), 3*length(Body{j}));
            R1 = gpuArray(Body{i});
            R2 = gpuArray(Body{j});
            for alpha = 1:length(Body{i})     % Iterate over spheres
                
                
                % calculate the differences between stokeslets
                R3  = repmat(R1(alpha, :), length(R2), 1)  - R2;

                % get the norm of each difference vector and create
                % diagonal matrices
                R3S = repelem(sqrt(sum(R3.^2, 2)), 3, 3);%R3 * R3').^(1/2);

                % diag_cell = F_0 * sig / 6 * ((sig/rm)^12-(sig/rm)^6).* [1 1 1]' * rij/norm(rij);
                F_GPU = F_0 * sig ./ 6 .* ((sig./R3S).^12-(sig./R3S).^6).* (reshape(repelem(R3, 1, 3)', 3, [])' ./ R3S);

                % copy array to full system matrix
                F_arr(alpha*3-2:alpha*3, :) = gather(F_GPU)';

            end
            
            F{i, j} = F_arr;
            
          else
            X = zeros(length(Body{i})*3);
        
            F{i, j} = X;  
          end
        end   
      end
      
      F = cell2mat(F);
      
      I  = ones(length(F),1);%size(I):Used identity matrix to make size(F)=4191*4191 to size(I)=4191*1
      F = F*I;
    end % end LennardJoneForce