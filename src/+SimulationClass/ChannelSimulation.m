% ChannelSimulation
classdef ChannelSimulation

    properties
       homedir = '.\';
       deltaT = 0;
       time = 0;
       R_s = 0;
       chan_s = 0;
       Vel_Interpolate = {0, 0, 0};
       Vel_InterpolatePos = {0, 0};
    end
    
    methods(Hidden = true)
        

        function obj = ChannelSimulation(home)
            obj.homedir = home;
            
            % check for folders
            if exist(home)
            end
            if exist(join([home, 'Figures'], ''))
            end
            if exist(join([home, 'TaskList'], ''))
            end 
            if exist(join([home, 'Logs'], ''))
            end
            if exist(join([home, 'Mesh', ''))
            end
            if exist(join([home, 'Workspaces']))
            end
            if exist(join([home, 'JobSettings']))
            end
            if exist(join([home, 'Convergence']))
            end
        end % end channel Simulation
                
        
        % wait for job
        function wait()
            while true
                % check task file
                if (task == checkTaskFile)
                    if task == 'newJob'
                    % if new job
                        % parse job
                        getArguments()
                        % check validity job
                        if validJob()
                            try
                            % instanciate variables
                            setEnvironmentVariables()
                            % Simulate
                            simulate()
                            % return status of simulation
                            updateLog()

                            % save copmleted data
                            % clean up environment

                            catch
                                % if errors log them

                            end % end catch
                        elseif task == 'newCom'
                        % if command
                            % stop
                        end % end if
                    end % end if
                end % end if
            end % end while loop
        end % end wait function
        
        % initialize variables

        % initialize geometry

        % initialize channel

        % initialize backgroundFlow
        function addBackgroundFlow(obj, backGroundFile, skipLines)
            
            dat_dat = csvread(backGroundFile, skipLines);
            obj.Vel_InterpolatePos(1) = dat_dat(:,1)';%which is x axis
            obj.Vel_InterpolatePos(2) = dat_dat(:,2)';%which is y axis
            obj.Vel_Interpolate(1) = dat_dat(:,3)';%which is Ux axis
            obj.Vel_Interpolate(2) = dat_dat(:,5)';%which is Uz axis
            obj.Vel_Interpolate(3) = dat_dat(:,4)';%which is Uy axis
            
        end

        % iterate
        function run_simulation(obj, particleFunct, channel, backgoundFlow, iterations, delta_time)
            
            t_ = linspace(0, (iterations - 1) * delta_time, delta_time);
                        
            obj.R_sFunct = particleFunct;
            obj.chan_s = channel;
            obj.addBackgourndFlow(backgoundFlow, 10);
            
            % generate particle
            r_s = obj.R_sFunct(P_body);
            
            % open log file
            obj.LFile = fopen(logfile);
            
            % preallocate space
            V_body = zeros(iterations+1, 6);
            P_body = zeros(iterations+1, 6);
                        
            for k = 1:length(t_)
                % interpolate
                l_dot = obj.interpolate(r_s);
                % calculate Velocity               
                V_Body(k+1,:) = obj.iterate({obj.R_s, obj.chan_s}, {obj.P, obj.P_chan}, l_dot, obj.a, obj.eps, obj.mu, obj.a_LJ, obj.e_LJ);
                % integrate
                P_Body(k+1,:) = P_body(k) + V_body(k+1) * delta_time;
                % regenerate particle
                r_s = obj.R_sFunct(P_body);
                
                % log data
                logArray(LFile, V_body(k+1, :))
                logArray(LFile, P_body(k+1, :))
                %logData(LFile, mem);
                
            end % end for loop
        % plot and save figures
        end % end simulate function
        
        function [] = logArray(obj, file, input)
            for i = 1:length(input)
                 fprintf(file, strcat(num2str(input), ','));
            end
        end
        
        function [] = logVar(obj, file, input)
            for i = 1:length(input)
                fprintf(file, strcat(num2str(input), ','));
            end
        end
        
        
        function [] = logNewLine(obj, file)
            fprintf(file, '\n');
        end
        
           
    end % end methods Hidden
    methods(Static=true)
        
        function [out] = getVarName(var)
           out = inputName(1); 
        end
        
        function start(home)
            sim = ChannelSimulation(home);
            sim.wait()
        end % end start
        
        function genChannel(x_c, y_c, z_c, dS)
            
        end % end genChannel
        
        function meshSTL(stlFile, elementSize)
            
        end % end meshSTL
        

        % Matrix Calculation
        function [G] = G_Matrix_sim_multi(r_s, a_, e_, mu)
            LowReynoldsSimulation.G_Matrix_sim_multi_S(r_s, a_, e_, mu)
        end
        function [IRX] = IRX_Matrix_sim_multi(r_, r_o)
            LowReynoldsSimulation.IRX_Matrix_sim_multi(r_, r_o)
        end
        
    end
end