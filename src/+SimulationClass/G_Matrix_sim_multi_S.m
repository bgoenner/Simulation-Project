function [Ginv] = G_Matrix_sim_multi_S(r_s, a_, e_, mu)
      % Currently more efficient than the stokeslet matrix technique
      
      l = 0;
      
      % calculate length of cell
      for i = 1:length(r_s)
        l = length(r_s{i}) + l;
      end      

      B_cell = cell(length(r_s));
      
      delta = eye(3);
      velocity = zeros(3);
      for i = 1:length(r_s) % iterate over bodies
        
        for j = i:length(r_s)
            S_diag = 1/(6*pi*a_{i}*mu);
            G_cell = cell(length(r_s{i}), length(r_s{j}));
            if i ~= j
              for alpha = 1:length(r_s{i})     % Iterate over spheres
                for beta = 1:length(r_s{j})
                                                %Hydrodynamic Forces
                  position = r_s{i}(alpha, :) - r_s{j}(beta, :);

                  % inlined stokeslet_e reduces function calls

                  r = (position * position')^(1/2);
                  r_ = position;

                  velocity = 1/(8 * pi * mu) * (delta *(r^2 + 2*e_{i}^2)/(r^2 + e_{i}^2)^(3/2) + (r_' * r_)/(r^2 + e_{i}^2)^(3/2));       

                  G_cell{alpha,beta} = velocity;    % Append to G matrix
                  
                end % end for beta
              end % end for alpha
              
              G_arr = cell2mat(G_cell);
              B_cell{i, j} = G_arr;
              B_cell{j, i} = G_arr';
          
            elseif i == j
                for alpha = 1:length(r_s{i})     % Iterate over spheres
                    for beta = 1:length(r_s{j})
                      if alpha == beta && i == j                    %This would mean the same sphere
                          %velocity = [S_diag 0 0; 0 S_diag 0; 0 0 S_diag];
                            % do nothing
                      elseif beta > alpha   %Hydrodynamic Forces
                          position = r_s{i}(alpha, :) - r_s{j}(beta, :);

                          % inlined stokeslet_e reduces function calls

                          r = (position * position')^(1/2);
                          r_ = position;

                          velocity = 1/(8 * pi * mu) * (delta *(r^2 + 2*e_{i}^2)/(r^2 + e_{i}^2)^(3/2) + (r_' * r_)/(r^2 + e_{i}^2)^(3/2));       
                      else
                        velocity = [0 0 0; 0 0 0; 0 0 0];
                      end % end if

                      G_cell{alpha,beta} = velocity;    % Append to G matrix

                    end % end for beta

                end % end for alpha
              
              G_arr = cell2mat(G_cell);
              G_arr = G_arr + S_diag.*eye(length(G_arr)) + G_arr';
              B_cell{i, j} = G_arr;
            end % end if
            
        end
      end  
          
      Ginv = cell2mat(B_cell)^-1;
      
      
      
    end % end G_Matrix_sim_multi