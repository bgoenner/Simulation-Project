 function [Ginv] = G_Matrix_sim_GPU_Pool(r_s, a_, e_, mu)
      % Currently more efficient than the stokeslet matrix technique
      
      
      
      l = 0;
      
      % calculate length of cell
      for i = 1:length(r_s)
        l = length(r_s{i}) + l;
      end      

      B_cell = cell(length(r_s));

        for i = 1:length(r_s) % iterate over bodies
            S_diag = 1/(6*pi*a_{i}*mu);
            for j = i:length(r_s)
                %G_cell = cell(length(r_s{i}), length(r_s{j}));
                %G_arr = gpuArray(zeros(length(r_s{i})*3, length(r_s{j})*3));
                %if (length(r_s{i}) * length(r_s{j}) > 625000000)
                if i ~= j

                    G_cell= cell(1, length(r_s{i}));
                    R1 = gpuArray(r_s{i});
                    R2 = gpuArray(r_s{j});
                    parfor alpha = 1:length(r_s{i})-1     % Iterate over spheres
                        
                        R3  = repmat(R1(alpha, :), length(R2)-alpha, 1)  - R2(alpha+1:end, :);
                        
                        R3S = repelem(sqrt(sum(R3.^2, 2)), 3, 3);%R3 * R3').^(1/2);
                        
                        deltaGPU = repmat(gpuArray.eye(3), length(R3(:,1)), 1);
                      
                        R3A = reshape(repelem(R3, 1, 3)', 3, [])' .* repelem(R3, 3, 1);
                        
                        e_G = gpuArray.ones(length(R3(:,1))*3, 3) * e_{i};
                        
                        G_GPU =  1/(8 * pi * mu) * (deltaGPU .* (R3S.^2 + 2*e_G.^2)./(R3S.^2 + e_G.^2).^(3/2) + (R3A)./(R3S.^2 + e_G.^2).^(3/2)); 

                        G_cell{alpha} = gather(G_GPU)

                        
                    end % end for alpha

                    G_arr = cell2mat(G_cell)';
                    B_cell{i, j} = G_arr;
                    
                    clear G_arr

                elseif i == j

                    G_cell= cell(1, length(r_s{i}));
                    R1 = gpuArray(r_s{i});
                    R2 = gpuArray(r_s{j});
                    
                    parfor alpha = 1:length(r_s{i})     % Iterate over spheres
                        
                        
                        %R3p  = repmat(R1(alpha, :), length(R2p), 1)  - R2p;
                        %R3Sp = repelem(sqrt(sum(R3p.^2, 2)), 3, 3).* repmat(eye(3), length(R2p), 1);%eye(length(R2p)) .* (R3p * R3p').^(1/2);
                        
                        %R3  = repmat(R1(alpha, :), length(R2), 1)  - R2;
                        R3  = repmat(R1(alpha, :), 1+length(R2)-alpha, 1)  - R2(alpha:end, :);
                        
                        R3S = repelem(sqrt(sum(R3.^2, 2)), 3, 3);%R3 * R3').^(1/2);
                      
                        deltaGPU = repmat(gpuArray.eye(3), length(R3(:,1)), 1);
                        
                        R3A = reshape(repelem(R3, 1, 3)', 3, [])' .* repelem(R3, 3, 1);
                        
                        e_G = gpuArray.ones(length(R3(:,1))*3, 3) * e_{i};
                        
                        G_GPU = gpuArray.zeros(length(R2)*3, 3);
                        
                        G_GPU(alpha*3-2:end, :) =  1/(8 * pi * mu) * (deltaGPU .* (R3S.^2 + 2*e_G.^2)./(R3S.^2 + e_G.^2).^(3/2) + (R3A)./(R3S.^2 + e_G.^2).^(3/2)); 

                        G_cell{alpha} = gather(G_GPU)
                        
                    end % end for alpha

                    G_arr = cell2mat(G_cell);
                    %G_arr = gather(G_GPU);
                    G_arr = G_arr + S_diag.*eye(length(G_arr)) + G_arr';
                    %G_arr = G_arr + G_arr';
                    B_cell{i, j} = G_arr;
                    
                    clear G_arr
                end % end if
            end
        end
        
        for i = 1:length(r_s)
            for j = 1:i
                if i ~= j
                    B_cell{i, j} = B_cell{j, i}';
                end
            end
        end
        
        G = gpuArray(cell2mat(B_cell));

        Ginv = gather(G^-1);
    end % end G_Matrix_sim_GPU_Pool