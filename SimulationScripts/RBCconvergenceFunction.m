function [V_body] = RBCconvergenceFunction(ds1, ds2)


    addpath('.\..\src', '.\..\SimulationScripts', '.\..\backgroundFlow');
    import SimulationClass.LowReynoldsSimulation.*
    import LowReynoldsGeometry.*
    
    
    % [V_Lab, V_G, V_LJ, M, mem, numS] = iterate_multi(r_, r_o, L_dot, a_, e_, mu, sig_LJ, e_LJ)
    % [errFig, E, C, Rsq] = convergenceFunction(geoFunct, dS, gam, cVar, fDir, name, verbose)

    Pos = [0, 0, 0, 0, 0, 0];
    
    mu = 8.9*10^-4;
    
    backgroudFlowFileName = 'Vfield_100w_50h_r2000_spiral.csv';
    dat_dat = csvread(backgroudFlowFileName,10);
    r = dat_dat(:,1)';%which is x axis
    z = dat_dat(:,2)';%which is y axis
    u = dat_dat(:,3)'*10e6;%which is Ux axis
    v = dat_dat(:,5)'*10e6;%which is Uz axis
    w = dat_dat(:,4)'*10e6;%which is Uy axis
    
    U = scatteredInterpolant(r', z', u');
    V = scatteredInterpolant(r', z', v');
    W = scatteredInterpolant(r', z', w');
    
    % generate channel geometry
    x_chan_center = 2000;
    y_chan_center = 0;
    z_chan_center = 0;
    ang_chan      = 0.1;
    dS_chan       = 1.3;
    dS_chan       = ds2;
    eps_chan      = dS_chan^0.9;
    R_chan        = 2000;
    width_chan    = 100;    % um
    heigh_chan    = 50;     % um
    a_chan        = dS_chan/10;

    % define wall stokeslet postions
    % CurvedChannel(x_center, y_center, z_center, R_channel, width, height, ang_channel, dS)
    r_w = CurvedChannel(x_chan_center, y_chan_center, z_chan_center, R_chan, width_chan, heigh_chan, ang_chan, dS_chan);

    
    % redBloodCell(xp, yp, zp, delta)
    r_s = unique(cell2mat(redBloodCell(Pos(1), Pos(2), Pos(3), ds1)), 'rows');
    
    %% interpolate velocities
    r_s_x_prime = (r_s(:,1).^2 + r_s(:,3).^2 ).^(1/2);
    u_s = -U(r_s_x_prime , r_s(:, 2));
    v_s = -V(r_s_x_prime , r_s(:, 2));
    w_s = -W(r_s_x_prime , r_s(:, 2));
    
    l_dot = [cell2mat(mat2cell([u_s v_s w_s], ones(length(v_s),1), 3)')'; zeros(length(r_w) * 3, 1)];
    
    V_body = iterate_multi({r_s, r_w}, {Pos, Pos}, l_dot, {ds1/10, a_chan}, {ds1^0.9, eps_chan}, mu, 0, 0);


end
