clc
clear
close all

% function [] = sphereScriptFunction(radius, ds1, ds2, itera, deltaT, LJ_sig, LJ_r, initPos)
% the radius argument now works properly

% RBC = @(P_body) generate_RBC_particle(P_body, ds1);
fDir = 'C:\Users\Chris\Documents\Brady\Simulation\';
simName = '_sphere_radius_1_5_';
simName = '_sphere_radius_1_5_Bfix';
addpath('.\..\backgroundFlow')

 ds1 = 0.10e-6;
 ds2 = 1.6e-6;
 itera = 5;
 
 F_0 = 0.8e-15;% WAS 0.8e-15;from bacteria paper, flagella...
 sig = 4 * ds1/10;%from properties of our simulation
 
 s_radius = 2e-6;
% sphereScriptFunction(0.5, ds1, ds2, 3000, 1e-7, 0, 0, [1990, 0, 0])

sphere = @(P_body) generate_sphere_particle(P_body, ds1, s_radius);

backgroudFlowFileName = 'Vfield_100w_50h_r2000_spiral.csv';
dat_dat = csvread(backgroudFlowFileName,10);
r = dat_dat(:,1)';%which is x axis
z = dat_dat(:,2)';%which is y axis
u = dat_dat(:,3)'*10e6;%which is Ux axis
v = dat_dat(:,4)'*10e6;%which is Uz axis
w = dat_dat(:,5)'*10e6;%which is Uy axis

U = scatteredInterpolant(r', z', u');
V = scatteredInterpolant(r', z', v');
W = scatteredInterpolant(r', z', w');

Zero = scatteredInterpolant(r', z', zeros(length(u), 1));

% sphereScriptFunction(radius, ds1, ds2, itera, deltaT, LJ_sig, LJ_r, initPos)

radius = 1.5e-6;
ds1 = 0.12e-6;
ds2 = 1.1e-6;%1.6;

% coarse mesh

ds1 = 0.6e-6;
ds2 = 2.0e-6;%1.6;

itera = 2;%6000;%6;% ds1, ds2 and iter have been overwritten  
deltaT = 3e-12;%5e-6;% write 1e-6 or 5e  -6, it should have factor, it does not allow you to run simulation w/o factor behind of e-6
%To continue particle tracking on iter= 547
%  for i = 1957+5:0.5:2057-5
%      for j = -25+4:0.5:25-4
 for i = 1957+5:1.5:2057-5
     for j = -25+4:1.5:25-4
         
xs = 2010*1e-6;%1965*1e-6; %(1990+i)*1e-6; 
ys = 0;
zs = 0*1e-6;%10e-6;%20e-6;% 22e-6
initPos = [xs, ys, zs, 0, 0, 0];

ang = 0.25;%0.35;%0.85;%0.9;%0.95;1; 1.05; 1.1; 1.15;1.2;
% for ang = 0.6:0.65:10;
% end 
%initPos = [2004.4023,1108.2894,24.3566,-16.9025,-0.51925,8.4285];


%sphereSpiralTest(radius, ds1, ds2, itera, deltaT, sig, F_0, initPos);
%function [] = sphereSpiralTest(radius, ds1, ds2, itera, deltaT, ang, LJ_sig, LJ_r, initPos)
sphereSpiralTest(radius, ds1, ds2, itera, deltaT, ang, sig, F_0, initPos);
    end
end

% start at center CHANNEL CENTERLINE
% initPos = [1990, 0, 0];
% simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% dt = 1e-6;
% particleFocusingFunction(sphere, ds1, ds2, itera, dt, 0, 0, initPos, fDir, simName)
%end
% start inner wall
% initPos = [2000, 0, 0];
% simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% dt = 1e-6;
% particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)


% % start NEAR1 inner wall, CHANNEL CENTERLINE
% for s_radius = 0.25:0.25:3
% sphere = @(P_body) generate_sphere_particle(P_body, ds1, s_radius);
% initPos = [2000, 0, 0];
% simName = strcat([strcat(['_sphere_radius_' strrep(num2str(s_radius), '.', '_')  '_init_']) strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% dt = 5e-7;
% particleFocusingVelFunction(sphere, ds1, ds2, itera, dt, sig, F_0, U, V, W, initPos, fDir, simName)
% end
% 
% % start NEAR2 inner wall, CHANNEL CENTERLINE
% initPos = [1980, 0, 0];
% simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% dt = 1e-7;
% particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)

% start NEAR3 inner wall, CHANNEL CENTERLINE
% % initPos = [2000, 0, 0];
% % simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% % dt = 1e-7;
% % particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)

% initPos = [1995, 0, 0];
% simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% dt = 5e-7;
% particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% 
% initPos = [2005, 0, 0];
% simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% dt = 1e-7;
% particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)

% % start at center
% initPos = [2010, 0, 0];
% simName = strcat(['_sphere_radius_1_5_init_' strrep(num2str(initPos), '     ', '_')]);
% dt = 1e-6;
% particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% 
% % start inner wall, bottom
% initPos = [1970, 0, 3];
% simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% dt = 1e-6;
% particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% 
% % start inner wall, bottom
% initPos = [1970, 0, 6];
% simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% dt = 1e-6;
% particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)

%%%% - --------------------------------------------

% % % start NEAR3 inner wall, CHANNEL CENTERLINE
% % initPos = [1990, 0, 10];
% % simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% % dt = 1e-6;
% % particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% % 
% % % start NEAR4 inner wall, CHANNEL CENTERLINE
% % initPos = [2000, 0, -10];
% % 
% % simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% % dt = 1e-6;
% % particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% % 
% % % start inner wall, bottom
% % initPos = [1970, 0, -20];
% % simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% % dt = 1e-6;
% % particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% % 
% % % start inner wall, top
% % initPos = [1970, 0, 20];
% % simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% % dt = 1e-6;
% % particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% % 
% % % start center, bottom
% % initPos = [2010, 0, 20];
% % simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% % dt = 1e-6;
% % particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% % 
% % % start center, bottom
% % initPos = [2010, 0, -20];
% % simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% % dt = 1e-6;
% % particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% % 
% % % start outer wall, TOP
% % initPos = [2050, 0, 20];
% % simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% % dt = 1e-6;
% % particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% % 
% % % start outer wall, bottom
% % initPos = [2050, 0, -20];
% % simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% % dt = 1e-6;
% % particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)
% % 
% % % start at outwall 
% % initPos = [2050, 0, 0];
% % simName = strcat(['_sphere_radius_1_5_init_' strrep(strrep(num2str(initPos), '    ', '_'), ' ', '')]);
% % dt = 1e-6;
% % particleFocusingFunction(sphere, ds1, ds2, itera, dt, sig, F_0, initPos, fDir, simName)

% ds1 = 0.12;
% ds2 = 1.5;
% sphereScriptFunction(1.5, ds1, ds2, 3000, 1e-7, 0, 0, [1990, 0, 0])
% 
% ds1 = 0.18;
% ds2 = 1.5;
% sphereScriptFunction(2.5, ds1, ds2, 3000, 1e-7, 0, 0, [1990, 0, 0])

% ds1 = 0.18;
% ds2 = 1.5;
% 
% RBC = @(P_body) generate_RBC_particle(P_body, ds1);
% fDir = 'C:\Users\Chris\Documents\Brady\Simulation\';
% simName = '_RBC_';

%sphereScriptFunction(funct, ds1, ds2, itera, deltaT, LJ_sig, LJ_r, initPos, fDir, simName)
%particleFocusingFunction(RBC, ds1, ds2, 3000, 1e-6, 0, 0, [1990, 0, 0], fDir, simName)
