%% functions
function [r_s] = generate_sphere_particle(P_body, dS, sphere_radius)
    % define how the particle geometry will be regenerated every iteration
    % this cuts down on the changing from one file to the next
    
    addpath('.\..\src')
    addpath('.\..\backgroundFlow')
    import SimulationClass.LowReynoldsSimulation.*
    import LowReynoldsGeometry.* % it include all of the functions related to the channel and particle shape

    %sphere_radius = 0.5; % um
    
    %Create body
    r_s = LowReynoldsGeometry.evenSphere(P_body(1), P_body(2), P_body(3), sphere_radius, dS);    
    %Rotate body
    
    %r_s = elps, RBC
    r_s = SimulationClass.rotation_Body(r_s, P_body);

end