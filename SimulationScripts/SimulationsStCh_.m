clc
clear
close all

fDir = 'C:\Users\Chris\Documents\Brady\Simulation\';
simName = '_sphereStCh_radius_1_5_';
simName = '_sphereStCh_radius_1_5_Bfix';
addpath('.\..\backgroundFlow')
                       

 ds1 = 0.10e-6;
 ds2 = 1.6e-6;
 itera = 5;
 
 F_0 = 0.8e-15;% WAS 0.8e-15;from bacteria paper, flagella...
 sig = 4 * ds1/10;%from properties of our simulation
 
 s_radius = 2e-6;


sphere = @(P_body) generate_sphere_particle(P_body, ds1, s_radius);

%backgroundFlowFileName = 'KatrinaVP_100w_50h_sphere_straight.csv';
%backgroundFlowFileName = 'Katrina_field_100w_50h_Straight_finerMesh.csv';

backgroundFlowFileName = 'KatrinaVP_100w_50h_sphere_straight.csv';
dat_dat = csvread(backgroundFlowFileName,10);

% slice of X
% dat_dat = dat_dat(dat_dat(:,1) == -5e-5, :);

r = dat_dat(:,1)'*1e0;%which is x axis (channel width)
z = dat_dat(:,3)'*1e0;%which is z axis (channel height)
u = dat_dat(:,4)'*1e0;%which is Ux axis
v = dat_dat(:,5)'*1e0;%which is Uz axis
w = dat_dat(:,6)'*1e0;%which is Uy axis

U = scatteredInterpolant(r', z', u');
V = scatteredInterpolant(r', z', v');
W = scatteredInterpolant(r', z', w');

Zero = scatteredInterpolant(r', z', zeros(length(u), 1));

radius = 1.5e-6;
ds1 = 0.12e-6;
ds2 = 1.1e-6;%1.6;

% coarse mesh

ds1 = 0.6e-6;
ds2 = 2.0e-6;%1.6;

itera = 6000;%6;% ds1, ds2 and iter have been overwritten  
deltaT = 3e-12;
xs = 20*1e-6; %(1990+i)*1e-6;  ??
ys = 0;%                       ??
zs = 10e-6;%20e-6;% 22e-6      ??
initPos = [xs, ys, zs, 0, 0, 0];

sphereStraightTest(radius, ds1, ds2, itera, deltaT, sig, F_0, initPos);
