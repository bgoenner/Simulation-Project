%% functions
function [r_s] = generate_RBC_particle(P_body, dS)
    % define how the particle geometry will be regenerated every iteration
    % this cuts down on the changing from one file to the next

    %sphere_radius = 0.5; % um
    
    r_s = unique(cell2mat(LowReynoldsGeometry.redBloodCell(P_body(1), P_body(2), P_body(3), dS)), 'rows');
    

    %r_s = elps, RBC
    r_s = SimulationClass.LowReynoldsSimulation.rotation_Body(r_s, P_body);
end