function [V_Body] = sphereSpiralConvergence(generate_particle, ds1, ds2, ang_chan)


dS_particle  = ds1;
eps_particle = dS_particle^0.9;
a_particle   = dS_particle/10;

% lennard jones parameters
e_LJ = LJ_sig;%1*10^2;
a_LJ = LJ_F;%rs;

xs = initPos(1);
ys = initPos(2);
zs = initPos(3);

mu = 8.9*10^-4;

generate_particle = @(P_body) particleFunct(P_body);

    %% Do not modify
% get background flow
%initialize background flow
backgroudFlowFileName = 'Vfield_100w_50h_r2000_spiral.csv';
dat_dat = csvread(backgroudFlowFileName,10);
r = dat_dat(:,1)';%which is x axis
z = dat_dat(:,2)';%which is y axis
u = dat_dat(:,3)'*10e6;%which is Ux axis
v = dat_dat(:,5)'*10e6;%which is Uz axis
w = dat_dat(:,4)'*10e6;%which is Uy axis

U = U_inter;%scatteredInterpolant(r', z', u');
V = V_inter;%scatteredInterpolant(r', z', v');
W = W_inter;%scatteredInterpolant(r', z', w');

% generate channel geometry
x_chan_center = 2010;
y_chan_center = 0;
z_chan_center = 0;
%ang_chan      = 0.3;
dS_chan       = ds2;
eps_chan      = dS_chan^0.9;
R_chan        = 2010;
width_chan    = 100;    % um
heigh_chan    = 50;     % um
a_chan        = dS_chan/10;

% define wall stokeslet postions
% CurvedChannel(x_center, y_center, z_center, R_channel, width, height, ang_channel, dS)
r_w = CurvedChannel(x_chan_center, y_chan_center, z_chan_center, R_chan, width_chan, heigh_chan, ang_chan, dS_chan);

% define velocity of wall stokeslets as zero
v_w = zeros(length(r_w) * 3, 1);
% define initial conditions
P_body(1,:) = [xs, ys, zs, 0, 0, 0]';  % initial position
V_body(1,:) = [0, 0, 0, 0, 0, 0]';     % initial velocity is zero


%logOutput = strcat([fDir, logFile, 'SimulationLogOutput', '_', time, '_', date '.txt']);
%logOutputFid = fopen(logFileName, 'a+');
% number of stokeslets
r_s = generate_particle(P_body);
%numS = length([r_s r_w]);

%% simulation iteration code

render = figure;
plot3(r_w(:, 1), r_w(:, 2), r_w(:, 3), '.b')
hold on
    plot3(r_s(:, 1), r_s(:, 2), r_s(:, 3), '.r')
hold off
saveas(render, strcat([fDir, figureFile, simName, '_renderPlot_', today], ''))

%try

%% do not modify

% print current time and date
disp(datestr(datetime()));

%while firstiter || endofsim || deltaP > deltaP_limit && t_current < t_next 
% code automatically decreases dt during large delta P_body

% regenerate body rotated
% y is set with zero
body1 = P_body(k, :);
body1(2) = 0;


%% regenerate geometery
r_s = generate_particle(body1);


%% interpolate velocities
r_s_x_prime = (r_s(:,1).^2 + r_s(:,2).^2 ).^(1/2);
u_s = -U(r_s_x_prime , r_s(:, 3));
% the positions are swapped to map y coord to z and vice versa
v_s = -V(r_s_x_prime , r_s(:, 3));
w_s = -W(r_s_x_prime , r_s(:, 3));

l_dot = [cell2mat(mat2cell([u_s v_s w_s], ones(length(v_s),1), 3)')'; v_w];


%% get body velocities
r_s_cell = {r_s, r_w};
P_cell   = {body1, body1};
a_cell   = {a_particle, a_chan};
eps_cell = {eps_particle, eps_chan};
[V_body(k+1,:), V_G, V_LJ, M, mem, numS] = iterate_multi(r_s_cell, P_cell, l_dot, a_cell, eps_cell, mu, a_LJ, e_LJ);

end