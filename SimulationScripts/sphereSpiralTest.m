function [V_G, V_P] = sphereSpiralTest(radius, ds1, ds2, itera, deltaT, ang, LJ_sig, LJ_r, initPos)

% clc
% clear
% close all


%% imports and path management

addpath('.\..\src')
addpath('.\..\backgroundFlow')
import SimulationClass.*
import LowReynoldsGeometry.*

%% simulation initializatoin

% important parameters
fDir = 'C:\Users\Chris\Documents\Brady\Simulation\';
% for testing in Brady's virtual machine CADE
%fDir = 'X:\Documents\Github\Simulation\';
simName = 'sphere_radius_1_5_';%never put period '.' to name any file (never use a . to name any file). error said: unsupported format or extensions or 
%itera  = 5000;%2000;
%deltaT = 1e-6;%1e-6;%back to e^-7, previously was e^-8

dS_particle  = 0.10;
dS_particle  = ds1;
eps_particle = dS_particle^0.9;
a_particle   = dS_particle/10;

% lennard jones parameters
e_LJ = LJ_sig;%1*10^2;
a_LJ = LJ_r;%rs;

deltaP_limit = 1e8;
endTime = (itera)*deltaT;
t = linspace(deltaT, endTime, itera);
dt = t(2) - t(1);

xs = initPos(1);
ys = 0; %Always set it to zero
zs = initPos(3);

mu = 8.9*10^-4;

% boundaries

boundary_x = [1958e-6,2057e-6];
boundary_z = [-25e-6, 25e-6];


generate_particle = @(P_body, dS) generate_sphere_particle(P_body, dS, radius);
% Keep (P_body, dS) as outputs; ^-- Modify to particle shape
    %% modify for different background flow
% get background flow
%initialize background flow
%backgroudFlowFileName = 'Vfield_100w_50h_r2000_spiral.csv';
backgroundFlowFileName = 'Vfield_100w_50h_r2000_spiral_resolutionfinerMesh.csv';
%backgroudFlowFileName = 'Vfield_100w_50h_r2000_spiral_relativeTolerancee-4MeshCustomsize1.csv';
backgroundPressureFile = 'Pfield_100w_50h_r2000_spiral_finerMesh.csv';
backgroundFlowFile     = 'PressureBGFlowExport.csv';

%% Do not modify
dat_dat = csvread(backgroundFlowFileName,10);
r = dat_dat(:,1)'*1e-6;%which is x axis
z = dat_dat(:,2)'*1e-6;%which is y axis
u = dat_dat(:,3)'*1e0;%which is Ux axis
v = dat_dat(:,4)'*1e0;%which is Uz axis
w = dat_dat(:,5)'*1e0;%which is Uy axis

U = scatteredInterpolant(r', z', u', 'natural');
V = scatteredInterpolant(r', z', v', 'natural');
W = scatteredInterpolant(r', z', w', 'natural');


Press_data = csvread(backgroundPressureFile,10);
%
rp = Press_data(:,1)*1e-6;
zp = Press_data(:,2)*1e-6;
pp = Press_data(:,3)*1e0;

PP = scatteredInterpolant(rp, zp, pp, 'natural');


  %% Modify for channel
% generate channel geometry
x_chan_center = 2010e-6;
y_chan_center = 0;
z_chan_center = 0;
ang_chan      = ang;%0.07;%0.1;
dS_chan       = 1.3;
dS_chan       = ds2;
eps_chan      = dS_chan^0.9;
R_chan        = 2010e-6;
width_chan    = 100e-6;    % um
heigh_chan    = 50e-6;     % um
a_chan        = dS_chan/10;

% define wall stokeslet postions
% CurvedChannel(x_center, y_center, z_center, R_channel, width, height, ang_channel, dS)
r_w = CurvedChannel(x_chan_center, y_chan_center, z_chan_center, R_chan, width_chan, heigh_chan, ang_chan, dS_chan);

%% do not modify
% define velocity of wall stokeslets as zero
v_w = zeros(length(r_w) * 3, 1);

% define initial conditions
P_body(1,:) = [xs, ys, zs, 0, 0, 0]';  % initial position
%P_body(1,:) = [2014.8244,4266.2856,24.8487,-36.2473,-0.501,0.44205]';
V_body(1,:) = [0, 0, 0, 0, 0, 0]';     % initial velocity is zero


%% log important initial data
% data
today = strrep(strrep(datestr(datetime()), ':', ''), ' ', '_');
workspaceFile = 'Workspaces\';
figureFile    = 'Figures\';
logFile       = 'Log\';

%logOutput = strcat([fDir, logFile, 'SimulationLogOutput', '_', time, '_', date '.txt']);
%logOutputFid = fopen(logFileName, 'a+');
% number of stokeslets
r_s = generate_particle(P_body, dS_particle);
%numS = length([r_s r_w]);

save(strcat([fDir workspaceFile simName '_InitWork_' today])) 

%% simulation iteration code
endofsim = 0;
deltaP = 0;
firstiter = 1;

render = figure; % figure 1
plot3(r_w(:, 1), r_w(:, 2), r_w(:, 3), '.b')
hold on
    plot3(r_s(:, 1), r_s(:, 2), r_s(:, 3), '.r')
hold off

channelPlot = figure; % figure 2
% plot velocity field
hold on
    quiver(r,z,u,w);
    plot(r_s(:,1), r_s(:,3), '.k')
hold off

red = 0;
green = 0;
blue = 0;

velPlot = figure; % velocity plot, figure 3
pause(0.05)
fileType = '.jpg';

PressurePlot = figure;% figure 4
interVelPlot = figure;% figure 5

for k = 1:length(t)
    %% do not modify
    %while firstiter || endofsim || deltaP > deltaP_limit && t_current < t_next 
        % code automatically decreases dt during large delta P_body
     
        start = cputime;

        % regenerate body rotated
        % z is set with zero
        body1 = P_body(k, :);
        body1(2) = 0;


        %% regenerate geometery
        
        % 
        r_s = generate_particle(body1, dS_particle);
        
        % 3D plot
        figure(1)
        plot3(r_w(:, 1), r_w(:, 2), r_w(:, 3), '.b')
        hold on
            plot3(r_s(:, 1), r_s(:, 2), r_s(:, 3), '.r')
        hold off
        

        % 2D plot
        % plot sphere position
        figure(2)
        if red >= 0.9 || green >= 0.9
            if green < 0.9
                green = green + 0.05;

            elseif green >= 0.9
                red = 0;
                blue = blue + 0.05;

                if blue >= 0.9
                    red = 0;
                    green = 0;
                      blue = 0;
                end
            end
        else
            red = red + 0.05;
        end

        hold on
            plot(r_s(:,1), r_s(:,3), '.', 'color', [red, green, blue])
        hold off
        
        %% interpolate velocities
        
        
        r_s_x_prime = (r_s(:,1).^2 + r_s(:,2).^2 ).^(1/2);
        u_s = -U(r_s_x_prime , r_s(:, 3));
        v_s = -V(r_s_x_prime , r_s(:, 3));
        w_s = -W(r_s_x_prime , r_s(:, 3));
        
        % consider x and z 0 vel
        u_s(1:end) = 0;
        w_s(1:end) = 0;
        
        figure(5)
        plot(r_s(:,1), r_s(:,3), '*')
        hold on
        quiver(r_s(:,1), r_s(:,3), u_s, w_s)
        hold off

        l_dot = [cell2mat(mat2cell([u_s v_s w_s], ones(length(v_s),1), 3)')'; v_w]; % with wall
        %l_dot = [cell2mat(mat2cell([u_s v_s w_s],ones(length(v_s),1),3)')']; %without wall
		%% Calculate pressures at stokeslet
		
		intPress = PP(r_s_x_prime , r_s(:, 3))*1e0;
        intPress = [repelem(intPress, 3, 1); v_w];
        %intPress = [intPress; zeros(length(v_w)/3, 1)];
      
		
		% calculate normal vector to surface and normalizes
		% for spheres it is the vector pointing to the center
		r_n = [r_s - repmat(body1(1:3), length(r_s), 1); ones(length(v_w)/3, 3)];
        r_n = [r_s - repmat(body1(1:3), length(r_s), 1)];
        r_n = (r_n ./ repmat(sqrt(sum(r_n.^2, 2)),1,3));
		%r_np = repmat(intPress,1,3) .* (r_n ./ norm(r_n));
        
        figure(4)
        plot(r_s(:,1), r_s(:,3), '*')
        hold on
        %quiver(r_s(:,1), r_s(:,3), r_np(:,1), r_np(:,3))
        hold off
		
        %r_n = [cell2mat(mat2cell([r_n], ones(length(v_s),1), 3)')'; v_w];
        r_n = cell2mat(mat2cell(r_n, ones(length(r_n),1), 3)')';
        
        %% get body velocities
        r_s_cell = {r_s*1e0, r_w*1e0};
        P_cell   = {body1, body1};
        a_cell   = {a_particle, a_chan};
        eps_cell = {eps_particle, eps_chan};
        
%         r_s_cell = {r_s*1e0};
%         P_cell   = {body1};
%         a_cell   = {a_particle};
%         eps_cell = {eps_particle};
        %[V_body(k+1,:), V_G, V_LJ, M, mem, numS] = iterate_GPU(r_s_cell, P_cell, l_dot, a_cell, eps_cell, mu, a_LJ, e_LJ);
        [V_body1(k+1,:), V_G, V_LJ, M, mem, numS, V_P, V_Pex] = iterate_multi_PressureEx(r_s_cell, P_cell, l_dot, r_n, intPress, a_cell, eps_cell, mu, a_LJ, e_LJ);
        
        %[V_body(k+1,:), V_G, V_LJ, M, mem, numS] = iterate_multi(r_s_cell, P_cell, l_dot, a_cell, eps_cell, mu, a_LJ, e_LJ);
        
        % particle radial velocity
        r_VP = sqrt(P_body(k, 1)^2 + P_body(k, 2)^2);
        dth_VP = V_body1(k+1,1)/ (pi * 2 * r_VP);
        
        Ldot_ch = -repelem(dth_VP * 2 * pi * sqrt(r_w(:, 2).^2 + r_w(:, 1).^2), 1, 3).*cross(r_w./repelem(sqrt(sum(r_w.^2, 2)), 1, 3), repelem([0,0,1], length(r_w), 1));
        
        
        
        % velocity of the shpere is 0
        v_part_0 = zeros(length(r_s) * 3, 1);
        
        %                   Changes a (n, 3) matrix to (3n, 1)
        Ldot_2 = [v_part_0; cell2mat(mat2cell(Ldot_ch, ones(length(r_w*3),1), 3)')'];
        
        intPress = zeros(length(Ldot_2), 1);%add this to not count P effect twice
        % gets the velocity of a still flow with moving particle
        %v_m = IRX_Matrix_sim_multi({r_s}, {body1}) * V_body(k+1, :)';
        %v_m(length(r_s)+1:end) = zeros(length(v_m)-length(r_s), 1);
        [V_body2(k+1,:), V_G2, V_LJ2, M2, mem2, numS2, V_P2, V_Pex2] = iterate_multi_PressureEx(r_s_cell, P_cell, Ldot_2, r_n, intPress, a_cell, eps_cell, mu, a_LJ, e_LJ);
        
        V_body(k+1,:) = V_body1(k+1,:)+V_body2(k+1,:);
        %because iterate_GPU takes 8x more memory and takes shorter time.
        %so, if we switch it to iterate_multi it takes longer and less
        %memory and does not error anymore, which is out memory. 
        %% integrate position
        P_body(k+1,:) = P_body(k,:) + dt * V_body(k + 1,:);
        
        %% display iteration time
        iterationTime = cputime - start;

        fprintf('Iteration: %i \t\tCalculation Time: %i\n', k, iterationTime);

        %% report relevent log data
        logName = 'MatlabSimLog';
        logFileName = strcat([fDir, logFile, logName, '_', today, '.csv']);
        
        if ~(exist(logFileName, 'file') == 2)
          fid = fopen(logFileName, 'a+');
          if fid == -1
            mkdir(fDir, logFile);
            fid = fopen(logFileName, 'a+');
          end
          fprintf(fid, 'Num_Stokeslets, Memory_Used, Iteration_Time, P_body_1, P_body_2, P_body_3, P_body_4, P_body_5, P_body_6, V_body_1, V_body_2, V_body_3, V_body_4, V_body_5, V_body_6, VG_body_1, VG_body_2, VG_body_3, VG_body_4, VG_body_5, VG_body_6, VLJ_body_1, VLJ_body_2, VLJ_body_3, VLJ_body_4, VLJ_body_5, VLJ_body_6\n');
        else
            fid = fopen(logFileName, 'a+');
        end
        
        % number of stokeslets
        % memoryUsed
        % iteration time
        % P_body
        p_str = strcat([num2str(P_body(k, 1)) ',' num2str(P_body(k, 2)) ',' num2str(P_body(k, 3)) ',' num2str(P_body(k, 4)) ',' num2str(P_body(k, 5)) ',' num2str(P_body(k, 6))]);
        % V_body + components
        v_str = strcat([num2str(V_body(k, 1)) ',' num2str(V_body(k, 2)) ',' num2str(V_body(k, 3)) ',' num2str(V_body(k, 4)) ',' num2str(V_body(k, 5)) ',' num2str(V_body(k, 6))]);
        vg_str = strcat([num2str(V_G(1)) ',' num2str(V_G(2)) ',' num2str(V_G(3)) ',' num2str(V_G(4)) ',' num2str(V_G(5)) ',' num2str(V_G(6))]);
        vlj_str = strcat([num2str(V_LJ(1)) ',' num2str(V_LJ(2)) ',' num2str(V_LJ(3)) ',' num2str(V_LJ(4)) ',' num2str(V_LJ(5)) ',' num2str(V_LJ(6))]);

        fprintf(fid, strcat([int2str(numS) ',' num2str(mem) ',' num2str(iterationTime) ',' p_str ',' v_str ',' vg_str ',' vlj_str ',\n']));
        fclose(fid);
        
    %end
    
    %% set new time
    t_(k+1) = t(k);
    t_old = t(k);
    t_current = t(k);
    if k < length(t)
        t_next = t(k+1);
        endofsim = 0;
    else
        endofsim = 1;
    end
    
    % end simulation in the particle has moved outside the channel
    if P_body(k+1, 1) > boundary_x(2) || P_body(k+1, 1) < boundary_x(1) || P_body(k+1, 3) > boundary_z(2) || P_body(k+1, 3) < boundary_z(1)
        fprintf('Particle has moved outside the channel current position: \n x:%i \n y:%i \n z:%i \n', P_body(k+1, 1), P_body(k+1, 2), P_body(k+1, 3)) 
        fprintf('Boundaries: x:[%i, %i]; z:[%i, %i]\n\n', boundary_x(1), boundary_x(2), boundary_z(1), boundary_z(2)) 
        break; 
    end
    
    figure(3)
    % plot velocity 
    
    subplot(4, 3, 1)
    plot(t_, V_body(:, 1))
    subplot(4, 3, 2)
    plot(t_, V_body(:, 2))
    subplot(4, 3, 3)
    plot(t_, V_body(:, 3))
    
    title('velocity plot')
    

    %figure % position plot
    subplot(4, 3, 4)    
    plot(t_, P_body(:, 1))
    subplot(4, 3, 5)
    plot(t_, P_body(:, 2))
    subplot(4, 3, 6)
    plot(t_, P_body(:, 3))
    
    title('position plot')
    

    % rotational velocity plot
    subplot(4, 3, 7)
    plot(t_, V_body(:, 4))
    subplot(4, 3, 8)
    plot(t_, V_body(:, 5))
    subplot(4, 3, 9)
    plot(t_, V_body(:, 6))
    
    title('rotational velocity plot')
    

    subplot(4, 3, 10)
    
    plot(t_, P_body(:, 4))
    subplot(4, 3, 11)
    plot(t_, P_body(:, 5))
    subplot(4, 3, 12)
    plot(t_, P_body(:, 6))
    
    title('rotational position plot')
    
%% -------------------------------------------------------------
    
    % save channel plots
    
    %saveas(velPlot, strcat([fDir, figureFile, simName, '_velocityPlot_', today, fileType], ''))
    %saveas(velPlot, strcat([fDir, figureFile, simName, '_velocityPlot_', today], ''))
    
    %saveas(channelPlot, strcat([fDir, figureFile, simName, '_ChannelPlot_', today, fileType], ''))
    %saveas(channelPlot, strcat([fDir, figureFile, simName, '_ChannelPlot_', today], ''))
    
    %save(strrep(strcat([fDir, workspaceFile, simName, '_Workspace_', today]), ' ', ''))
    
    pause(0.05)
    
end %% end simulation
% 
% save(strrep(strcat([fDir, figureFile, simName, '_ChannelPlot_', today, fileType]), ' ', ''))
% 
% saveas(channelPlot, strcat([fDir, workspaceFile, simName, '_FinWorkspace_', today, fileType], ''))


%% simulation post processing

% generate channel plot




close all
end
