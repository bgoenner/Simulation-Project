function [V_body] = sphereScriptFunction(radius, ds1, ds2, itera, deltaT, LJ_sig, LJ_r, initPos)


%% imports and path management

addpath('.\..\src')
addpath('.\..\backgroundFlow')
import SimulationClass.LowReynoldsSimulation.*
import LowReynoldsGeometry.*

%% simulation initializatoin

% important parameters
fDir = 'C:\Users\Chris\Documents\Brady\Simulation\';
simName = 'sphere_radius_1_0';
itera  = 3000;
%deltaT = 1e-7;

dS_particle  = 0.10;
dS_particle  = ds1;
eps_particle = dS_particle^0.9;
a_particle   = dS_particle/10;

% lennard jones parameters
e_LJ = LJ_sig;%1*10^2;
a_LJ = LJ_r;%rs;

deltaP_limit = 1e8;
endTime = (itera)*deltaT;
t = linspace(deltaT, endTime, itera);
dt = t(2) - t(1);

xs = initPos(1);
ys = initPos(2);
zs = 0;

mu = 8.9*10^-4;

generate_particle = @(P_body, dS) generate_sphere_particle(P_body, dS, radius);

    %% Do not modify
% get background flow
%initialize background flow
backgroundFlowFileName = 'Vfield_100w_50h_r2000_spiral_resolutionfinerMesh.txt';
backgroundPressureFile = 'Pfield_100w_50h_r2000_spiral_finerMesh.txt';
dat_dat = csvread(backgroundFlowFileName,10);
r = dat_dat(:,1)';%which is x axis
z = dat_dat(:,2)';%which is y axis
u = dat_dat(:,3)'*10e6;%which is Ux axis
v = dat_dat(:,5)'*10e6;%which is Uz axis
w = dat_dat(:,4)'*10e6;%which is Uy axis

U = scatteredInterpolant(r', z', u');
V = scatteredInterpolant(r', z', v');
W = scatteredInterpolant(r', z', w');

% generate channel geometry
x_chan_center = 2000;
y_chan_center = 0;
z_chan_center = 0;
ang_chan      = 0.1;
dS_chan       = 1.3;
dS_chan       = ds2;
eps_chan      = dS_chan^0.9;
R_chan        = 2000;
width_chan    = 100;    % um
heigh_chan    = 50;     % um
a_chan        = dS_chan/10;

% define wall stokeslet postions
% CurvedChannel(x_center, y_center, z_center, R_channel, width, height, ang_channel, dS)
r_w = CurvedChannel(x_chan_center, y_chan_center, z_chan_center, R_chan, width_chan, heigh_chan, ang_chan, dS_chan);

% define velocity of wall stokeslets as zero
v_w = zeros(length(r_w) * 3, 1);
% define initial conditions
P_body(1,:) = [xs, ys, zs, 0, 0, 0]';  % initial position
V_body(1,:) = [0, 0, 0, 0, 0, 0]';     % initial velocity is zero


%% log important initial data
% data
today = strrep(strrep(datestr(datetime()), ':', ''), ' ', '_');
workspaceFile = 'Workspaces\';
figureFile    = 'Figures\';
logFile       = 'Log\';

%logOutput = strcat([fDir, logFile, 'SimulationLogOutput', '_', time, '_', date '.txt']);
%logOutputFid = fopen(logFileName, 'a+');
% number of stokeslets
r_s = generate_particle(P_body, dS_particle);
%numS = length([r_s r_w]);

save(strcat([fDir workspaceFile simName '_InitWorkspace_' today])) 

%% simulation iteration code
endofsim = 0;
deltaP = 0;
firstiter = 1;

render = figure;
plot3(r_w(:, 1), r_w(:, 2), r_w(:, 3), '.b')
hold on
    plot3(r_s(:, 1), r_s(:, 2), r_s(:,
    3), '.r')
hold off

channelPlot = figure;
% plot velocity field
hold on
    quiver(r,z,u,v);
    plot(r_s(:,1), r_s(:,2), '.k')
hold off

red = 0;
green = 0;
blue = 0;

%% 

for k = 1:length(t)
    %% do not modify
    %while firstiter || endofsim || deltaP > deltaP_limit && t_current < t_next 
        % code automatically decreases dt during large delta P_body
     
        start = cputime;

        % regenerate body rotated
        % z is set with zero
        body1 = P_body(k, :);
        body1(3) = 0;


        %% regenerate geometery
        r_s = generate_particle(body1, dS_particle);

        % plot sphere position

        if red >= 0.9 || green >= 0.9
            if green < 0.9
                green = green + 0.05;

            elseif green >= 0.9
                red = 0;
                blue = blue + 0.05;

                if blue >= 0.9
                    red = 0;
                    green = 0;
                      blue = 0;
                end
            end
        else
            red = red + 0.05;
        end

        hold on
            plot(r_s(:,1), r_s(:,2), '.', 'color', [red, green, blue])
        hold off
        
        %% interpolate velocities
        r_s_x_prime = (r_s(:,1).^2 + r_s(:,3).^2 ).^(1/2);
        u_s = -U(r_s_x_prime , r_s(:, 2));
        v_s = -V(r_s_x_prime , r_s(:, 2));
        w_s = -W(r_s_x_prime , r_s(:, 2));

        l_dot = [cell2mat(mat2cell([u_s v_s w_s], ones(length(v_s),1), 3)')'; v_w];

        
        %% get body velocities
        r_s_cell = {r_s, r_w};
        P_cell   = {body1, body1};
        a_cell   = {a_particle, a_chan};
        eps_cell = {eps_particle, eps_chan};
        [V_body(k+1,:), V_G, V_LJ, M, mem, numS] = iterate_multi(r_s_cell, P_cell, l_dot, a_cell, eps_cell, mu, a_LJ, e_LJ);

        %% integrate position
        P_body(k+1,:) = P_body(k,:) + dt * V_body(k + 1,:);
        
        %% display iteration time
        iterationTime = cputime - start;

        fprintf('Iteration: %i \t\tCalculation Time: %i\n', k, iterationTime);

        %% report relevent log data
        logName = 'MatlabSimLog';
        logFileName = strcat([fDir, logFile, logName, '_', today, '.csv']);
        
        if ~(exist(logFileName, 'file') == 2)
          fid = fopen(logFileName, 'a+');
          if fid == -1
            mkdir(fDir, logFile);
            fid = fopen(logFileName, 'a+');
          end
          fprintf(fid, 'Num_Stokeslets, Memory_Used, Iteration_Time, P_body_1, P_body_2, P_body_3, P_body_4, P_body_5, P_body_6, V_body_1, V_body_2, V_body_3, V_body_4, V_body_5, V_body_6, VG_body_1, VG_body_2, VG_body_3, VG_body_4, VG_body_5, VG_body_6, VLJ_body_1, VLJ_body_2, VLJ_body_3, VLJ_body_4, VLJ_body_5, VLJ_body_6');
        else
            fid = fopen(logFileName, 'a+');
        end
        
        % number of stokeslets
        % memoryUsed
        % iteration time
        % P_body
        p_str = strcat([num2str(P_body(k, 1)) ',' num2str(P_body(k, 2)) ',' num2str(P_body(k, 3)) ',' num2str(P_body(k, 4)) ',' num2str(P_body(k, 5)) ',' num2str(P_body(k, 6))]);
        % V_body + components
        v_str = strcat([num2str(V_body(k, 1)) ',' num2str(V_body(k, 2)) ',' num2str(V_body(k, 3)) ',' num2str(V_body(k, 4)) ',' num2str(V_body(k, 5)) ',' num2str(V_body(k, 6))]);
        vg_str = strcat([num2str(V_G(1)) ',' num2str(V_G(2)) ',' num2str(V_G(3)) ',' num2str(V_G(4)) ',' num2str(V_G(5)) ',' num2str(V_G(6))]);
        vlj_str = strcat([num2str(V_LJ(1)) ',' num2str(V_LJ(2)) ',' num2str(V_LJ(3)) ',' num2str(V_LJ(4)) ',' num2str(V_LJ(5)) ',' num2str(V_LJ(6))]);

        fprintf(fid, strcat([int2str(numS) ',' num2str(mem) ',' num2str(iterationTime) ',' p_str ',' v_str ',' vg_str ',' vlj_str ',\n']));
        fclose(fid);
        
    %end
    
    %% set new time
    t_(k+1) = t(k);
    t_old = t(k);
    t_current = t(k);
    if k < length(t)
        t_next = t(k+1);
        endofsim = 0;
    else
        endofsim = 1;
    end
    
    % end simulation in the particle has moved outside the channel
    if P_body(k+1, 1) > 2057 || P_body(k+1, 1) < 1958 || P_body(k+1, 2) > 25 || P_body(k+1, 2) < -25
        fprintf('Particle has moved outside the channel current position: \n x:%i \n y:%i \n z:%i \n\n\n', P_body(1), P_body(2), P_body(3)) 
        break; 
    end
    
end % end simulation

saveas(strrep(strcat([fDir, figureFile, simName, '_ChannelPlot_', today]), ' ', ''))

saveas(channelPlot, strcat([fDir, workspaceFile, simName, '_FinWorkspace_', today, fileType], ''))


%% simulation post processing

% generate channel plot


% plot velocity 
velPlot = figure; % velocity plot
subplot(4, 1, 1)
hold on
plot(t_, V_body(:, 1))
plot(t_, V_body(:, 2))
plot(t_, V_body(:, 3))
hold off
title('velocity plot')
legend('x', 'y', 'z')

%figure % position plot
subplot(4, 1, 2)
hold on
plot(t_, (P_body(:, 1) - P_body(1,1)))
plot(t_, P_body(:, 2) - P_body(1,2))
%plot(t_, P_body(:, 3) - - P_body(3))
hold off
title('position plot')
legend('x', 'y', 'z')

% rotational velocity plot
subplot(4, 1, 3)
hold on
plot(t_, V_body(:, 4))
plot(t_, V_body(:, 5))
plot(t_, V_body(:, 6))
hold off
title('rotational velocity plot')
legend('x', 'y', 'z')

subplot(4, 1, 4)
hold on
plot(t_, P_body(:, 4) - P_body(1,4))
plot(t_, P_body(:, 5) - P_body(1,5))
plot(t_, P_body(:, 6) - P_body(1,6))
hold off
title('rotational position plot')
legend('x', 'y', 'z')

saveas(velPlot, strcat([fDir, figureFile, simName, '_velocityPlot_', today, fileType], ''))

close all
end
