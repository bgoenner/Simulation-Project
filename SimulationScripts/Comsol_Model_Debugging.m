% Model Debugging

% Run this code with the correct information to load the model. After
% functions can be called to verify the code is running correctly

addpath('.\..\src')
import ParticleLIVELINK.*

model_dir = 'C:\Users\Chris\Documents\Katrina';
model_dir_2 = 'C:\Users\Chris\Desktop';
addpath(model_dir_2)

model_name = 'Sphere_LongStrCh_10micron .mph';

comsol_dir = 'C:\Program Files\COMSOL\COMSOL54\Multiphysics\mli';

% this will recover from an error if the simulation is running; This can
% probably be implemented better
try
    mphstart(2036);
end

addpath(comsol_dir)

import com.comsol.model.*;
import com.comsol.model.util.*;


%model = ModelUtil.create('Model');
model = mphload(model_name);

format compact
format long
datestr(now)

% To unlock the model type
% ModelUtil.disconnect

