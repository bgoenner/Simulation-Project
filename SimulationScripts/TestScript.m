 clc
clear

addpath('.\..\src')
import ParticleLIVELINK.*

model_dir = 'C:\Users\Chris\Documents\Katrina';
model_dir_2 = 'C:\Users\Chris\Desktop';
addpath(model_dir_2)

%x
upper_x = 94.05;%45% xp
step_x = 94.05;%45
lower_x = 94.05;%45
boundary{1} = [lower_x:step_x:upper_x];
%y
upper_z = 25;%20
step_z = 25;%20
lower_z = 25;%20
boundary{2} = [lower_z:step_z:upper_z];
%y
constant = 0;

% args = [String Name, String Unit, Number Value] 
args = [];

[Force, Moment] = particle_CFD_solver('Sphere_LongStrCh_10micron .mph', boundary, constant, args);

% Output format Force{axis}(position_x, position_y)

Force_x = Force{1};
Force_z = Force{2};

[X, Z] = meshgrid(boundary{1}, boundary{2});

quiver(X, Z, Force_x, Force_z);

% Const = rho* Uavg^2 *da^4/ch^2;
% 
% CL_x = Force_x/(Const);
% CL_z = Force_z/(Const);
% 
% figure 
% hold on
% plot(CL_x, x/ch)
% figure
% plot(CL_z, z/ch)
% hold off
