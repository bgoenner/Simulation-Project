clc
clear
close all

addpath('.\..\src')
fDir = 'C:\Users\Chris\Documents\Brady\Simulation\';
simName = '_sphere_radius_1_5_';
addpath('.\..\backgroundFlow')

import LowReynoldsGeometry.*
import SimlulationClass.LowReynoldsSimulation.*

% [V_Lab, V_G, V_LJ, M, mem, numS] = iterate_multi(r_, r_o, L_dot, a_, e_, mu, sig_LJ, e_LJ)
% [errFig, E, C, Rsq] = convergenceFunction(geoFunct, dS, gam, cVar, fDir, name, verbose)

dS = [0.1, 0.2, 0.3, 0.4, 0.5];
%gam = 1:0.1:1.5;
gam = 0.7:0.05:0.9;

ds1 = 0.10;
ds2 = 1.6;
itera = 2;
s_radius = 1.5;
dt = 1;
initPos = [2000, 0, 0, 0, 0, 0];

F_0 = 0.8e-15;
sig = 4 * ds1/10;

% sphereScriptFunction(0.5, ds1, ds2, 3000, 1e-7, 0, 0, [1990, 0, 0])

sphere = @(P_body) generate_sphere_particle(P_body, ds1, s_radius);

sphereConvergenceAng = @(chan_ang) particleFocusingFunction(sphere, ds1, ds2, chan_ang, itera, dt, sig, F_0, initPos, fDir, simName);

[E, C, Rsq] = convergenceFunction(sphereConvergenceAng, dS, gam, 1, fDir, 'sphereAngTest', 1);