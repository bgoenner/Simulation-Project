clc
clear
close all

addpath('C:\Users\Chris\Documents\Brady\Simulation\Log')

myDir = 'C:\Users\Chris\Documents\Brady\Simulation\Log';

% get all file names
r = dir(fullfile(myDir,'*.csv'));

% x - pos, col 4; z -pos, col 6; x - vel, col 7; Z - vel, col 9;

pos_data = zeros(length(r), 2);
velt_data = zeros(length(r), 2);
velG_data = zeros(length(r), 2);
% extract the data from each csv file
% 
for i = 1:length(r)
    datatemp = csvread(r(i).name, 2);
    pos_data(i, 1) = datatemp(4);
    pos_data(i, 2) = datatemp(6);
    velt_data(i, 1) = datatemp(10);
    velt_data(i, 2) = datatemp(12);
    velG_data(i, 1) = datatemp(16);
    velG_data(i, 2) = datatemp(18);
end

% quiver plot

figure
quiver(pos_data(:, 1), pos_data(:, 2), velt_data(:, 1), velt_data(:, 2))
title('Total velocity')
figure
quiver(pos_data(:, 1), pos_data(:, 2), velG_data(:, 1), velG_data(:, 2))
title('G velocity')