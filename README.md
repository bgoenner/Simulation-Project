# Simulation Project

## general organization

- Keep all code in the src file

- Please only commit working code to master make a branch by


## useful git commands

<pre><code>git branch $branchname
git checkout $branchname
git clone
git fetch
git add
git commit
git pull
git push
git merge
</pre></code>

commit to the dev branch for all unstable changes

## general program outline

-> simulation scripts
    
    -> simulation.m( This is where all the simulation calls are made from)
        
        -> (specific simulation file)
            - setup
                - defines the background flow
                - initial geometries
                - defines particle geometry call
                - defines directories to save the data
            - simulation loop
                - interpolates the stokeslet velocity based geometric data
                - renerate particle based on the current position
                - perform the regularized stokeslet simulation
                - plot the data and save relevent plots



## TODO

- General geometry meshing
- Move class code to functions (easier to read and find relevent code)
- Retest convergence file and validate the code output  
- Add automatic job submission
- Implement normal vector for the pressure force
- Adaptive meshing based on particle postion

## General simulation issues and tests

- validate pressure force and velocity output
